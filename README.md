# Tutorial TabNet

## Installation

We use a standard DARTS environment with a GPU. Identify with a SUNset / LDAP account (or student one):

 -   https://data-analysis.synchrotron-soleil.fr/desktop/

The TabNet is already installed in the TensorFlow environment. Search for Tensorflow or TabNet in the lower left search bar.

- https://pypi.org/project/pytorch-tabnet/
- https://github.com/dreamquark-ai/tabnet

### Manual installation (not necessary)

Should you need to install it yourself, open a terminal to create a TabNet environment, relying on PyTorch.
```
    cd persistent_area
    python3 -m venv --system-site-packages tabnet-venv
    source tabnet-venv/bin/activate
    git clone https://github.com/dreamquark-ai/tabnet
    cd tabnet
    pip3 install .
```
TabNet is installed, relying on system libraries.
```
    ipython3
    import pytorch_tabnet
```

![image-2024-4-2_11-59-55](images/image-2024-4-2_11-59-55.png)

Then we can start to use TabNet.

## Application: Accelerator surrogate

The modelling of an accelerator requires heavy computations. It is desirable to build an estimator of the results, based on accumulated previous simulations.

In the following, we wish to build an interpolator that estimates:

*(injection efficiency, Touschek life-time) = f(69 magnet currents)*

![parametres-accelerateurs-1092x600](images/parametres-accelerateurs-1092x600.jpg)

We collect the required data from

- [tabnet_surrogate.ipynb](images/tabnet_surrogate.ipynb)
- [From_test_pool_v1604.mat](images/From_test_pool_v1604.mat)
- [tabnet_accelerator.py](images/tabnet_accelerator.py)

The training data set can be looked-into with Matlab:
```matlab
>> load From_test_pool_v1604.mat

data = 

  struct with fields:

     in: [5000×69 double]
    out: [5000×2 double]
```

We model the SOLEIL accelerator at E0=2.75 GeV. It contains 412 magnets, which we may reduce by making use of the ring symmetry.
We have 5000 optimized simulations configured with 60 magnet currents and 9 pseudo-parameters (in **in**). This results in 2 observables: the injection efficiency and the electron pack life-time (Touschek). In pratice, as these quantities must be both maximized, we define two quantities that should be minimized:
- **out(1)**: distance to the optimal injection location (in mm)
- **out(2)**: 6% - energy stability (in %)

Lets review the different phases of our study.

The python script which performs all the following tasks is attached here:

- [tabnet_accelerator.py](images/tabnet_accelerator.py)


### Import data

We first import the data from the Matlab format, and store in as Pandas DataFrames `X` and `y`. The Pandas DataFrames have attached column names, which we do not use further.

```python
# TabNet Acc: import data
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pytorch_tabnet.tab_model import TabNetRegressor # or from tabnet import TabNetRegressor 
from sklearn.model_selection import KFold
import scipy.io
 
# Read data
mat     = scipy.io.loadmat('./From_test_pool_v1604.mat')
inputs  = mat['data']['in'][0][0]
outputs = mat['data']['out'][0][0]
# raw input data
X = pd.DataFrame(inputs,  columns=["col_{}".format(i) for i in range(inputs.shape[1])])
# raw output data
y = pd.DataFrame(outputs, columns=['injection_efficiency','orbit_stability'])
```
Then we prepare and launch the TabNet training.

We split the input/output data as:

- Test: 20%, we shall measure at the end the training performance with this exclusive data
- Train: 60%, we train with this data
- Validation: 20%, we measure the learning process with this data

```python
# TabNet Acc: prepare training
from sklearn.model_selection import train_test_split
# split available data in train/test/validation (60/20/20 %)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
X_train, X_val,  y_train, y_val  = train_test_split(X_train, y_train, test_size=0.25, random_state=42)
 
# simplify naming
X_train = X_train.values
X_val   = X_val.values
X_test  = X_test.values
y_train = y_train.values
y_val   = y_val.values
y_test  = y_test.values
```

We may plot the training data set (output)
```python
    plt.hist(y_train)
    plt.show()
```

![image-2024-4-2_15-29-21](images/image-2024-4-2_15-29-21.png)

Here the 'Y' axis is the row count number in the training data, and the 'X' axis is the output variable (basically efficiency and life-time).

### Train the TabNet

We now launch the training. The aim is to correlate the input and the output, to build and interpolator.

```python
# TabNet Acc: train
# Train, with sklearn syntax
# The model will learn to match (X_train, y_train) and monitor the training with (X_val, y_val)
regressor = TabNetRegressor(seed=42)
regressor.fit(X_train, y_train, eval_set=[(X_val, y_val)], patience=300, max_epochs=2000, eval_metric=['rmse', 'mae', 'mse'])
 
# Evaluate on test data: difference between the regression (X_test -> preds) and the expected data (y_test)
from sklearn.metrics import mean_squared_error
preds = regressor.predict(X_test) # should be close to 'y_test'
test_mse = mean_squared_error(y_pred=preds, y_true=y_test)
print("Final test score: ", test_mse)
```

The  training produces the output (we use an NIVIDIA RTX 3090):
```
Device used : cuda
epoch 0  | loss: 5.91564 | val_0_rmse: 161.7938| val_0_mae: 158.94471| val_0_mse: 26177.2347|  0:00:02s
 
...
epoch 942| loss: 0.0126  | val_0_rmse: 0.34707 | val_0_mae: 0.11977 | val_0_mse: 0.12046 |  0:01:49s
epoch 943| loss: 0.01267 | val_0_rmse: 0.34723 | val_0_mae: 0.12151 | val_0_mse: 0.12057 |  0:01:49s
Early stopping occurred at epoch 943 with best_epoch = 643 and best_val_0_mse = 0.07669
 
Final test score:  0.06484647938842211
```

This takes e.g. 2 minutes. We can see that the surrogate prediction for the test data is close to the expected values within ~0.06 and the expected values are in the range 1:2. This makes about 3-6% relative error on predictions. Not bad.


### Using the model

We can now send any configuration of the magnets, not too far from the training data set, and get the prediction for the 2 quantities:
```python
    y = regressor.predict(X) # X is a vector with 69 values
```

Let's evaluate the test data and compare it to the one that has been obtained from the initial 'split' (20% of the total available data).
```python
X_test.shape
    (1000, 69)
y = regressor.predict(X_test)
```
Now `y` is the computed test output from the model, whereas `y_test` is the ground truth. Let's plot the difference.
```python
plt.plot(y_test[:, 0], y_test[:, 1], 'k.', alpha=0.2, label="Test data")
plt.plot(y[:, 0], y[:, 1], 'bx', alpha=0.2, label="predicted")
plt.yscale("log")
plt.xscale("log")
plt.legend()
```

We obtain the following plot:
![output_18_1](images/output_18_1.png)

Here the two axes are the 'output' variables of the model, e.g. the injection offset to the best efficiency (on X axis) and the offset to the optimal energy (life-time, on Y axis). We seek to minimize both (lower left side), which defines the Pareto boundary.

We may also show how the computed and ground-truth test data are coorelated:
```python
points_start = y
points_end = y_test

differences = points_end - points_start
plt.figure(figsize=(10, 6))
plt.plot(y_test[:, 0], y_test[:, 1], 'b.', alpha=0.3, label="Test data")
plt.plot(y[:, 0], y[:, 1], 'r.', alpha=0.3, label="predicted")
plt.quiver(points_start[:, 0], points_start[:, 1], differences[:, 0], differences[:, 1],
           angles='xy', scale_units='xy', scale=1, width=0.0025, color="black", alpha=0.4)
plt.yscale("log")
plt.xscale("log")
plt.legend()
```

![output_19_1](images/output_19_1.png)

Here the two axes are the 'output' variables of the model, e.g. the injection offset to best efficiency (on X axis) and offset to optimal energy (life-time, on Y axis). We seek to minimize both (lower left side).

We can clearly identify outliers which stand far from the main point cloud. By removing these, we may improve the model accuracy, but this is only relevant if we actually understand the causes for these outliers... The physics comes in.

### Saving/restoring the model

Once the model has been obtained, it is possible to save it to disk fir further re-use without restarting the training.
```python
from joblib import dump
dump(regressor, 'tabnet_accelerator.joblib')
```
and then later:
```python
from joblib import load
regressor = load('tabnet_accelerator.joblib')
```

## Credits

The initial TabNet tutorial was prepared by A. Bellachehab (at Synchrotron SOLEIL). The TabNet tutorial was improved by Mads Bertelsen (DTU/DMSC/ESS) during a training at the MLZ IA conference, April 2024.
