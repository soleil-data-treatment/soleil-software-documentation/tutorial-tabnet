#!/usr/bin/python3

# https://confluence.synchrotron-soleil.fr/display/EG/Experimentation+with+TabNet
# In the following, we wish to build an interpolator that estimates:
#
#   (injection efficiency, Touschek life-time) = f(69 magnet currents)

# TabNet Acc: import data
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pytorch_tabnet.tab_model import TabNetRegressor
from sklearn.model_selection import KFold
import scipy.io
# Read data
mat
= scipy.io.loadmat('./From_test_pool_v1604.mat')
inputs = mat['data']['in'][0][0]
outputs = mat['data']['out'][0][0]
X = pd.DataFrame(inputs, columns=["col_{}".format(i) for i in range(inputs.shape[1])])
y = pd.DataFrame(outputs, columns=['res1','res2'])

# TabNet Acc: prepare training
from sklearn.model_selection import train_test_split
# split available data in train/test/validation (60/20/20 %)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=42)
# simplify naming
X_train = X_train.values
X_val
= X_val.values
X_test = X_test.values
y_train = y_train.values
y_val
= y_val.values
y_test = y_test.values

plt.hist(y_train)
plt.show()

# TabNet Acc: train
# Train, with sklearn syntax
# The model will learn to match (X_train, y_train) and monitor the training with (X_val, y_val)
regressor = TabNetRegressor(seed=42)
regressor.fit(X_train, y_train, eval_set=[(X_val, y_val)], patience=300, max_epochs=2000, eval_metric=['rmse',
'mae', 'mse'])
# Evaluate on test data: difference between the regression (X_test -> preds) and the expected data (y_test)
from sklearn.metrics import mean_squared_error
preds = regressor.predict(X_test) # should be close to 'y_test'
test_mse = mean_squared_error(y_pred=preds, y_true=y_test)
print("Final test score: ", test_mse)

# Save the model
from joblib import dump
dump(regressor, 'tabnet_accelerator.joblib')

# load the model
# from joblib import load
# regressor = load('tabnet_accelerator.joblib')
