.. code:: ipython3

    import tabnet


.. parsed-literal::

    2024-04-10 11:07:00.468633: I tensorflow/core/platform/cpu_feature_guard.cc:182] This TensorFlow binary is optimized to use available CPU instructions in performance-critical operations.
    To enable the following instructions: AVX2 FMA, in other operations, rebuild TensorFlow with the appropriate compiler flags.
    2024-04-10 11:07:03.475874: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT


.. code:: ipython3

    !pip install pytorch_tabnet


.. parsed-literal::

    [33mWARNING: Ignoring invalid distribution -pycbf (/usr/lib/python3/dist-packages)[0m[33m
    [0mCollecting pytorch_tabnet
      Downloading pytorch_tabnet-4.1.0-py3-none-any.whl (44 kB)
    [2K     [38;2;114;156;31m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━[0m [32m44.5/44.5 kB[0m [31m1.8 MB/s[0m eta [36m0:00:00[0m
    [?25hRequirement already satisfied: numpy>=1.17 in /usr/lib/python3/dist-packages (from pytorch_tabnet) (1.24.2)
    Requirement already satisfied: scikit_learn>0.21 in /usr/lib/python3/dist-packages (from pytorch_tabnet) (1.2.1)
    Requirement already satisfied: scipy>1.4 in /usr/lib/python3/dist-packages (from pytorch_tabnet) (1.10.1)
    Requirement already satisfied: torch>=1.3 in /usr/lib/python3/dist-packages (from pytorch_tabnet) (2.1.2+gitunknown)
    Requirement already satisfied: tqdm>=4.36 in /usr/lib/python3/dist-packages (from pytorch_tabnet) (4.64.1)
    [33mWARNING: Ignoring invalid distribution -pycbf (/usr/lib/python3/dist-packages)[0m[33m
    [0mInstalling collected packages: pytorch_tabnet
    Successfully installed pytorch_tabnet-4.1.0


Tutorial
========

.. code:: ipython3

    # TabNet Acc: import data
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    from pytorch_tabnet.tab_model import TabNetRegressor # or from tabnet import TabNetRegressor 
    from sklearn.model_selection import KFold
    import scipy.io
     
    # Read data
    mat     = scipy.io.loadmat('./From_test_pool_v1604.mat')
    inputs  = mat['data']['in'][0][0]
    outputs = mat['data']['out'][0][0]
    X = pd.DataFrame(inputs,  columns=["col_{}".format(i) for i in range(inputs.shape[1])])
    y = pd.DataFrame(outputs, columns=['res1','res2'])

.. code:: ipython3

    tabnet.TabNetRegressor




.. parsed-literal::

    tabnet.tabnet.TabNetRegressor



.. code:: ipython3

    X




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>col_0</th>
          <th>col_1</th>
          <th>col_2</th>
          <th>col_3</th>
          <th>col_4</th>
          <th>col_5</th>
          <th>col_6</th>
          <th>col_7</th>
          <th>col_8</th>
          <th>col_9</th>
          <th>...</th>
          <th>col_59</th>
          <th>col_60</th>
          <th>col_61</th>
          <th>col_62</th>
          <th>col_63</th>
          <th>col_64</th>
          <th>col_65</th>
          <th>col_66</th>
          <th>col_67</th>
          <th>col_68</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>318.537570</td>
          <td>-105.688985</td>
          <td>-344.782099</td>
          <td>776.301796</td>
          <td>-422.124764</td>
          <td>-349.252299</td>
          <td>892.480273</td>
          <td>-314.072991</td>
          <td>-765.183088</td>
          <td>833.742853</td>
          <td>...</td>
          <td>-673.510182</td>
          <td>-797.151793</td>
          <td>102.383167</td>
          <td>351.377492</td>
          <td>705.769464</td>
          <td>745.626934</td>
          <td>-672.749070</td>
          <td>37.912559</td>
          <td>44.572019</td>
          <td>-0.068508</td>
        </tr>
        <tr>
          <th>1</th>
          <td>331.043792</td>
          <td>-106.066452</td>
          <td>-325.057207</td>
          <td>780.875925</td>
          <td>-405.770977</td>
          <td>-340.830997</td>
          <td>861.915655</td>
          <td>-316.604112</td>
          <td>-773.849192</td>
          <td>796.474642</td>
          <td>...</td>
          <td>-670.348882</td>
          <td>-803.903229</td>
          <td>470.845687</td>
          <td>978.464027</td>
          <td>193.131275</td>
          <td>380.666434</td>
          <td>976.566840</td>
          <td>38.236469</td>
          <td>42.399777</td>
          <td>-0.088507</td>
        </tr>
        <tr>
          <th>2</th>
          <td>325.220097</td>
          <td>-106.740678</td>
          <td>-339.710357</td>
          <td>777.722674</td>
          <td>-433.236777</td>
          <td>-365.925475</td>
          <td>885.879756</td>
          <td>-302.301703</td>
          <td>-761.395322</td>
          <td>794.216045</td>
          <td>...</td>
          <td>-673.831722</td>
          <td>-800.956963</td>
          <td>56.483612</td>
          <td>947.728908</td>
          <td>1183.604256</td>
          <td>646.327178</td>
          <td>136.844224</td>
          <td>51.441338</td>
          <td>30.844137</td>
          <td>-0.070826</td>
        </tr>
        <tr>
          <th>3</th>
          <td>315.041000</td>
          <td>-100.251559</td>
          <td>-364.497573</td>
          <td>814.283888</td>
          <td>-411.104582</td>
          <td>-367.199172</td>
          <td>888.625287</td>
          <td>-316.603090</td>
          <td>-795.967765</td>
          <td>876.380577</td>
          <td>...</td>
          <td>-663.932624</td>
          <td>-810.366501</td>
          <td>10.556329</td>
          <td>1424.036500</td>
          <td>464.316577</td>
          <td>710.433742</td>
          <td>355.677607</td>
          <td>58.454446</td>
          <td>30.161479</td>
          <td>-0.062253</td>
        </tr>
        <tr>
          <th>4</th>
          <td>327.202995</td>
          <td>-102.659730</td>
          <td>-336.783640</td>
          <td>817.603617</td>
          <td>-423.830321</td>
          <td>-367.601537</td>
          <td>931.636914</td>
          <td>-295.274436</td>
          <td>-732.291440</td>
          <td>838.988254</td>
          <td>...</td>
          <td>-680.300329</td>
          <td>-804.427753</td>
          <td>40.458445</td>
          <td>244.053107</td>
          <td>137.972405</td>
          <td>1174.084352</td>
          <td>1235.238752</td>
          <td>36.631358</td>
          <td>39.560704</td>
          <td>-0.094738</td>
        </tr>
        <tr>
          <th>...</th>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
        </tr>
        <tr>
          <th>4995</th>
          <td>312.658498</td>
          <td>-108.490348</td>
          <td>-350.826961</td>
          <td>776.732486</td>
          <td>-415.718930</td>
          <td>-342.191044</td>
          <td>892.593256</td>
          <td>-306.501771</td>
          <td>-760.163055</td>
          <td>798.573730</td>
          <td>...</td>
          <td>-671.620917</td>
          <td>-806.277565</td>
          <td>854.572764</td>
          <td>1417.694118</td>
          <td>461.622975</td>
          <td>1247.086226</td>
          <td>1159.433770</td>
          <td>33.790555</td>
          <td>41.937085</td>
          <td>-0.080447</td>
        </tr>
        <tr>
          <th>4996</th>
          <td>333.808974</td>
          <td>-111.618743</td>
          <td>-358.315406</td>
          <td>756.268801</td>
          <td>-433.045223</td>
          <td>-370.207437</td>
          <td>926.244264</td>
          <td>-295.228612</td>
          <td>-721.551561</td>
          <td>800.059243</td>
          <td>...</td>
          <td>-660.437624</td>
          <td>-782.266526</td>
          <td>211.603579</td>
          <td>636.975500</td>
          <td>675.417136</td>
          <td>1286.907805</td>
          <td>875.912466</td>
          <td>37.970978</td>
          <td>48.243003</td>
          <td>-0.073006</td>
        </tr>
        <tr>
          <th>4997</th>
          <td>322.349713</td>
          <td>-111.898039</td>
          <td>-330.297291</td>
          <td>804.613920</td>
          <td>-415.032000</td>
          <td>-363.548828</td>
          <td>853.023507</td>
          <td>-317.629365</td>
          <td>-805.332571</td>
          <td>841.473202</td>
          <td>...</td>
          <td>-678.610047</td>
          <td>-811.097153</td>
          <td>475.953638</td>
          <td>310.072152</td>
          <td>1281.330362</td>
          <td>595.399871</td>
          <td>1292.472002</td>
          <td>51.349449</td>
          <td>54.055874</td>
          <td>-0.060540</td>
        </tr>
        <tr>
          <th>4998</th>
          <td>320.941704</td>
          <td>-109.376080</td>
          <td>-352.789073</td>
          <td>740.130769</td>
          <td>-402.333706</td>
          <td>-368.692858</td>
          <td>896.105857</td>
          <td>-316.625261</td>
          <td>-768.947026</td>
          <td>821.369079</td>
          <td>...</td>
          <td>-661.460364</td>
          <td>-807.745470</td>
          <td>137.259720</td>
          <td>223.818457</td>
          <td>1442.211061</td>
          <td>485.027785</td>
          <td>1213.311455</td>
          <td>31.507366</td>
          <td>49.434780</td>
          <td>-0.071665</td>
        </tr>
        <tr>
          <th>4999</th>
          <td>317.307279</td>
          <td>-105.163018</td>
          <td>-357.287618</td>
          <td>750.070604</td>
          <td>-418.285415</td>
          <td>-358.040925</td>
          <td>907.415014</td>
          <td>-301.048090</td>
          <td>-756.703210</td>
          <td>837.797896</td>
          <td>...</td>
          <td>-671.132669</td>
          <td>-795.625598</td>
          <td>430.693792</td>
          <td>409.190929</td>
          <td>1087.061549</td>
          <td>1224.381272</td>
          <td>656.509527</td>
          <td>45.271678</td>
          <td>53.091571</td>
          <td>-0.070216</td>
        </tr>
      </tbody>
    </table>
    <p>5000 rows × 69 columns</p>
    </div>



.. code:: ipython3

    # TabNet Acc: prepare training
    from sklearn.model_selection import train_test_split
    # split available data in train/test/validation (60/20/20 %)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=37)
    X_train, X_val,  y_train, y_val  = train_test_split(X_train, y_train, test_size=0.25, random_state=37)
     
    # simplify naming
    X_train = X_train.values
    X_val   = X_val.values
    X_test  = X_test.values
    y_train = y_train.values
    
    indices = np.any(y_train > 4, axis=1)
    X_train = X_train[~indices]
    y_train = y_train[~indices]
    
    y_val   = y_val.values
    indices = np.any(y_val > 4, axis=1)
    X_val = X_val[~indices]
    y_val = y_val[~indices]
    
    """
    y_test  = y_test.values
    indices = np.any(y_test > 4, axis=1)
    X_test = X_test[~indices]
    y_test = y_test[~indices]
    """
    
    plt.plot(y_val, label='val')
    plt.plot(y_train, label='train')
    #plt.plot(y_test, label='test')
    plt.legend()
    plt.show()




.. image:: output_6_0.png


.. code:: ipython3

    indices




.. parsed-literal::

    array([False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False,  True, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False,  True, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False,  True,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
            True, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False,  True,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False, False, False, False, False, False, False, False, False,
           False])



.. code:: ipython3

    plt.hist(y_train)
    plt.show()



.. image:: output_8_0.png


.. code:: ipython3

    # TabNet Acc: train
    # Train, with sklearn syntax
    # The model will learn to match (X_train, y_train) and monitor the training with (X_val, y_val)
    regressor = TabNetRegressor(seed=42)
    regressor.fit(X_train, y_train, eval_set=[(X_val, y_val)], patience=300, max_epochs=2000, eval_metric=['rmse', 'mae', 'mse'])
     
    # Evaluate on test data: difference between the regression (X_test -> preds) and the expected data (y_test)
    from sklearn.metrics import mean_squared_error
    preds = regressor.predict(X_test) # should be close to 'y_test'
    test_mse = mean_squared_error(y_pred=preds, y_true=y_test)
    print("Final test score: ", test_mse)


.. parsed-literal::

    /opt/venvs/tensorflow/lib/python3.11/site-packages/pytorch_tabnet/abstract_model.py:82: UserWarning: Device used : cuda
      warnings.warn(f"Device used : {self.device}")


.. parsed-literal::

    epoch 0  | loss: 5.50709 | val_0_rmse: 84.02641| val_0_mae: 80.48755| val_0_mse: 7060.43696|  0:00:00s
    epoch 1  | loss: 3.06129 | val_0_rmse: 178.18814| val_0_mae: 132.10004| val_0_mse: 31751.01446|  0:00:00s
    epoch 2  | loss: 1.7055  | val_0_rmse: 109.14009| val_0_mae: 100.69735| val_0_mse: 11911.55967|  0:00:00s
    epoch 3  | loss: 1.02957 | val_0_rmse: 212.58184| val_0_mae: 210.52457| val_0_mse: 45191.03696|  0:00:00s
    epoch 4  | loss: 0.68948 | val_0_rmse: 181.81438| val_0_mae: 181.62491| val_0_mse: 33056.46735|  0:00:00s
    epoch 5  | loss: 0.55846 | val_0_rmse: 156.60874| val_0_mae: 156.16101| val_0_mse: 24526.29883|  0:00:00s
    epoch 6  | loss: 0.39584 | val_0_rmse: 69.32335| val_0_mae: 42.97304| val_0_mse: 4805.72665|  0:00:00s
    epoch 7  | loss: 0.32318 | val_0_rmse: 227.23521| val_0_mae: 224.2206| val_0_mse: 51635.83932|  0:00:00s
    epoch 8  | loss: 0.26335 | val_0_rmse: 225.56209| val_0_mae: 212.84448| val_0_mse: 50878.25567|  0:00:01s
    epoch 9  | loss: 0.22328 | val_0_rmse: 35.69459| val_0_mae: 26.80043| val_0_mse: 1274.10356|  0:00:01s
    epoch 10 | loss: 0.19046 | val_0_rmse: 56.14846| val_0_mae: 45.41832| val_0_mse: 3152.64985|  0:00:01s
    epoch 11 | loss: 0.17414 | val_0_rmse: 86.57294| val_0_mae: 74.19116| val_0_mse: 7494.87356|  0:00:01s
    epoch 12 | loss: 0.15102 | val_0_rmse: 102.51813| val_0_mae: 91.35763| val_0_mse: 10509.96761|  0:00:01s
    epoch 13 | loss: 0.1272  | val_0_rmse: 66.85674| val_0_mae: 52.70008| val_0_mse: 4469.8236|  0:00:01s
    epoch 14 | loss: 0.11233 | val_0_rmse: 19.45465| val_0_mae: 14.63498| val_0_mse: 378.48331|  0:00:01s
    epoch 15 | loss: 0.10681 | val_0_rmse: 27.53441| val_0_mae: 21.00184| val_0_mse: 758.14382|  0:00:01s
    epoch 16 | loss: 0.10352 | val_0_rmse: 59.74472| val_0_mae: 56.40976| val_0_mse: 3569.43197|  0:00:01s
    epoch 17 | loss: 0.09728 | val_0_rmse: 56.90404| val_0_mae: 54.09201| val_0_mse: 3238.07021|  0:00:02s
    epoch 18 | loss: 0.08982 | val_0_rmse: 53.73497| val_0_mae: 51.55131| val_0_mse: 2887.44677|  0:00:02s
    epoch 19 | loss: 0.08722 | val_0_rmse: 45.98882| val_0_mae: 39.93104| val_0_mse: 2114.97131|  0:00:02s
    epoch 20 | loss: 0.08169 | val_0_rmse: 40.07185| val_0_mae: 32.24306| val_0_mse: 1605.75297|  0:00:02s
    epoch 21 | loss: 0.07925 | val_0_rmse: 34.81702| val_0_mae: 25.5072 | val_0_mse: 1212.22484|  0:00:02s
    epoch 22 | loss: 0.07432 | val_0_rmse: 38.98673| val_0_mae: 27.07195| val_0_mse: 1519.96503|  0:00:02s
    epoch 23 | loss: 0.07446 | val_0_rmse: 36.5922 | val_0_mae: 25.73723| val_0_mse: 1338.98874|  0:00:02s
    epoch 24 | loss: 0.0701  | val_0_rmse: 35.91848| val_0_mae: 25.62835| val_0_mse: 1290.13733|  0:00:02s
    epoch 25 | loss: 0.06886 | val_0_rmse: 36.52588| val_0_mae: 25.70156| val_0_mse: 1334.13988|  0:00:03s
    epoch 26 | loss: 0.06868 | val_0_rmse: 34.29988| val_0_mae: 24.41905| val_0_mse: 1176.48193|  0:00:03s
    epoch 27 | loss: 0.07026 | val_0_rmse: 31.11502| val_0_mae: 23.29091| val_0_mse: 968.14461|  0:00:03s
    epoch 28 | loss: 0.06274 | val_0_rmse: 33.87569| val_0_mae: 31.00433| val_0_mse: 1147.56225|  0:00:03s
    epoch 29 | loss: 0.06388 | val_0_rmse: 29.40896| val_0_mae: 26.11389| val_0_mse: 864.88696|  0:00:03s
    epoch 30 | loss: 0.06223 | val_0_rmse: 27.41784| val_0_mae: 24.16329| val_0_mse: 751.73788|  0:00:03s
    epoch 31 | loss: 0.06142 | val_0_rmse: 28.31229| val_0_mae: 26.45896| val_0_mse: 801.58581|  0:00:03s
    epoch 32 | loss: 0.05979 | val_0_rmse: 31.10715| val_0_mae: 29.68603| val_0_mse: 967.65492|  0:00:03s
    epoch 33 | loss: 0.05977 | val_0_rmse: 29.28734| val_0_mae: 29.10353| val_0_mse: 857.7481|  0:00:04s
    epoch 34 | loss: 0.05973 | val_0_rmse: 27.37908| val_0_mae: 27.17045| val_0_mse: 749.61386|  0:00:04s
    epoch 35 | loss: 0.05844 | val_0_rmse: 17.8407 | val_0_mae: 17.63263| val_0_mse: 318.29044|  0:00:04s
    epoch 36 | loss: 0.05922 | val_0_rmse: 13.27531| val_0_mae: 11.69707| val_0_mse: 176.23373|  0:00:04s
    epoch 37 | loss: 0.05886 | val_0_rmse: 10.5969 | val_0_mae: 8.99129 | val_0_mse: 112.29424|  0:00:04s
    epoch 38 | loss: 0.05579 | val_0_rmse: 8.285   | val_0_mae: 6.48197 | val_0_mse: 68.64115|  0:00:04s
    epoch 39 | loss: 0.05643 | val_0_rmse: 7.8998  | val_0_mae: 6.17258 | val_0_mse: 62.40677|  0:00:04s
    epoch 40 | loss: 0.05637 | val_0_rmse: 7.10273 | val_0_mae: 5.31752 | val_0_mse: 50.44876|  0:00:04s
    epoch 41 | loss: 0.05786 | val_0_rmse: 5.67939 | val_0_mae: 4.34001 | val_0_mse: 32.25546|  0:00:05s
    epoch 42 | loss: 0.05425 | val_0_rmse: 10.89168| val_0_mae: 8.0473  | val_0_mse: 118.62861|  0:00:05s
    epoch 43 | loss: 0.05344 | val_0_rmse: 6.36402 | val_0_mae: 4.5055  | val_0_mse: 40.50071|  0:00:05s
    epoch 44 | loss: 0.05368 | val_0_rmse: 4.44611 | val_0_mae: 3.69761 | val_0_mse: 19.76789|  0:00:05s
    epoch 45 | loss: 0.05224 | val_0_rmse: 4.57946 | val_0_mae: 3.59857 | val_0_mse: 20.97143|  0:00:05s
    epoch 46 | loss: 0.05425 | val_0_rmse: 5.82231 | val_0_mae: 4.14058 | val_0_mse: 33.89934|  0:00:05s
    epoch 47 | loss: 0.05368 | val_0_rmse: 5.38064 | val_0_mae: 3.84252 | val_0_mse: 28.95129|  0:00:05s
    epoch 48 | loss: 0.05216 | val_0_rmse: 4.46389 | val_0_mae: 3.32488 | val_0_mse: 19.92631|  0:00:05s
    epoch 49 | loss: 0.05201 | val_0_rmse: 4.12489 | val_0_mae: 3.18335 | val_0_mse: 17.01469|  0:00:05s
    epoch 50 | loss: 0.05082 | val_0_rmse: 7.03343 | val_0_mae: 5.36576 | val_0_mse: 49.46912|  0:00:06s
    epoch 51 | loss: 0.05075 | val_0_rmse: 8.69671 | val_0_mae: 6.87225 | val_0_mse: 75.63285|  0:00:06s
    epoch 52 | loss: 0.05162 | val_0_rmse: 9.042   | val_0_mae: 7.25361 | val_0_mse: 81.75782|  0:00:06s
    epoch 53 | loss: 0.05064 | val_0_rmse: 9.05544 | val_0_mae: 7.07595 | val_0_mse: 82.00092|  0:00:06s
    epoch 54 | loss: 0.05135 | val_0_rmse: 8.85186 | val_0_mae: 7.14319 | val_0_mse: 78.35548|  0:00:06s
    epoch 55 | loss: 0.05018 | val_0_rmse: 9.18583 | val_0_mae: 7.40595 | val_0_mse: 84.37955|  0:00:06s
    epoch 56 | loss: 0.05194 | val_0_rmse: 9.19815 | val_0_mae: 7.22707 | val_0_mse: 84.60596|  0:00:06s
    epoch 57 | loss: 0.05104 | val_0_rmse: 8.38692 | val_0_mae: 6.83723 | val_0_mse: 70.34042|  0:00:06s
    epoch 58 | loss: 0.05013 | val_0_rmse: 8.42817 | val_0_mae: 6.92609 | val_0_mse: 71.03398|  0:00:06s
    epoch 59 | loss: 0.04979 | val_0_rmse: 8.90895 | val_0_mae: 7.68478 | val_0_mse: 79.36936|  0:00:07s
    epoch 60 | loss: 0.04899 | val_0_rmse: 8.42083 | val_0_mae: 7.19019 | val_0_mse: 70.91035|  0:00:07s
    epoch 61 | loss: 0.04966 | val_0_rmse: 9.23904 | val_0_mae: 7.4141  | val_0_mse: 85.35982|  0:00:07s
    epoch 62 | loss: 0.04943 | val_0_rmse: 8.11631 | val_0_mae: 6.96523 | val_0_mse: 65.87441|  0:00:07s
    epoch 63 | loss: 0.0499  | val_0_rmse: 7.428   | val_0_mae: 5.9377  | val_0_mse: 55.17524|  0:00:07s
    epoch 64 | loss: 0.04782 | val_0_rmse: 7.06894 | val_0_mae: 5.59429 | val_0_mse: 49.96987|  0:00:07s
    epoch 65 | loss: 0.04899 | val_0_rmse: 6.9549  | val_0_mae: 5.49582 | val_0_mse: 48.37062|  0:00:07s
    epoch 66 | loss: 0.05137 | val_0_rmse: 7.77963 | val_0_mae: 5.47103 | val_0_mse: 60.52264|  0:00:07s
    epoch 67 | loss: 0.04807 | val_0_rmse: 11.17876| val_0_mae: 6.01775 | val_0_mse: 124.96463|  0:00:07s
    epoch 68 | loss: 0.04836 | val_0_rmse: 21.97777| val_0_mae: 7.66724 | val_0_mse: 483.0222|  0:00:08s
    epoch 69 | loss: 0.04689 | val_0_rmse: 24.68623| val_0_mae: 8.36514 | val_0_mse: 609.41018|  0:00:08s
    epoch 70 | loss: 0.04602 | val_0_rmse: 24.27697| val_0_mae: 8.49344 | val_0_mse: 589.37141|  0:00:08s
    epoch 71 | loss: 0.04624 | val_0_rmse: 19.81415| val_0_mae: 7.47985 | val_0_mse: 392.60055|  0:00:08s
    epoch 72 | loss: 0.04494 | val_0_rmse: 26.18023| val_0_mae: 10.22925| val_0_mse: 685.40446|  0:00:08s
    epoch 73 | loss: 0.04805 | val_0_rmse: 29.46334| val_0_mae: 11.09678| val_0_mse: 868.08841|  0:00:08s
    epoch 74 | loss: 0.04546 | val_0_rmse: 23.86337| val_0_mae: 8.85899 | val_0_mse: 569.46025|  0:00:08s
    epoch 75 | loss: 0.04713 | val_0_rmse: 29.24261| val_0_mae: 10.49669| val_0_mse: 855.13004|  0:00:08s
    epoch 76 | loss: 0.04411 | val_0_rmse: 23.8396 | val_0_mae: 8.57933 | val_0_mse: 568.32672|  0:00:08s
    epoch 77 | loss: 0.04686 | val_0_rmse: 17.22682| val_0_mae: 6.85296 | val_0_mse: 296.76321|  0:00:09s
    epoch 78 | loss: 0.04753 | val_0_rmse: 12.48765| val_0_mae: 5.39125 | val_0_mse: 155.94135|  0:00:09s
    epoch 79 | loss: 0.04661 | val_0_rmse: 8.18097 | val_0_mae: 4.45577 | val_0_mse: 66.92832|  0:00:09s
    epoch 80 | loss: 0.04448 | val_0_rmse: 10.15132| val_0_mae: 4.34612 | val_0_mse: 103.04938|  0:00:09s
    epoch 81 | loss: 0.04492 | val_0_rmse: 7.63018 | val_0_mae: 4.04564 | val_0_mse: 58.21962|  0:00:09s
    epoch 82 | loss: 0.04463 | val_0_rmse: 5.23152 | val_0_mae: 3.22873 | val_0_mse: 27.36879|  0:00:09s
    epoch 83 | loss: 0.0431  | val_0_rmse: 6.47978 | val_0_mae: 3.28413 | val_0_mse: 41.98751|  0:00:09s
    epoch 84 | loss: 0.04516 | val_0_rmse: 5.67811 | val_0_mae: 3.03659 | val_0_mse: 32.24088|  0:00:09s
    epoch 85 | loss: 0.04303 | val_0_rmse: 5.58076 | val_0_mae: 3.07544 | val_0_mse: 31.14488|  0:00:09s
    epoch 86 | loss: 0.04412 | val_0_rmse: 7.31741 | val_0_mae: 3.26323 | val_0_mse: 53.54448|  0:00:10s
    epoch 87 | loss: 0.04526 | val_0_rmse: 6.70685 | val_0_mae: 3.13853 | val_0_mse: 44.98184|  0:00:10s
    epoch 88 | loss: 0.04386 | val_0_rmse: 7.03535 | val_0_mae: 2.83159 | val_0_mse: 49.49622|  0:00:10s
    epoch 89 | loss: 0.04308 | val_0_rmse: 4.89096 | val_0_mae: 2.40634 | val_0_mse: 23.92145|  0:00:10s
    epoch 90 | loss: 0.04301 | val_0_rmse: 2.90085 | val_0_mae: 2.09631 | val_0_mse: 8.41494 |  0:00:10s
    epoch 91 | loss: 0.04285 | val_0_rmse: 2.60579 | val_0_mae: 1.89924 | val_0_mse: 6.79014 |  0:00:10s
    epoch 92 | loss: 0.04464 | val_0_rmse: 4.20453 | val_0_mae: 2.23286 | val_0_mse: 17.67809|  0:00:10s
    epoch 93 | loss: 0.04224 | val_0_rmse: 3.79134 | val_0_mae: 2.41551 | val_0_mse: 14.37426|  0:00:10s
    epoch 94 | loss: 0.04329 | val_0_rmse: 11.09148| val_0_mae: 3.74009 | val_0_mse: 123.02098|  0:00:11s
    epoch 95 | loss: 0.04322 | val_0_rmse: 13.25199| val_0_mae: 4.29674 | val_0_mse: 175.61535|  0:00:11s
    epoch 96 | loss: 0.04373 | val_0_rmse: 16.34558| val_0_mae: 5.23355 | val_0_mse: 267.17783|  0:00:11s
    epoch 97 | loss: 0.04171 | val_0_rmse: 15.82937| val_0_mae: 5.01169 | val_0_mse: 250.56896|  0:00:11s
    epoch 98 | loss: 0.04408 | val_0_rmse: 15.8581 | val_0_mae: 5.12807 | val_0_mse: 251.47923|  0:00:11s
    epoch 99 | loss: 0.0431  | val_0_rmse: 14.86726| val_0_mae: 4.85086 | val_0_mse: 221.0355|  0:00:11s
    epoch 100| loss: 0.04144 | val_0_rmse: 13.65214| val_0_mae: 4.07228 | val_0_mse: 186.38103|  0:00:11s
    epoch 101| loss: 0.04156 | val_0_rmse: 13.88934| val_0_mae: 4.19471 | val_0_mse: 192.91374|  0:00:12s
    epoch 102| loss: 0.04166 | val_0_rmse: 12.00377| val_0_mae: 3.76466 | val_0_mse: 144.09055|  0:00:12s
    epoch 103| loss: 0.042   | val_0_rmse: 8.96231 | val_0_mae: 2.86127 | val_0_mse: 80.32294|  0:00:12s
    epoch 104| loss: 0.04017 | val_0_rmse: 6.96366 | val_0_mae: 2.17401 | val_0_mse: 48.49261|  0:00:12s
    epoch 105| loss: 0.04107 | val_0_rmse: 5.33025 | val_0_mae: 1.87934 | val_0_mse: 28.41155|  0:00:12s
    epoch 106| loss: 0.04311 | val_0_rmse: 5.58543 | val_0_mae: 1.90737 | val_0_mse: 31.19707|  0:00:12s
    epoch 107| loss: 0.04176 | val_0_rmse: 4.11186 | val_0_mae: 1.50189 | val_0_mse: 16.9074 |  0:00:12s
    epoch 108| loss: 0.04249 | val_0_rmse: 3.27199 | val_0_mae: 1.18785 | val_0_mse: 10.70593|  0:00:13s
    epoch 109| loss: 0.04108 | val_0_rmse: 2.76411 | val_0_mae: 1.16505 | val_0_mse: 7.64033 |  0:00:13s
    epoch 110| loss: 0.04268 | val_0_rmse: 2.88498 | val_0_mae: 1.15703 | val_0_mse: 8.3231  |  0:00:13s
    epoch 111| loss: 0.04007 | val_0_rmse: 2.93335 | val_0_mae: 1.1801  | val_0_mse: 8.60454 |  0:00:13s
    epoch 112| loss: 0.04084 | val_0_rmse: 3.30841 | val_0_mae: 1.37333 | val_0_mse: 10.94556|  0:00:13s
    epoch 113| loss: 0.03932 | val_0_rmse: 3.08353 | val_0_mae: 1.38822 | val_0_mse: 9.50818 |  0:00:13s
    epoch 114| loss: 0.0392  | val_0_rmse: 2.40866 | val_0_mae: 1.27828 | val_0_mse: 5.80162 |  0:00:13s
    epoch 115| loss: 0.03986 | val_0_rmse: 2.34336 | val_0_mae: 1.20459 | val_0_mse: 5.49133 |  0:00:14s
    epoch 116| loss: 0.04004 | val_0_rmse: 2.14854 | val_0_mae: 1.18351 | val_0_mse: 4.61623 |  0:00:14s
    epoch 117| loss: 0.04074 | val_0_rmse: 1.63239 | val_0_mae: 1.19084 | val_0_mse: 2.6647  |  0:00:14s
    epoch 118| loss: 0.03942 | val_0_rmse: 1.54069 | val_0_mae: 1.11384 | val_0_mse: 2.37374 |  0:00:14s
    epoch 119| loss: 0.0397  | val_0_rmse: 1.3732  | val_0_mae: 0.97756 | val_0_mse: 1.88567 |  0:00:14s
    epoch 120| loss: 0.03971 | val_0_rmse: 1.24538 | val_0_mae: 1.04725 | val_0_mse: 1.55097 |  0:00:14s
    epoch 121| loss: 0.04126 | val_0_rmse: 1.51199 | val_0_mae: 1.13081 | val_0_mse: 2.28613 |  0:00:14s
    epoch 122| loss: 0.03953 | val_0_rmse: 1.88914 | val_0_mae: 1.05407 | val_0_mse: 3.56884 |  0:00:15s
    epoch 123| loss: 0.03998 | val_0_rmse: 2.728   | val_0_mae: 1.22569 | val_0_mse: 7.44197 |  0:00:15s
    epoch 124| loss: 0.0389  | val_0_rmse: 1.99089 | val_0_mae: 1.02914 | val_0_mse: 3.96362 |  0:00:15s
    epoch 125| loss: 0.0399  | val_0_rmse: 1.80154 | val_0_mae: 0.85352 | val_0_mse: 3.24554 |  0:00:15s
    epoch 126| loss: 0.0369  | val_0_rmse: 1.58745 | val_0_mae: 0.76764 | val_0_mse: 2.51999 |  0:00:15s
    epoch 127| loss: 0.03636 | val_0_rmse: 1.82137 | val_0_mae: 0.80372 | val_0_mse: 3.3174  |  0:00:15s
    epoch 128| loss: 0.03824 | val_0_rmse: 1.17375 | val_0_mae: 0.69836 | val_0_mse: 1.3777  |  0:00:15s
    epoch 129| loss: 0.03831 | val_0_rmse: 1.1893  | val_0_mae: 0.60803 | val_0_mse: 1.41443 |  0:00:15s
    epoch 130| loss: 0.03992 | val_0_rmse: 1.29566 | val_0_mae: 0.58228 | val_0_mse: 1.67874 |  0:00:15s
    epoch 131| loss: 0.03822 | val_0_rmse: 1.22216 | val_0_mae: 0.54659 | val_0_mse: 1.49368 |  0:00:16s
    epoch 132| loss: 0.03936 | val_0_rmse: 0.74041 | val_0_mae: 0.47454 | val_0_mse: 0.54821 |  0:00:16s
    epoch 133| loss: 0.03748 | val_0_rmse: 0.70376 | val_0_mae: 0.40986 | val_0_mse: 0.49528 |  0:00:16s
    epoch 134| loss: 0.03634 | val_0_rmse: 0.88346 | val_0_mae: 0.40126 | val_0_mse: 0.7805  |  0:00:16s
    epoch 135| loss: 0.03676 | val_0_rmse: 0.77304 | val_0_mae: 0.41318 | val_0_mse: 0.59759 |  0:00:16s
    epoch 136| loss: 0.03705 | val_0_rmse: 0.47957 | val_0_mae: 0.38016 | val_0_mse: 0.22999 |  0:00:16s
    epoch 137| loss: 0.03547 | val_0_rmse: 0.93295 | val_0_mae: 0.44532 | val_0_mse: 0.87039 |  0:00:16s
    epoch 138| loss: 0.03852 | val_0_rmse: 0.93436 | val_0_mae: 0.48602 | val_0_mse: 0.87302 |  0:00:16s
    epoch 139| loss: 0.03622 | val_0_rmse: 0.73478 | val_0_mae: 0.46226 | val_0_mse: 0.5399  |  0:00:17s
    epoch 140| loss: 0.03654 | val_0_rmse: 0.76167 | val_0_mae: 0.47894 | val_0_mse: 0.58014 |  0:00:17s
    epoch 141| loss: 0.03685 | val_0_rmse: 0.7326  | val_0_mae: 0.48801 | val_0_mse: 0.5367  |  0:00:17s
    epoch 142| loss: 0.03731 | val_0_rmse: 0.66537 | val_0_mae: 0.45774 | val_0_mse: 0.44272 |  0:00:17s
    epoch 143| loss: 0.0359  | val_0_rmse: 0.63206 | val_0_mae: 0.44223 | val_0_mse: 0.3995  |  0:00:17s
    epoch 144| loss: 0.03556 | val_0_rmse: 0.59902 | val_0_mae: 0.44623 | val_0_mse: 0.35883 |  0:00:17s
    epoch 145| loss: 0.03472 | val_0_rmse: 0.70291 | val_0_mae: 0.46537 | val_0_mse: 0.49408 |  0:00:17s
    epoch 146| loss: 0.03797 | val_0_rmse: 0.59229 | val_0_mae: 0.44402 | val_0_mse: 0.35081 |  0:00:17s
    epoch 147| loss: 0.03613 | val_0_rmse: 0.62103 | val_0_mae: 0.47451 | val_0_mse: 0.38567 |  0:00:18s
    epoch 148| loss: 0.03585 | val_0_rmse: 0.67554 | val_0_mae: 0.50287 | val_0_mse: 0.45635 |  0:00:18s
    epoch 149| loss: 0.03706 | val_0_rmse: 0.63571 | val_0_mae: 0.4844  | val_0_mse: 0.40412 |  0:00:18s
    epoch 150| loss: 0.03518 | val_0_rmse: 0.64898 | val_0_mae: 0.48391 | val_0_mse: 0.42118 |  0:00:18s
    epoch 151| loss: 0.03536 | val_0_rmse: 0.70638 | val_0_mae: 0.54667 | val_0_mse: 0.49897 |  0:00:18s
    epoch 152| loss: 0.03581 | val_0_rmse: 0.72528 | val_0_mae: 0.57351 | val_0_mse: 0.52603 |  0:00:18s
    epoch 153| loss: 0.03455 | val_0_rmse: 0.75243 | val_0_mae: 0.57947 | val_0_mse: 0.56615 |  0:00:18s
    epoch 154| loss: 0.03466 | val_0_rmse: 0.96588 | val_0_mae: 0.60594 | val_0_mse: 0.93293 |  0:00:18s
    epoch 155| loss: 0.03412 | val_0_rmse: 0.9091  | val_0_mae: 0.61878 | val_0_mse: 0.82647 |  0:00:19s
    epoch 156| loss: 0.03456 | val_0_rmse: 1.50648 | val_0_mae: 0.7132  | val_0_mse: 2.26949 |  0:00:19s
    epoch 157| loss: 0.03446 | val_0_rmse: 1.55436 | val_0_mae: 0.7058  | val_0_mse: 2.41602 |  0:00:19s
    epoch 158| loss: 0.03429 | val_0_rmse: 1.48229 | val_0_mae: 0.69327 | val_0_mse: 2.19719 |  0:00:19s
    epoch 159| loss: 0.0359  | val_0_rmse: 1.0206  | val_0_mae: 0.58835 | val_0_mse: 1.04163 |  0:00:19s
    epoch 160| loss: 0.03449 | val_0_rmse: 0.98109 | val_0_mae: 0.58181 | val_0_mse: 0.96253 |  0:00:19s
    epoch 161| loss: 0.0325  | val_0_rmse: 0.70599 | val_0_mae: 0.52256 | val_0_mse: 0.49842 |  0:00:19s
    epoch 162| loss: 0.0336  | val_0_rmse: 0.72477 | val_0_mae: 0.49793 | val_0_mse: 0.5253  |  0:00:19s
    epoch 163| loss: 0.03492 | val_0_rmse: 0.70313 | val_0_mae: 0.48662 | val_0_mse: 0.4944  |  0:00:19s
    epoch 164| loss: 0.03442 | val_0_rmse: 0.60315 | val_0_mae: 0.47193 | val_0_mse: 0.36379 |  0:00:20s
    epoch 165| loss: 0.03366 | val_0_rmse: 0.54118 | val_0_mae: 0.43481 | val_0_mse: 0.29288 |  0:00:20s
    epoch 166| loss: 0.03363 | val_0_rmse: 0.5568  | val_0_mae: 0.44904 | val_0_mse: 0.31003 |  0:00:20s
    epoch 167| loss: 0.03245 | val_0_rmse: 0.56859 | val_0_mae: 0.462   | val_0_mse: 0.3233  |  0:00:20s
    epoch 168| loss: 0.03196 | val_0_rmse: 0.53506 | val_0_mae: 0.43375 | val_0_mse: 0.28629 |  0:00:20s
    epoch 169| loss: 0.03392 | val_0_rmse: 0.54854 | val_0_mae: 0.44579 | val_0_mse: 0.30089 |  0:00:20s
    epoch 170| loss: 0.03223 | val_0_rmse: 0.58659 | val_0_mae: 0.48044 | val_0_mse: 0.34409 |  0:00:20s
    epoch 171| loss: 0.03299 | val_0_rmse: 0.55869 | val_0_mae: 0.45294 | val_0_mse: 0.31214 |  0:00:20s
    epoch 172| loss: 0.03064 | val_0_rmse: 0.53096 | val_0_mae: 0.42783 | val_0_mse: 0.28192 |  0:00:21s
    epoch 173| loss: 0.03298 | val_0_rmse: 0.56249 | val_0_mae: 0.44689 | val_0_mse: 0.3164  |  0:00:21s
    epoch 174| loss: 0.0333  | val_0_rmse: 0.57485 | val_0_mae: 0.44653 | val_0_mse: 0.33045 |  0:00:21s
    epoch 175| loss: 0.03325 | val_0_rmse: 0.55614 | val_0_mae: 0.42506 | val_0_mse: 0.30929 |  0:00:21s
    epoch 176| loss: 0.0345  | val_0_rmse: 0.56821 | val_0_mae: 0.43902 | val_0_mse: 0.32286 |  0:00:21s
    epoch 177| loss: 0.02954 | val_0_rmse: 0.56303 | val_0_mae: 0.43618 | val_0_mse: 0.317   |  0:00:21s
    epoch 178| loss: 0.03335 | val_0_rmse: 0.54933 | val_0_mae: 0.417   | val_0_mse: 0.30176 |  0:00:21s
    epoch 179| loss: 0.03105 | val_0_rmse: 0.58021 | val_0_mae: 0.42189 | val_0_mse: 0.33665 |  0:00:21s
    epoch 180| loss: 0.0318  | val_0_rmse: 0.55777 | val_0_mae: 0.42115 | val_0_mse: 0.31111 |  0:00:21s
    epoch 181| loss: 0.03071 | val_0_rmse: 0.54912 | val_0_mae: 0.40863 | val_0_mse: 0.30154 |  0:00:22s
    epoch 182| loss: 0.03186 | val_0_rmse: 0.5759  | val_0_mae: 0.40376 | val_0_mse: 0.33166 |  0:00:22s
    epoch 183| loss: 0.03127 | val_0_rmse: 0.63332 | val_0_mae: 0.41743 | val_0_mse: 0.4011  |  0:00:22s
    epoch 184| loss: 0.03205 | val_0_rmse: 0.62939 | val_0_mae: 0.43    | val_0_mse: 0.39613 |  0:00:22s
    epoch 185| loss: 0.03064 | val_0_rmse: 0.60842 | val_0_mae: 0.43605 | val_0_mse: 0.37018 |  0:00:22s
    epoch 186| loss: 0.0326  | val_0_rmse: 0.61891 | val_0_mae: 0.43393 | val_0_mse: 0.38304 |  0:00:22s
    epoch 187| loss: 0.03153 | val_0_rmse: 0.62082 | val_0_mae: 0.44062 | val_0_mse: 0.38542 |  0:00:22s
    epoch 188| loss: 0.03131 | val_0_rmse: 0.60461 | val_0_mae: 0.43041 | val_0_mse: 0.36556 |  0:00:23s
    epoch 189| loss: 0.03115 | val_0_rmse: 0.61048 | val_0_mae: 0.42865 | val_0_mse: 0.37269 |  0:00:23s
    epoch 190| loss: 0.03179 | val_0_rmse: 0.67433 | val_0_mae: 0.43676 | val_0_mse: 0.45473 |  0:00:23s
    epoch 191| loss: 0.03127 | val_0_rmse: 0.65685 | val_0_mae: 0.42459 | val_0_mse: 0.43146 |  0:00:23s
    epoch 192| loss: 0.03107 | val_0_rmse: 0.55915 | val_0_mae: 0.39562 | val_0_mse: 0.31265 |  0:00:23s
    epoch 193| loss: 0.03002 | val_0_rmse: 0.50978 | val_0_mae: 0.39267 | val_0_mse: 0.25988 |  0:00:23s
    epoch 194| loss: 0.03062 | val_0_rmse: 0.48679 | val_0_mae: 0.37154 | val_0_mse: 0.23696 |  0:00:23s
    epoch 195| loss: 0.03135 | val_0_rmse: 0.51614 | val_0_mae: 0.38241 | val_0_mse: 0.2664  |  0:00:23s
    epoch 196| loss: 0.02959 | val_0_rmse: 0.53476 | val_0_mae: 0.38683 | val_0_mse: 0.28597 |  0:00:23s
    epoch 197| loss: 0.03015 | val_0_rmse: 0.56402 | val_0_mae: 0.38578 | val_0_mse: 0.31812 |  0:00:24s
    epoch 198| loss: 0.03111 | val_0_rmse: 0.64577 | val_0_mae: 0.41001 | val_0_mse: 0.41702 |  0:00:24s
    epoch 199| loss: 0.03035 | val_0_rmse: 0.67178 | val_0_mae: 0.42122 | val_0_mse: 0.45129 |  0:00:24s
    epoch 200| loss: 0.0296  | val_0_rmse: 0.66242 | val_0_mae: 0.41489 | val_0_mse: 0.4388  |  0:00:24s
    epoch 201| loss: 0.02993 | val_0_rmse: 0.65125 | val_0_mae: 0.41073 | val_0_mse: 0.42413 |  0:00:24s
    epoch 202| loss: 0.03056 | val_0_rmse: 0.67805 | val_0_mae: 0.42831 | val_0_mse: 0.45975 |  0:00:24s
    epoch 203| loss: 0.03075 | val_0_rmse: 0.68151 | val_0_mae: 0.40859 | val_0_mse: 0.46446 |  0:00:24s
    epoch 204| loss: 0.03066 | val_0_rmse: 0.66279 | val_0_mae: 0.40519 | val_0_mse: 0.43928 |  0:00:24s
    epoch 205| loss: 0.02883 | val_0_rmse: 0.67397 | val_0_mae: 0.41907 | val_0_mse: 0.45424 |  0:00:25s
    epoch 206| loss: 0.02839 | val_0_rmse: 0.66402 | val_0_mae: 0.40858 | val_0_mse: 0.44093 |  0:00:25s
    epoch 207| loss: 0.02942 | val_0_rmse: 0.66213 | val_0_mae: 0.4077  | val_0_mse: 0.43842 |  0:00:25s
    epoch 208| loss: 0.02984 | val_0_rmse: 0.70162 | val_0_mae: 0.43622 | val_0_mse: 0.49227 |  0:00:25s
    epoch 209| loss: 0.02878 | val_0_rmse: 0.70029 | val_0_mae: 0.42288 | val_0_mse: 0.4904  |  0:00:25s
    epoch 210| loss: 0.02932 | val_0_rmse: 0.70795 | val_0_mae: 0.423   | val_0_mse: 0.5012  |  0:00:25s
    epoch 211| loss: 0.02924 | val_0_rmse: 0.76406 | val_0_mae: 0.46665 | val_0_mse: 0.58379 |  0:00:25s
    epoch 212| loss: 0.03071 | val_0_rmse: 0.80644 | val_0_mae: 0.46212 | val_0_mse: 0.65035 |  0:00:26s
    epoch 213| loss: 0.03121 | val_0_rmse: 0.78206 | val_0_mae: 0.45358 | val_0_mse: 0.61161 |  0:00:26s
    epoch 214| loss: 0.02882 | val_0_rmse: 0.80181 | val_0_mae: 0.48237 | val_0_mse: 0.6429  |  0:00:26s
    epoch 215| loss: 0.02962 | val_0_rmse: 0.75792 | val_0_mae: 0.45455 | val_0_mse: 0.57444 |  0:00:26s
    epoch 216| loss: 0.02999 | val_0_rmse: 0.72666 | val_0_mae: 0.43021 | val_0_mse: 0.52803 |  0:00:26s
    epoch 217| loss: 0.03008 | val_0_rmse: 0.75324 | val_0_mae: 0.45    | val_0_mse: 0.56736 |  0:00:26s
    epoch 218| loss: 0.03022 | val_0_rmse: 0.76183 | val_0_mae: 0.45234 | val_0_mse: 0.58038 |  0:00:26s
    epoch 219| loss: 0.02864 | val_0_rmse: 0.7866  | val_0_mae: 0.42374 | val_0_mse: 0.61874 |  0:00:26s
    epoch 220| loss: 0.02935 | val_0_rmse: 0.79231 | val_0_mae: 0.42252 | val_0_mse: 0.62775 |  0:00:27s
    epoch 221| loss: 0.0292  | val_0_rmse: 0.76821 | val_0_mae: 0.42291 | val_0_mse: 0.59015 |  0:00:27s
    epoch 222| loss: 0.02924 | val_0_rmse: 0.69764 | val_0_mae: 0.37998 | val_0_mse: 0.4867  |  0:00:27s
    epoch 223| loss: 0.02939 | val_0_rmse: 0.62631 | val_0_mae: 0.35902 | val_0_mse: 0.39226 |  0:00:27s
    epoch 224| loss: 0.02814 | val_0_rmse: 0.61999 | val_0_mae: 0.37477 | val_0_mse: 0.38439 |  0:00:27s
    epoch 225| loss: 0.02839 | val_0_rmse: 0.59119 | val_0_mae: 0.358   | val_0_mse: 0.3495  |  0:00:27s
    epoch 226| loss: 0.02873 | val_0_rmse: 0.56809 | val_0_mae: 0.34044 | val_0_mse: 0.32272 |  0:00:27s
    epoch 227| loss: 0.02839 | val_0_rmse: 0.58055 | val_0_mae: 0.35777 | val_0_mse: 0.33704 |  0:00:27s
    epoch 228| loss: 0.02736 | val_0_rmse: 0.54694 | val_0_mae: 0.34584 | val_0_mse: 0.29914 |  0:00:28s
    epoch 229| loss: 0.02803 | val_0_rmse: 0.54157 | val_0_mae: 0.33397 | val_0_mse: 0.29329 |  0:00:28s
    epoch 230| loss: 0.02878 | val_0_rmse: 0.52858 | val_0_mae: 0.33685 | val_0_mse: 0.2794  |  0:00:28s
    epoch 231| loss: 0.02799 | val_0_rmse: 0.48693 | val_0_mae: 0.32307 | val_0_mse: 0.2371  |  0:00:28s
    epoch 232| loss: 0.02785 | val_0_rmse: 0.46859 | val_0_mae: 0.31157 | val_0_mse: 0.21958 |  0:00:28s
    epoch 233| loss: 0.02718 | val_0_rmse: 0.48151 | val_0_mae: 0.32338 | val_0_mse: 0.23185 |  0:00:28s
    epoch 234| loss: 0.02799 | val_0_rmse: 0.46058 | val_0_mae: 0.30642 | val_0_mse: 0.21213 |  0:00:28s
    epoch 235| loss: 0.02829 | val_0_rmse: 0.41792 | val_0_mae: 0.29078 | val_0_mse: 0.17466 |  0:00:28s
    epoch 236| loss: 0.02763 | val_0_rmse: 0.41125 | val_0_mae: 0.29677 | val_0_mse: 0.16912 |  0:00:29s
    epoch 237| loss: 0.02672 | val_0_rmse: 0.39931 | val_0_mae: 0.29215 | val_0_mse: 0.15945 |  0:00:29s
    epoch 238| loss: 0.02796 | val_0_rmse: 0.39043 | val_0_mae: 0.28473 | val_0_mse: 0.15244 |  0:00:29s
    epoch 239| loss: 0.02797 | val_0_rmse: 0.3887  | val_0_mae: 0.28332 | val_0_mse: 0.15109 |  0:00:29s
    epoch 240| loss: 0.02689 | val_0_rmse: 0.3888  | val_0_mae: 0.2842  | val_0_mse: 0.15116 |  0:00:29s
    epoch 241| loss: 0.02781 | val_0_rmse: 0.38161 | val_0_mae: 0.27821 | val_0_mse: 0.14562 |  0:00:29s
    epoch 242| loss: 0.02686 | val_0_rmse: 0.36697 | val_0_mae: 0.26483 | val_0_mse: 0.13467 |  0:00:29s
    epoch 243| loss: 0.02744 | val_0_rmse: 0.37877 | val_0_mae: 0.2738  | val_0_mse: 0.14346 |  0:00:29s
    epoch 244| loss: 0.02733 | val_0_rmse: 0.38167 | val_0_mae: 0.27034 | val_0_mse: 0.14567 |  0:00:30s
    epoch 245| loss: 0.02739 | val_0_rmse: 0.40367 | val_0_mae: 0.27259 | val_0_mse: 0.16295 |  0:00:30s
    epoch 246| loss: 0.02718 | val_0_rmse: 0.42365 | val_0_mae: 0.27923 | val_0_mse: 0.17948 |  0:00:30s
    epoch 247| loss: 0.02909 | val_0_rmse: 0.43931 | val_0_mae: 0.28747 | val_0_mse: 0.19299 |  0:00:30s
    epoch 248| loss: 0.02798 | val_0_rmse: 0.40932 | val_0_mae: 0.26561 | val_0_mse: 0.16755 |  0:00:30s
    epoch 249| loss: 0.02786 | val_0_rmse: 0.38736 | val_0_mae: 0.26265 | val_0_mse: 0.15005 |  0:00:30s
    epoch 250| loss: 0.02604 | val_0_rmse: 0.37567 | val_0_mae: 0.2576  | val_0_mse: 0.14113 |  0:00:30s
    epoch 251| loss: 0.02788 | val_0_rmse: 0.38912 | val_0_mae: 0.25424 | val_0_mse: 0.15141 |  0:00:30s
    epoch 252| loss: 0.02786 | val_0_rmse: 0.37021 | val_0_mae: 0.24488 | val_0_mse: 0.13705 |  0:00:31s
    epoch 253| loss: 0.02604 | val_0_rmse: 0.36042 | val_0_mae: 0.24306 | val_0_mse: 0.1299  |  0:00:31s
    epoch 254| loss: 0.02658 | val_0_rmse: 0.35192 | val_0_mae: 0.23338 | val_0_mse: 0.12385 |  0:00:31s
    epoch 255| loss: 0.02653 | val_0_rmse: 0.34314 | val_0_mae: 0.2238  | val_0_mse: 0.11775 |  0:00:31s
    epoch 256| loss: 0.02664 | val_0_rmse: 0.34042 | val_0_mae: 0.21707 | val_0_mse: 0.11588 |  0:00:31s
    epoch 257| loss: 0.02765 | val_0_rmse: 0.34017 | val_0_mae: 0.2119  | val_0_mse: 0.11572 |  0:00:31s
    epoch 258| loss: 0.0255  | val_0_rmse: 0.3316  | val_0_mae: 0.20485 | val_0_mse: 0.10996 |  0:00:31s
    epoch 259| loss: 0.02718 | val_0_rmse: 0.33236 | val_0_mae: 0.20753 | val_0_mse: 0.11046 |  0:00:32s
    epoch 260| loss: 0.02534 | val_0_rmse: 0.33346 | val_0_mae: 0.205   | val_0_mse: 0.1112  |  0:00:32s
    epoch 261| loss: 0.02674 | val_0_rmse: 0.31879 | val_0_mae: 0.19849 | val_0_mse: 0.10163 |  0:00:32s
    epoch 262| loss: 0.0273  | val_0_rmse: 0.32433 | val_0_mae: 0.20396 | val_0_mse: 0.10519 |  0:00:32s
    epoch 263| loss: 0.02657 | val_0_rmse: 0.33132 | val_0_mae: 0.20798 | val_0_mse: 0.10977 |  0:00:32s
    epoch 264| loss: 0.02695 | val_0_rmse: 0.31972 | val_0_mae: 0.19816 | val_0_mse: 0.10222 |  0:00:32s
    epoch 265| loss: 0.02599 | val_0_rmse: 0.31861 | val_0_mae: 0.1989  | val_0_mse: 0.10152 |  0:00:32s
    epoch 266| loss: 0.02662 | val_0_rmse: 0.32396 | val_0_mae: 0.20035 | val_0_mse: 0.10495 |  0:00:32s
    epoch 267| loss: 0.02607 | val_0_rmse: 0.31897 | val_0_mae: 0.19419 | val_0_mse: 0.10174 |  0:00:33s
    epoch 268| loss: 0.02651 | val_0_rmse: 0.32504 | val_0_mae: 0.19617 | val_0_mse: 0.10565 |  0:00:33s
    epoch 269| loss: 0.02698 | val_0_rmse: 0.32044 | val_0_mae: 0.19439 | val_0_mse: 0.10268 |  0:00:33s
    epoch 270| loss: 0.02547 | val_0_rmse: 0.30425 | val_0_mae: 0.18968 | val_0_mse: 0.09257 |  0:00:33s
    epoch 271| loss: 0.02467 | val_0_rmse: 0.30461 | val_0_mae: 0.19287 | val_0_mse: 0.09279 |  0:00:33s
    epoch 272| loss: 0.02726 | val_0_rmse: 0.29641 | val_0_mae: 0.18943 | val_0_mse: 0.08786 |  0:00:33s
    epoch 273| loss: 0.02557 | val_0_rmse: 0.29593 | val_0_mae: 0.18686 | val_0_mse: 0.08758 |  0:00:33s
    epoch 274| loss: 0.02507 | val_0_rmse: 0.29843 | val_0_mae: 0.18906 | val_0_mse: 0.08906 |  0:00:33s
    epoch 275| loss: 0.02574 | val_0_rmse: 0.29306 | val_0_mae: 0.18709 | val_0_mse: 0.08589 |  0:00:34s
    epoch 276| loss: 0.02494 | val_0_rmse: 0.28058 | val_0_mae: 0.18182 | val_0_mse: 0.07873 |  0:00:34s
    epoch 277| loss: 0.02508 | val_0_rmse: 0.2762  | val_0_mae: 0.18166 | val_0_mse: 0.07629 |  0:00:34s
    epoch 278| loss: 0.02477 | val_0_rmse: 0.26914 | val_0_mae: 0.1793  | val_0_mse: 0.07244 |  0:00:34s
    epoch 279| loss: 0.02525 | val_0_rmse: 0.26621 | val_0_mae: 0.17642 | val_0_mse: 0.07087 |  0:00:34s
    epoch 280| loss: 0.02541 | val_0_rmse: 0.26318 | val_0_mae: 0.17522 | val_0_mse: 0.06926 |  0:00:34s
    epoch 281| loss: 0.02433 | val_0_rmse: 0.25616 | val_0_mae: 0.17082 | val_0_mse: 0.06562 |  0:00:34s
    epoch 282| loss: 0.02525 | val_0_rmse: 0.25697 | val_0_mae: 0.17004 | val_0_mse: 0.06603 |  0:00:34s
    epoch 283| loss: 0.02494 | val_0_rmse: 0.26136 | val_0_mae: 0.17086 | val_0_mse: 0.06831 |  0:00:35s
    epoch 284| loss: 0.02557 | val_0_rmse: 0.26526 | val_0_mae: 0.17153 | val_0_mse: 0.07036 |  0:00:35s
    epoch 285| loss: 0.02578 | val_0_rmse: 0.26819 | val_0_mae: 0.17233 | val_0_mse: 0.07193 |  0:00:35s
    epoch 286| loss: 0.02527 | val_0_rmse: 0.26231 | val_0_mae: 0.17162 | val_0_mse: 0.06881 |  0:00:35s
    epoch 287| loss: 0.02597 | val_0_rmse: 0.25254 | val_0_mae: 0.16856 | val_0_mse: 0.06378 |  0:00:35s
    epoch 288| loss: 0.02559 | val_0_rmse: 0.24474 | val_0_mae: 0.16594 | val_0_mse: 0.0599  |  0:00:35s
    epoch 289| loss: 0.02535 | val_0_rmse: 0.24034 | val_0_mae: 0.16414 | val_0_mse: 0.05777 |  0:00:35s
    epoch 290| loss: 0.02515 | val_0_rmse: 0.23605 | val_0_mae: 0.16266 | val_0_mse: 0.05572 |  0:00:35s
    epoch 291| loss: 0.02473 | val_0_rmse: 0.23451 | val_0_mae: 0.16238 | val_0_mse: 0.055   |  0:00:36s
    epoch 292| loss: 0.02569 | val_0_rmse: 0.23528 | val_0_mae: 0.16373 | val_0_mse: 0.05536 |  0:00:36s
    epoch 293| loss: 0.02429 | val_0_rmse: 0.23692 | val_0_mae: 0.16402 | val_0_mse: 0.05613 |  0:00:36s
    epoch 294| loss: 0.0259  | val_0_rmse: 0.2295  | val_0_mae: 0.15917 | val_0_mse: 0.05267 |  0:00:36s
    epoch 295| loss: 0.02402 | val_0_rmse: 0.23282 | val_0_mae: 0.1617  | val_0_mse: 0.05421 |  0:00:36s
    epoch 296| loss: 0.02481 | val_0_rmse: 0.23359 | val_0_mae: 0.161   | val_0_mse: 0.05457 |  0:00:36s
    epoch 297| loss: 0.0251  | val_0_rmse: 0.23284 | val_0_mae: 0.16129 | val_0_mse: 0.05422 |  0:00:37s
    epoch 298| loss: 0.02351 | val_0_rmse: 0.23426 | val_0_mae: 0.16327 | val_0_mse: 0.05488 |  0:00:37s
    epoch 299| loss: 0.02349 | val_0_rmse: 0.23001 | val_0_mae: 0.16074 | val_0_mse: 0.0529  |  0:00:37s
    epoch 300| loss: 0.02391 | val_0_rmse: 0.22666 | val_0_mae: 0.1585  | val_0_mse: 0.05138 |  0:00:37s
    epoch 301| loss: 0.02466 | val_0_rmse: 0.22834 | val_0_mae: 0.16038 | val_0_mse: 0.05214 |  0:00:37s
    epoch 302| loss: 0.02613 | val_0_rmse: 0.22606 | val_0_mae: 0.15769 | val_0_mse: 0.0511  |  0:00:37s
    epoch 303| loss: 0.0245  | val_0_rmse: 0.22424 | val_0_mae: 0.15621 | val_0_mse: 0.05028 |  0:00:37s
    epoch 304| loss: 0.02444 | val_0_rmse: 0.225   | val_0_mae: 0.15636 | val_0_mse: 0.05063 |  0:00:38s
    epoch 305| loss: 0.02424 | val_0_rmse: 0.2221  | val_0_mae: 0.15406 | val_0_mse: 0.04933 |  0:00:38s
    epoch 306| loss: 0.02441 | val_0_rmse: 0.2181  | val_0_mae: 0.15078 | val_0_mse: 0.04757 |  0:00:38s
    epoch 307| loss: 0.0249  | val_0_rmse: 0.21618 | val_0_mae: 0.15121 | val_0_mse: 0.04673 |  0:00:38s
    epoch 308| loss: 0.02429 | val_0_rmse: 0.21364 | val_0_mae: 0.14954 | val_0_mse: 0.04564 |  0:00:38s
    epoch 309| loss: 0.02353 | val_0_rmse: 0.21488 | val_0_mae: 0.15042 | val_0_mse: 0.04617 |  0:00:38s
    epoch 310| loss: 0.02381 | val_0_rmse: 0.21579 | val_0_mae: 0.15185 | val_0_mse: 0.04656 |  0:00:39s
    epoch 311| loss: 0.02485 | val_0_rmse: 0.21724 | val_0_mae: 0.15234 | val_0_mse: 0.04719 |  0:00:39s
    epoch 312| loss: 0.02438 | val_0_rmse: 0.21672 | val_0_mae: 0.15172 | val_0_mse: 0.04697 |  0:00:39s
    epoch 313| loss: 0.02464 | val_0_rmse: 0.21954 | val_0_mae: 0.15319 | val_0_mse: 0.0482  |  0:00:39s
    epoch 314| loss: 0.02266 | val_0_rmse: 0.21695 | val_0_mae: 0.15113 | val_0_mse: 0.04707 |  0:00:39s
    epoch 315| loss: 0.02385 | val_0_rmse: 0.21795 | val_0_mae: 0.15145 | val_0_mse: 0.0475  |  0:00:39s
    epoch 316| loss: 0.02323 | val_0_rmse: 0.22278 | val_0_mae: 0.1537  | val_0_mse: 0.04963 |  0:00:39s
    epoch 317| loss: 0.02308 | val_0_rmse: 0.22302 | val_0_mae: 0.15296 | val_0_mse: 0.04974 |  0:00:39s
    epoch 318| loss: 0.0241  | val_0_rmse: 0.21991 | val_0_mae: 0.15053 | val_0_mse: 0.04836 |  0:00:40s
    epoch 319| loss: 0.02407 | val_0_rmse: 0.21847 | val_0_mae: 0.15111 | val_0_mse: 0.04773 |  0:00:40s
    epoch 320| loss: 0.0244  | val_0_rmse: 0.2145  | val_0_mae: 0.14859 | val_0_mse: 0.04601 |  0:00:40s
    epoch 321| loss: 0.02368 | val_0_rmse: 0.21478 | val_0_mae: 0.14962 | val_0_mse: 0.04613 |  0:00:40s
    epoch 322| loss: 0.02327 | val_0_rmse: 0.21359 | val_0_mae: 0.14876 | val_0_mse: 0.04562 |  0:00:40s
    epoch 323| loss: 0.0239  | val_0_rmse: 0.21191 | val_0_mae: 0.14736 | val_0_mse: 0.0449  |  0:00:40s
    epoch 324| loss: 0.02366 | val_0_rmse: 0.21187 | val_0_mae: 0.14827 | val_0_mse: 0.04489 |  0:00:40s
    epoch 325| loss: 0.0243  | val_0_rmse: 0.21267 | val_0_mae: 0.1494  | val_0_mse: 0.04523 |  0:00:40s
    epoch 326| loss: 0.02373 | val_0_rmse: 0.21132 | val_0_mae: 0.14847 | val_0_mse: 0.04466 |  0:00:40s
    epoch 327| loss: 0.02537 | val_0_rmse: 0.21167 | val_0_mae: 0.14966 | val_0_mse: 0.0448  |  0:00:41s
    epoch 328| loss: 0.02389 | val_0_rmse: 0.20965 | val_0_mae: 0.14707 | val_0_mse: 0.04395 |  0:00:41s
    epoch 329| loss: 0.02425 | val_0_rmse: 0.21026 | val_0_mae: 0.14676 | val_0_mse: 0.04421 |  0:00:41s
    epoch 330| loss: 0.02398 | val_0_rmse: 0.21397 | val_0_mae: 0.14962 | val_0_mse: 0.04578 |  0:00:41s
    epoch 331| loss: 0.02312 | val_0_rmse: 0.21131 | val_0_mae: 0.14723 | val_0_mse: 0.04465 |  0:00:41s
    epoch 332| loss: 0.02374 | val_0_rmse: 0.21248 | val_0_mae: 0.14859 | val_0_mse: 0.04515 |  0:00:41s
    epoch 333| loss: 0.02256 | val_0_rmse: 0.21287 | val_0_mae: 0.14835 | val_0_mse: 0.04531 |  0:00:41s
    epoch 334| loss: 0.02245 | val_0_rmse: 0.21385 | val_0_mae: 0.1486  | val_0_mse: 0.04573 |  0:00:41s
    epoch 335| loss: 0.02301 | val_0_rmse: 0.21741 | val_0_mae: 0.15086 | val_0_mse: 0.04727 |  0:00:42s
    epoch 336| loss: 0.02249 | val_0_rmse: 0.21355 | val_0_mae: 0.14843 | val_0_mse: 0.0456  |  0:00:42s
    epoch 337| loss: 0.0236  | val_0_rmse: 0.2146  | val_0_mae: 0.14966 | val_0_mse: 0.04605 |  0:00:42s
    epoch 338| loss: 0.02201 | val_0_rmse: 0.21729 | val_0_mae: 0.1523  | val_0_mse: 0.04721 |  0:00:42s
    epoch 339| loss: 0.02339 | val_0_rmse: 0.21298 | val_0_mae: 0.14848 | val_0_mse: 0.04536 |  0:00:42s
    epoch 340| loss: 0.02348 | val_0_rmse: 0.21171 | val_0_mae: 0.1471  | val_0_mse: 0.04482 |  0:00:42s
    epoch 341| loss: 0.02368 | val_0_rmse: 0.2109  | val_0_mae: 0.14678 | val_0_mse: 0.04448 |  0:00:42s
    epoch 342| loss: 0.02443 | val_0_rmse: 0.20781 | val_0_mae: 0.14508 | val_0_mse: 0.04318 |  0:00:42s
    epoch 343| loss: 0.02333 | val_0_rmse: 0.20413 | val_0_mae: 0.14297 | val_0_mse: 0.04167 |  0:00:42s
    epoch 344| loss: 0.02225 | val_0_rmse: 0.20257 | val_0_mae: 0.14214 | val_0_mse: 0.04103 |  0:00:43s
    epoch 345| loss: 0.02384 | val_0_rmse: 0.20108 | val_0_mae: 0.14083 | val_0_mse: 0.04043 |  0:00:43s
    epoch 346| loss: 0.02152 | val_0_rmse: 0.19889 | val_0_mae: 0.13962 | val_0_mse: 0.03956 |  0:00:43s
    epoch 347| loss: 0.02197 | val_0_rmse: 0.19608 | val_0_mae: 0.13923 | val_0_mse: 0.03845 |  0:00:43s
    epoch 348| loss: 0.02324 | val_0_rmse: 0.19488 | val_0_mae: 0.13855 | val_0_mse: 0.03798 |  0:00:43s
    epoch 349| loss: 0.02377 | val_0_rmse: 0.1963  | val_0_mae: 0.13843 | val_0_mse: 0.03854 |  0:00:43s
    epoch 350| loss: 0.02262 | val_0_rmse: 0.19453 | val_0_mae: 0.13795 | val_0_mse: 0.03784 |  0:00:43s
    epoch 351| loss: 0.02355 | val_0_rmse: 0.1939  | val_0_mae: 0.1375  | val_0_mse: 0.0376  |  0:00:44s
    epoch 352| loss: 0.0229  | val_0_rmse: 0.19492 | val_0_mae: 0.13815 | val_0_mse: 0.038   |  0:00:44s
    epoch 353| loss: 0.02247 | val_0_rmse: 0.19357 | val_0_mae: 0.13761 | val_0_mse: 0.03747 |  0:00:44s
    epoch 354| loss: 0.02258 | val_0_rmse: 0.19437 | val_0_mae: 0.13793 | val_0_mse: 0.03778 |  0:00:44s
    epoch 355| loss: 0.02321 | val_0_rmse: 0.19489 | val_0_mae: 0.13796 | val_0_mse: 0.03798 |  0:00:44s
    epoch 356| loss: 0.02327 | val_0_rmse: 0.19477 | val_0_mae: 0.13865 | val_0_mse: 0.03793 |  0:00:44s
    epoch 357| loss: 0.02312 | val_0_rmse: 0.19448 | val_0_mae: 0.13758 | val_0_mse: 0.03782 |  0:00:44s
    epoch 358| loss: 0.02329 | val_0_rmse: 0.19361 | val_0_mae: 0.13748 | val_0_mse: 0.03749 |  0:00:44s
    epoch 359| loss: 0.02239 | val_0_rmse: 0.19364 | val_0_mae: 0.13787 | val_0_mse: 0.03749 |  0:00:45s
    epoch 360| loss: 0.02227 | val_0_rmse: 0.19354 | val_0_mae: 0.13741 | val_0_mse: 0.03746 |  0:00:45s
    epoch 361| loss: 0.02266 | val_0_rmse: 0.19329 | val_0_mae: 0.13717 | val_0_mse: 0.03736 |  0:00:45s
    epoch 362| loss: 0.02181 | val_0_rmse: 0.19336 | val_0_mae: 0.13761 | val_0_mse: 0.03739 |  0:00:45s
    epoch 363| loss: 0.02242 | val_0_rmse: 0.19389 | val_0_mae: 0.13716 | val_0_mse: 0.03759 |  0:00:45s
    epoch 364| loss: 0.02236 | val_0_rmse: 0.19579 | val_0_mae: 0.14016 | val_0_mse: 0.03833 |  0:00:45s
    epoch 365| loss: 0.02204 | val_0_rmse: 0.19426 | val_0_mae: 0.13787 | val_0_mse: 0.03774 |  0:00:45s
    epoch 366| loss: 0.02156 | val_0_rmse: 0.19456 | val_0_mae: 0.13773 | val_0_mse: 0.03785 |  0:00:45s
    epoch 367| loss: 0.02207 | val_0_rmse: 0.19476 | val_0_mae: 0.13814 | val_0_mse: 0.03793 |  0:00:45s
    epoch 368| loss: 0.02301 | val_0_rmse: 0.19297 | val_0_mae: 0.13584 | val_0_mse: 0.03724 |  0:00:46s
    epoch 369| loss: 0.02174 | val_0_rmse: 0.19287 | val_0_mae: 0.13548 | val_0_mse: 0.0372  |  0:00:46s
    epoch 370| loss: 0.02194 | val_0_rmse: 0.19265 | val_0_mae: 0.13625 | val_0_mse: 0.03712 |  0:00:46s
    epoch 371| loss: 0.0222  | val_0_rmse: 0.19264 | val_0_mae: 0.13594 | val_0_mse: 0.03711 |  0:00:46s
    epoch 372| loss: 0.02275 | val_0_rmse: 0.19221 | val_0_mae: 0.13573 | val_0_mse: 0.03695 |  0:00:46s
    epoch 373| loss: 0.02193 | val_0_rmse: 0.19203 | val_0_mae: 0.13628 | val_0_mse: 0.03687 |  0:00:47s
    epoch 374| loss: 0.01996 | val_0_rmse: 0.192   | val_0_mae: 0.1356  | val_0_mse: 0.03686 |  0:00:47s
    epoch 375| loss: 0.02172 | val_0_rmse: 0.19237 | val_0_mae: 0.13598 | val_0_mse: 0.03701 |  0:00:47s
    epoch 376| loss: 0.02186 | val_0_rmse: 0.19167 | val_0_mae: 0.13521 | val_0_mse: 0.03674 |  0:00:47s
    epoch 377| loss: 0.02089 | val_0_rmse: 0.19224 | val_0_mae: 0.13527 | val_0_mse: 0.03696 |  0:00:47s
    epoch 378| loss: 0.02116 | val_0_rmse: 0.19283 | val_0_mae: 0.13485 | val_0_mse: 0.03718 |  0:00:47s
    epoch 379| loss: 0.02094 | val_0_rmse: 0.19275 | val_0_mae: 0.13526 | val_0_mse: 0.03715 |  0:00:47s
    epoch 380| loss: 0.02102 | val_0_rmse: 0.19348 | val_0_mae: 0.13608 | val_0_mse: 0.03743 |  0:00:47s
    epoch 381| loss: 0.02145 | val_0_rmse: 0.19286 | val_0_mae: 0.13505 | val_0_mse: 0.03719 |  0:00:47s
    epoch 382| loss: 0.02217 | val_0_rmse: 0.19308 | val_0_mae: 0.13544 | val_0_mse: 0.03728 |  0:00:48s
    epoch 383| loss: 0.022   | val_0_rmse: 0.19505 | val_0_mae: 0.13815 | val_0_mse: 0.03805 |  0:00:48s
    epoch 384| loss: 0.02194 | val_0_rmse: 0.19516 | val_0_mae: 0.13778 | val_0_mse: 0.03809 |  0:00:48s
    epoch 385| loss: 0.02156 | val_0_rmse: 0.19387 | val_0_mae: 0.13693 | val_0_mse: 0.03759 |  0:00:48s
    epoch 386| loss: 0.02157 | val_0_rmse: 0.19349 | val_0_mae: 0.13658 | val_0_mse: 0.03744 |  0:00:48s
    epoch 387| loss: 0.02132 | val_0_rmse: 0.19392 | val_0_mae: 0.1365  | val_0_mse: 0.0376  |  0:00:48s
    epoch 388| loss: 0.0217  | val_0_rmse: 0.19339 | val_0_mae: 0.13631 | val_0_mse: 0.0374  |  0:00:48s
    epoch 389| loss: 0.02233 | val_0_rmse: 0.19241 | val_0_mae: 0.13541 | val_0_mse: 0.03702 |  0:00:48s
    epoch 390| loss: 0.02119 | val_0_rmse: 0.19259 | val_0_mae: 0.13534 | val_0_mse: 0.03709 |  0:00:49s
    epoch 391| loss: 0.02076 | val_0_rmse: 0.19115 | val_0_mae: 0.13482 | val_0_mse: 0.03654 |  0:00:49s
    epoch 392| loss: 0.02189 | val_0_rmse: 0.19113 | val_0_mae: 0.13486 | val_0_mse: 0.03653 |  0:00:49s
    epoch 393| loss: 0.02368 | val_0_rmse: 0.19265 | val_0_mae: 0.13583 | val_0_mse: 0.03711 |  0:00:49s
    epoch 394| loss: 0.02319 | val_0_rmse: 0.19308 | val_0_mae: 0.13692 | val_0_mse: 0.03728 |  0:00:49s
    epoch 395| loss: 0.02221 | val_0_rmse: 0.19279 | val_0_mae: 0.1365  | val_0_mse: 0.03717 |  0:00:49s
    epoch 396| loss: 0.02243 | val_0_rmse: 0.19105 | val_0_mae: 0.13473 | val_0_mse: 0.0365  |  0:00:49s
    epoch 397| loss: 0.02242 | val_0_rmse: 0.19092 | val_0_mae: 0.1338  | val_0_mse: 0.03645 |  0:00:49s
    epoch 398| loss: 0.02239 | val_0_rmse: 0.19328 | val_0_mae: 0.13601 | val_0_mse: 0.03736 |  0:00:49s
    epoch 399| loss: 0.02105 | val_0_rmse: 0.19158 | val_0_mae: 0.13564 | val_0_mse: 0.0367  |  0:00:50s
    epoch 400| loss: 0.02245 | val_0_rmse: 0.19088 | val_0_mae: 0.13474 | val_0_mse: 0.03644 |  0:00:50s
    epoch 401| loss: 0.02068 | val_0_rmse: 0.19068 | val_0_mae: 0.13405 | val_0_mse: 0.03636 |  0:00:50s
    epoch 402| loss: 0.02056 | val_0_rmse: 0.18936 | val_0_mae: 0.13368 | val_0_mse: 0.03586 |  0:00:50s
    epoch 403| loss: 0.0215  | val_0_rmse: 0.18947 | val_0_mae: 0.13391 | val_0_mse: 0.0359  |  0:00:50s
    epoch 404| loss: 0.02239 | val_0_rmse: 0.19156 | val_0_mae: 0.1345  | val_0_mse: 0.03669 |  0:00:50s
    epoch 405| loss: 0.02093 | val_0_rmse: 0.18992 | val_0_mae: 0.13392 | val_0_mse: 0.03607 |  0:00:50s
    epoch 406| loss: 0.02052 | val_0_rmse: 0.18981 | val_0_mae: 0.13301 | val_0_mse: 0.03603 |  0:00:50s
    epoch 407| loss: 0.02057 | val_0_rmse: 0.19217 | val_0_mae: 0.13388 | val_0_mse: 0.03693 |  0:00:51s
    epoch 408| loss: 0.02096 | val_0_rmse: 0.1905  | val_0_mae: 0.13209 | val_0_mse: 0.03629 |  0:00:51s
    epoch 409| loss: 0.02035 | val_0_rmse: 0.19194 | val_0_mae: 0.13351 | val_0_mse: 0.03684 |  0:00:51s
    epoch 410| loss: 0.02061 | val_0_rmse: 0.19189 | val_0_mae: 0.13315 | val_0_mse: 0.03682 |  0:00:51s
    epoch 411| loss: 0.02112 | val_0_rmse: 0.19051 | val_0_mae: 0.13329 | val_0_mse: 0.0363  |  0:00:51s
    epoch 412| loss: 0.02136 | val_0_rmse: 0.19128 | val_0_mae: 0.1345  | val_0_mse: 0.03659 |  0:00:51s
    epoch 413| loss: 0.02089 | val_0_rmse: 0.19159 | val_0_mae: 0.13381 | val_0_mse: 0.03671 |  0:00:51s
    epoch 414| loss: 0.02034 | val_0_rmse: 0.18934 | val_0_mae: 0.13145 | val_0_mse: 0.03585 |  0:00:51s
    epoch 415| loss: 0.0203  | val_0_rmse: 0.18957 | val_0_mae: 0.13302 | val_0_mse: 0.03594 |  0:00:51s
    epoch 416| loss: 0.02123 | val_0_rmse: 0.19013 | val_0_mae: 0.13165 | val_0_mse: 0.03615 |  0:00:52s
    epoch 417| loss: 0.02115 | val_0_rmse: 0.18747 | val_0_mae: 0.13033 | val_0_mse: 0.03515 |  0:00:52s
    epoch 418| loss: 0.01987 | val_0_rmse: 0.18637 | val_0_mae: 0.13028 | val_0_mse: 0.03474 |  0:00:52s
    epoch 419| loss: 0.01993 | val_0_rmse: 0.18652 | val_0_mae: 0.13027 | val_0_mse: 0.03479 |  0:00:52s
    epoch 420| loss: 0.02045 | val_0_rmse: 0.18605 | val_0_mae: 0.13017 | val_0_mse: 0.03461 |  0:00:52s
    epoch 421| loss: 0.0205  | val_0_rmse: 0.18785 | val_0_mae: 0.13086 | val_0_mse: 0.03529 |  0:00:52s
    epoch 422| loss: 0.02083 | val_0_rmse: 0.18837 | val_0_mae: 0.13135 | val_0_mse: 0.03548 |  0:00:52s
    epoch 423| loss: 0.01954 | val_0_rmse: 0.19058 | val_0_mae: 0.13277 | val_0_mse: 0.03632 |  0:00:52s
    epoch 424| loss: 0.02138 | val_0_rmse: 0.19112 | val_0_mae: 0.13193 | val_0_mse: 0.03653 |  0:00:53s
    epoch 425| loss: 0.02054 | val_0_rmse: 0.19138 | val_0_mae: 0.13163 | val_0_mse: 0.03663 |  0:00:53s
    epoch 426| loss: 0.02012 | val_0_rmse: 0.19268 | val_0_mae: 0.13387 | val_0_mse: 0.03712 |  0:00:53s
    epoch 427| loss: 0.02035 | val_0_rmse: 0.19134 | val_0_mae: 0.13211 | val_0_mse: 0.03661 |  0:00:53s
    epoch 428| loss: 0.01905 | val_0_rmse: 0.19118 | val_0_mae: 0.13222 | val_0_mse: 0.03655 |  0:00:53s
    epoch 429| loss: 0.01916 | val_0_rmse: 0.19061 | val_0_mae: 0.13197 | val_0_mse: 0.03633 |  0:00:53s
    epoch 430| loss: 0.01922 | val_0_rmse: 0.19064 | val_0_mae: 0.13214 | val_0_mse: 0.03634 |  0:00:53s
    epoch 431| loss: 0.01952 | val_0_rmse: 0.1908  | val_0_mae: 0.13216 | val_0_mse: 0.0364  |  0:00:53s
    epoch 432| loss: 0.01993 | val_0_rmse: 0.19012 | val_0_mae: 0.13205 | val_0_mse: 0.03615 |  0:00:54s
    epoch 433| loss: 0.02006 | val_0_rmse: 0.19028 | val_0_mae: 0.13237 | val_0_mse: 0.03621 |  0:00:54s
    epoch 434| loss: 0.01941 | val_0_rmse: 0.19132 | val_0_mae: 0.13337 | val_0_mse: 0.0366  |  0:00:54s
    epoch 435| loss: 0.02013 | val_0_rmse: 0.19091 | val_0_mae: 0.13311 | val_0_mse: 0.03645 |  0:00:54s
    epoch 436| loss: 0.01949 | val_0_rmse: 0.19018 | val_0_mae: 0.13123 | val_0_mse: 0.03617 |  0:00:54s
    epoch 437| loss: 0.01956 | val_0_rmse: 0.1892  | val_0_mae: 0.13075 | val_0_mse: 0.0358  |  0:00:54s
    epoch 438| loss: 0.01945 | val_0_rmse: 0.19158 | val_0_mae: 0.13348 | val_0_mse: 0.0367  |  0:00:54s
    epoch 439| loss: 0.01905 | val_0_rmse: 0.19272 | val_0_mae: 0.13211 | val_0_mse: 0.03714 |  0:00:54s
    epoch 440| loss: 0.01979 | val_0_rmse: 0.19262 | val_0_mae: 0.13197 | val_0_mse: 0.0371  |  0:00:54s
    epoch 441| loss: 0.02037 | val_0_rmse: 0.19333 | val_0_mae: 0.13327 | val_0_mse: 0.03738 |  0:00:55s
    epoch 442| loss: 0.01964 | val_0_rmse: 0.19407 | val_0_mae: 0.13319 | val_0_mse: 0.03766 |  0:00:55s
    epoch 443| loss: 0.01942 | val_0_rmse: 0.19329 | val_0_mae: 0.13413 | val_0_mse: 0.03736 |  0:00:55s
    epoch 444| loss: 0.01989 | val_0_rmse: 0.19276 | val_0_mae: 0.13311 | val_0_mse: 0.03716 |  0:00:55s
    epoch 445| loss: 0.01958 | val_0_rmse: 0.19386 | val_0_mae: 0.13378 | val_0_mse: 0.03758 |  0:00:55s
    epoch 446| loss: 0.02031 | val_0_rmse: 0.19187 | val_0_mae: 0.13456 | val_0_mse: 0.03681 |  0:00:55s
    epoch 447| loss: 0.01977 | val_0_rmse: 0.19086 | val_0_mae: 0.13193 | val_0_mse: 0.03643 |  0:00:55s
    epoch 448| loss: 0.01977 | val_0_rmse: 0.19178 | val_0_mae: 0.13194 | val_0_mse: 0.03678 |  0:00:55s
    epoch 449| loss: 0.0192  | val_0_rmse: 0.19016 | val_0_mae: 0.13136 | val_0_mse: 0.03616 |  0:00:55s
    epoch 450| loss: 0.01886 | val_0_rmse: 0.18808 | val_0_mae: 0.12848 | val_0_mse: 0.03538 |  0:00:56s
    epoch 451| loss: 0.01836 | val_0_rmse: 0.18859 | val_0_mae: 0.1294  | val_0_mse: 0.03557 |  0:00:56s
    epoch 452| loss: 0.01856 | val_0_rmse: 0.18806 | val_0_mae: 0.13124 | val_0_mse: 0.03537 |  0:00:56s
    epoch 453| loss: 0.01937 | val_0_rmse: 0.18736 | val_0_mae: 0.12965 | val_0_mse: 0.0351  |  0:00:56s
    epoch 454| loss: 0.01784 | val_0_rmse: 0.18706 | val_0_mae: 0.12904 | val_0_mse: 0.03499 |  0:00:56s
    epoch 455| loss: 0.01843 | val_0_rmse: 0.18686 | val_0_mae: 0.12917 | val_0_mse: 0.03492 |  0:00:56s
    epoch 456| loss: 0.0203  | val_0_rmse: 0.18762 | val_0_mae: 0.12967 | val_0_mse: 0.0352  |  0:00:56s
    epoch 457| loss: 0.01919 | val_0_rmse: 0.18881 | val_0_mae: 0.1297  | val_0_mse: 0.03565 |  0:00:56s
    epoch 458| loss: 0.01927 | val_0_rmse: 0.18883 | val_0_mae: 0.13048 | val_0_mse: 0.03566 |  0:00:57s
    epoch 459| loss: 0.01833 | val_0_rmse: 0.18923 | val_0_mae: 0.13125 | val_0_mse: 0.03581 |  0:00:57s
    epoch 460| loss: 0.01869 | val_0_rmse: 0.1903  | val_0_mae: 0.13251 | val_0_mse: 0.03621 |  0:00:57s
    epoch 461| loss: 0.01891 | val_0_rmse: 0.18905 | val_0_mae: 0.13196 | val_0_mse: 0.03574 |  0:00:57s
    epoch 462| loss: 0.01878 | val_0_rmse: 0.18893 | val_0_mae: 0.13153 | val_0_mse: 0.03569 |  0:00:57s
    epoch 463| loss: 0.01878 | val_0_rmse: 0.18879 | val_0_mae: 0.131   | val_0_mse: 0.03564 |  0:00:57s
    epoch 464| loss: 0.01804 | val_0_rmse: 0.18839 | val_0_mae: 0.13033 | val_0_mse: 0.03549 |  0:00:57s
    epoch 465| loss: 0.01927 | val_0_rmse: 0.18827 | val_0_mae: 0.1305  | val_0_mse: 0.03545 |  0:00:57s
    epoch 466| loss: 0.01994 | val_0_rmse: 0.18708 | val_0_mae: 0.1301  | val_0_mse: 0.035   |  0:00:57s
    epoch 467| loss: 0.01927 | val_0_rmse: 0.18689 | val_0_mae: 0.12912 | val_0_mse: 0.03493 |  0:00:58s
    epoch 468| loss: 0.01907 | val_0_rmse: 0.18687 | val_0_mae: 0.12938 | val_0_mse: 0.03492 |  0:00:58s
    epoch 469| loss: 0.0183  | val_0_rmse: 0.18788 | val_0_mae: 0.13129 | val_0_mse: 0.0353  |  0:00:58s
    epoch 470| loss: 0.01865 | val_0_rmse: 0.18686 | val_0_mae: 0.13    | val_0_mse: 0.03492 |  0:00:58s
    epoch 471| loss: 0.01869 | val_0_rmse: 0.1865  | val_0_mae: 0.12955 | val_0_mse: 0.03478 |  0:00:58s
    epoch 472| loss: 0.01842 | val_0_rmse: 0.18595 | val_0_mae: 0.13006 | val_0_mse: 0.03458 |  0:00:58s
    epoch 473| loss: 0.01742 | val_0_rmse: 0.18561 | val_0_mae: 0.12941 | val_0_mse: 0.03445 |  0:00:58s
    epoch 474| loss: 0.01813 | val_0_rmse: 0.18588 | val_0_mae: 0.12953 | val_0_mse: 0.03455 |  0:00:58s
    epoch 475| loss: 0.01879 | val_0_rmse: 0.18506 | val_0_mae: 0.12881 | val_0_mse: 0.03425 |  0:00:58s
    epoch 476| loss: 0.01827 | val_0_rmse: 0.18463 | val_0_mae: 0.12804 | val_0_mse: 0.03409 |  0:00:59s
    epoch 477| loss: 0.0179  | val_0_rmse: 0.18611 | val_0_mae: 0.12808 | val_0_mse: 0.03464 |  0:00:59s
    epoch 478| loss: 0.01976 | val_0_rmse: 0.18561 | val_0_mae: 0.12885 | val_0_mse: 0.03445 |  0:00:59s
    epoch 479| loss: 0.01727 | val_0_rmse: 0.18744 | val_0_mae: 0.13089 | val_0_mse: 0.03513 |  0:00:59s
    epoch 480| loss: 0.01874 | val_0_rmse: 0.18761 | val_0_mae: 0.13079 | val_0_mse: 0.0352  |  0:00:59s
    epoch 481| loss: 0.01812 | val_0_rmse: 0.18623 | val_0_mae: 0.12975 | val_0_mse: 0.03468 |  0:00:59s
    epoch 482| loss: 0.01721 | val_0_rmse: 0.18529 | val_0_mae: 0.12917 | val_0_mse: 0.03433 |  0:00:59s
    epoch 483| loss: 0.01845 | val_0_rmse: 0.18522 | val_0_mae: 0.12834 | val_0_mse: 0.03431 |  0:00:59s
    epoch 484| loss: 0.01887 | val_0_rmse: 0.18457 | val_0_mae: 0.12861 | val_0_mse: 0.03407 |  0:01:00s
    epoch 485| loss: 0.01711 | val_0_rmse: 0.18505 | val_0_mae: 0.12964 | val_0_mse: 0.03424 |  0:01:00s
    epoch 486| loss: 0.01793 | val_0_rmse: 0.18451 | val_0_mae: 0.12844 | val_0_mse: 0.03404 |  0:01:00s
    epoch 487| loss: 0.01774 | val_0_rmse: 0.18404 | val_0_mae: 0.12838 | val_0_mse: 0.03387 |  0:01:00s
    epoch 488| loss: 0.01749 | val_0_rmse: 0.1835  | val_0_mae: 0.12874 | val_0_mse: 0.03367 |  0:01:00s
    epoch 489| loss: 0.01811 | val_0_rmse: 0.18352 | val_0_mae: 0.128   | val_0_mse: 0.03368 |  0:01:00s
    epoch 490| loss: 0.01715 | val_0_rmse: 0.18478 | val_0_mae: 0.12887 | val_0_mse: 0.03414 |  0:01:00s
    epoch 491| loss: 0.01736 | val_0_rmse: 0.18659 | val_0_mae: 0.131   | val_0_mse: 0.03481 |  0:01:00s
    epoch 492| loss: 0.01748 | val_0_rmse: 0.18709 | val_0_mae: 0.12989 | val_0_mse: 0.035   |  0:01:01s
    epoch 493| loss: 0.01813 | val_0_rmse: 0.18608 | val_0_mae: 0.12858 | val_0_mse: 0.03463 |  0:01:01s
    epoch 494| loss: 0.0172  | val_0_rmse: 0.18532 | val_0_mae: 0.12832 | val_0_mse: 0.03434 |  0:01:01s
    epoch 495| loss: 0.01885 | val_0_rmse: 0.18407 | val_0_mae: 0.128   | val_0_mse: 0.03388 |  0:01:01s
    epoch 496| loss: 0.01826 | val_0_rmse: 0.18445 | val_0_mae: 0.1285  | val_0_mse: 0.03402 |  0:01:01s
    epoch 497| loss: 0.01861 | val_0_rmse: 0.18454 | val_0_mae: 0.12858 | val_0_mse: 0.03405 |  0:01:01s
    epoch 498| loss: 0.01793 | val_0_rmse: 0.18382 | val_0_mae: 0.12711 | val_0_mse: 0.03379 |  0:01:02s
    epoch 499| loss: 0.01784 | val_0_rmse: 0.18417 | val_0_mae: 0.12671 | val_0_mse: 0.03392 |  0:01:02s
    epoch 500| loss: 0.01798 | val_0_rmse: 0.18478 | val_0_mae: 0.12802 | val_0_mse: 0.03414 |  0:01:02s
    epoch 501| loss: 0.01727 | val_0_rmse: 0.18559 | val_0_mae: 0.12795 | val_0_mse: 0.03444 |  0:01:02s
    epoch 502| loss: 0.01828 | val_0_rmse: 0.18569 | val_0_mae: 0.12852 | val_0_mse: 0.03448 |  0:01:02s
    epoch 503| loss: 0.01805 | val_0_rmse: 0.18597 | val_0_mae: 0.12961 | val_0_mse: 0.03459 |  0:01:02s
    epoch 504| loss: 0.0174  | val_0_rmse: 0.18531 | val_0_mae: 0.12852 | val_0_mse: 0.03434 |  0:01:02s
    epoch 505| loss: 0.01684 | val_0_rmse: 0.18577 | val_0_mae: 0.12869 | val_0_mse: 0.03451 |  0:01:02s
    epoch 506| loss: 0.01835 | val_0_rmse: 0.18581 | val_0_mae: 0.13065 | val_0_mse: 0.03452 |  0:01:03s
    epoch 507| loss: 0.01713 | val_0_rmse: 0.18538 | val_0_mae: 0.12863 | val_0_mse: 0.03436 |  0:01:03s
    epoch 508| loss: 0.01735 | val_0_rmse: 0.18707 | val_0_mae: 0.13055 | val_0_mse: 0.03499 |  0:01:03s
    epoch 509| loss: 0.01707 | val_0_rmse: 0.1859  | val_0_mae: 0.12952 | val_0_mse: 0.03456 |  0:01:03s
    epoch 510| loss: 0.01791 | val_0_rmse: 0.18517 | val_0_mae: 0.12948 | val_0_mse: 0.03429 |  0:01:03s
    epoch 511| loss: 0.01751 | val_0_rmse: 0.18466 | val_0_mae: 0.12865 | val_0_mse: 0.0341  |  0:01:03s
    epoch 512| loss: 0.01802 | val_0_rmse: 0.18418 | val_0_mae: 0.12791 | val_0_mse: 0.03392 |  0:01:03s
    epoch 513| loss: 0.01765 | val_0_rmse: 0.18398 | val_0_mae: 0.12843 | val_0_mse: 0.03385 |  0:01:03s
    epoch 514| loss: 0.01705 | val_0_rmse: 0.18325 | val_0_mae: 0.12745 | val_0_mse: 0.03358 |  0:01:03s
    epoch 515| loss: 0.01727 | val_0_rmse: 0.1838  | val_0_mae: 0.12698 | val_0_mse: 0.03378 |  0:01:04s
    epoch 516| loss: 0.01629 | val_0_rmse: 0.1843  | val_0_mae: 0.12689 | val_0_mse: 0.03397 |  0:01:04s
    epoch 517| loss: 0.0171  | val_0_rmse: 0.18448 | val_0_mae: 0.12794 | val_0_mse: 0.03403 |  0:01:04s
    epoch 518| loss: 0.0169  | val_0_rmse: 0.18336 | val_0_mae: 0.12634 | val_0_mse: 0.03362 |  0:01:04s
    epoch 519| loss: 0.01667 | val_0_rmse: 0.18225 | val_0_mae: 0.12583 | val_0_mse: 0.03321 |  0:01:04s
    epoch 520| loss: 0.0174  | val_0_rmse: 0.18168 | val_0_mae: 0.12571 | val_0_mse: 0.03301 |  0:01:04s
    epoch 521| loss: 0.01638 | val_0_rmse: 0.18158 | val_0_mae: 0.12518 | val_0_mse: 0.03297 |  0:01:04s
    epoch 522| loss: 0.01688 | val_0_rmse: 0.18214 | val_0_mae: 0.12658 | val_0_mse: 0.03318 |  0:01:05s
    epoch 523| loss: 0.01713 | val_0_rmse: 0.18207 | val_0_mae: 0.12707 | val_0_mse: 0.03315 |  0:01:05s
    epoch 524| loss: 0.01606 | val_0_rmse: 0.1827  | val_0_mae: 0.12662 | val_0_mse: 0.03338 |  0:01:05s
    epoch 525| loss: 0.01592 | val_0_rmse: 0.18331 | val_0_mae: 0.12772 | val_0_mse: 0.0336  |  0:01:05s
    epoch 526| loss: 0.017   | val_0_rmse: 0.18378 | val_0_mae: 0.12721 | val_0_mse: 0.03377 |  0:01:05s
    epoch 527| loss: 0.01647 | val_0_rmse: 0.18618 | val_0_mae: 0.12837 | val_0_mse: 0.03466 |  0:01:05s
    epoch 528| loss: 0.01664 | val_0_rmse: 0.1865  | val_0_mae: 0.12898 | val_0_mse: 0.03478 |  0:01:05s
    epoch 529| loss: 0.0165  | val_0_rmse: 0.18699 | val_0_mae: 0.12857 | val_0_mse: 0.03496 |  0:01:05s
    epoch 530| loss: 0.01632 | val_0_rmse: 0.18675 | val_0_mae: 0.12739 | val_0_mse: 0.03488 |  0:01:05s
    epoch 531| loss: 0.01748 | val_0_rmse: 0.1857  | val_0_mae: 0.1282  | val_0_mse: 0.03448 |  0:01:06s
    epoch 532| loss: 0.01596 | val_0_rmse: 0.18469 | val_0_mae: 0.12708 | val_0_mse: 0.03411 |  0:01:06s
    epoch 533| loss: 0.01741 | val_0_rmse: 0.18437 | val_0_mae: 0.12635 | val_0_mse: 0.03399 |  0:01:06s
    epoch 534| loss: 0.01572 | val_0_rmse: 0.18368 | val_0_mae: 0.12812 | val_0_mse: 0.03374 |  0:01:06s
    epoch 535| loss: 0.01675 | val_0_rmse: 0.18422 | val_0_mae: 0.12814 | val_0_mse: 0.03394 |  0:01:06s
    epoch 536| loss: 0.01603 | val_0_rmse: 0.18532 | val_0_mae: 0.12737 | val_0_mse: 0.03434 |  0:01:06s
    epoch 537| loss: 0.01692 | val_0_rmse: 0.18494 | val_0_mae: 0.12821 | val_0_mse: 0.0342  |  0:01:06s
    epoch 538| loss: 0.01597 | val_0_rmse: 0.1846  | val_0_mae: 0.12773 | val_0_mse: 0.03408 |  0:01:06s
    epoch 539| loss: 0.01685 | val_0_rmse: 0.184   | val_0_mae: 0.1271  | val_0_mse: 0.03386 |  0:01:07s
    epoch 540| loss: 0.01725 | val_0_rmse: 0.1831  | val_0_mae: 0.12678 | val_0_mse: 0.03352 |  0:01:07s
    epoch 541| loss: 0.0165  | val_0_rmse: 0.18194 | val_0_mae: 0.12639 | val_0_mse: 0.0331  |  0:01:07s
    epoch 542| loss: 0.01703 | val_0_rmse: 0.18172 | val_0_mae: 0.12508 | val_0_mse: 0.03302 |  0:01:07s
    epoch 543| loss: 0.01652 | val_0_rmse: 0.1818  | val_0_mae: 0.1257  | val_0_mse: 0.03305 |  0:01:07s
    epoch 544| loss: 0.01656 | val_0_rmse: 0.18276 | val_0_mae: 0.12644 | val_0_mse: 0.0334  |  0:01:07s
    epoch 545| loss: 0.01672 | val_0_rmse: 0.1824  | val_0_mae: 0.12579 | val_0_mse: 0.03327 |  0:01:07s
    epoch 546| loss: 0.01579 | val_0_rmse: 0.18275 | val_0_mae: 0.12748 | val_0_mse: 0.0334  |  0:01:07s
    epoch 547| loss: 0.01639 | val_0_rmse: 0.18287 | val_0_mae: 0.12701 | val_0_mse: 0.03344 |  0:01:07s
    epoch 548| loss: 0.01639 | val_0_rmse: 0.18235 | val_0_mae: 0.12617 | val_0_mse: 0.03325 |  0:01:08s
    epoch 549| loss: 0.01629 | val_0_rmse: 0.18136 | val_0_mae: 0.12665 | val_0_mse: 0.03289 |  0:01:08s
    epoch 550| loss: 0.01641 | val_0_rmse: 0.18081 | val_0_mae: 0.12585 | val_0_mse: 0.03269 |  0:01:08s
    epoch 551| loss: 0.01582 | val_0_rmse: 0.18092 | val_0_mae: 0.12591 | val_0_mse: 0.03273 |  0:01:08s
    epoch 552| loss: 0.01575 | val_0_rmse: 0.18159 | val_0_mae: 0.12619 | val_0_mse: 0.03297 |  0:01:08s
    epoch 553| loss: 0.01642 | val_0_rmse: 0.18185 | val_0_mae: 0.12628 | val_0_mse: 0.03307 |  0:01:08s
    epoch 554| loss: 0.01638 | val_0_rmse: 0.18248 | val_0_mae: 0.12687 | val_0_mse: 0.0333  |  0:01:08s
    epoch 555| loss: 0.01655 | val_0_rmse: 0.18343 | val_0_mae: 0.1271  | val_0_mse: 0.03365 |  0:01:08s
    epoch 556| loss: 0.01624 | val_0_rmse: 0.18272 | val_0_mae: 0.12641 | val_0_mse: 0.03339 |  0:01:08s
    epoch 557| loss: 0.01646 | val_0_rmse: 0.18342 | val_0_mae: 0.129   | val_0_mse: 0.03364 |  0:01:09s
    epoch 558| loss: 0.01683 | val_0_rmse: 0.18089 | val_0_mae: 0.12553 | val_0_mse: 0.03272 |  0:01:09s
    epoch 559| loss: 0.01583 | val_0_rmse: 0.17994 | val_0_mae: 0.12491 | val_0_mse: 0.03238 |  0:01:09s
    epoch 560| loss: 0.016   | val_0_rmse: 0.17927 | val_0_mae: 0.12398 | val_0_mse: 0.03214 |  0:01:09s
    epoch 561| loss: 0.01687 | val_0_rmse: 0.18045 | val_0_mae: 0.12483 | val_0_mse: 0.03256 |  0:01:09s
    epoch 562| loss: 0.01612 | val_0_rmse: 0.18    | val_0_mae: 0.12463 | val_0_mse: 0.0324  |  0:01:09s
    epoch 563| loss: 0.01558 | val_0_rmse: 0.18015 | val_0_mae: 0.12422 | val_0_mse: 0.03245 |  0:01:09s
    epoch 564| loss: 0.01633 | val_0_rmse: 0.18101 | val_0_mae: 0.12599 | val_0_mse: 0.03276 |  0:01:10s
    epoch 565| loss: 0.01517 | val_0_rmse: 0.18021 | val_0_mae: 0.12449 | val_0_mse: 0.03248 |  0:01:10s
    epoch 566| loss: 0.01587 | val_0_rmse: 0.18115 | val_0_mae: 0.1253  | val_0_mse: 0.03281 |  0:01:10s
    epoch 567| loss: 0.01579 | val_0_rmse: 0.18048 | val_0_mae: 0.12445 | val_0_mse: 0.03257 |  0:01:10s
    epoch 568| loss: 0.01607 | val_0_rmse: 0.18062 | val_0_mae: 0.12547 | val_0_mse: 0.03262 |  0:01:10s
    epoch 569| loss: 0.01612 | val_0_rmse: 0.18168 | val_0_mae: 0.12554 | val_0_mse: 0.03301 |  0:01:10s
    epoch 570| loss: 0.01607 | val_0_rmse: 0.18134 | val_0_mae: 0.12575 | val_0_mse: 0.03288 |  0:01:10s
    epoch 571| loss: 0.01579 | val_0_rmse: 0.18082 | val_0_mae: 0.12608 | val_0_mse: 0.0327  |  0:01:10s
    epoch 572| loss: 0.01644 | val_0_rmse: 0.1809  | val_0_mae: 0.12633 | val_0_mse: 0.03273 |  0:01:11s
    epoch 573| loss: 0.01515 | val_0_rmse: 0.18184 | val_0_mae: 0.12553 | val_0_mse: 0.03307 |  0:01:11s
    epoch 574| loss: 0.01658 | val_0_rmse: 0.18254 | val_0_mae: 0.12668 | val_0_mse: 0.03332 |  0:01:11s
    epoch 575| loss: 0.01651 | val_0_rmse: 0.18275 | val_0_mae: 0.12767 | val_0_mse: 0.0334  |  0:01:11s
    epoch 576| loss: 0.01628 | val_0_rmse: 0.18275 | val_0_mae: 0.12601 | val_0_mse: 0.0334  |  0:01:11s
    epoch 577| loss: 0.0152  | val_0_rmse: 0.18249 | val_0_mae: 0.12677 | val_0_mse: 0.0333  |  0:01:11s
    epoch 578| loss: 0.01608 | val_0_rmse: 0.18183 | val_0_mae: 0.12707 | val_0_mse: 0.03306 |  0:01:11s
    epoch 579| loss: 0.01584 | val_0_rmse: 0.1811  | val_0_mae: 0.12352 | val_0_mse: 0.0328  |  0:01:11s
    epoch 580| loss: 0.01548 | val_0_rmse: 0.18095 | val_0_mae: 0.12535 | val_0_mse: 0.03274 |  0:01:12s
    epoch 581| loss: 0.01595 | val_0_rmse: 0.18029 | val_0_mae: 0.12441 | val_0_mse: 0.03251 |  0:01:12s
    epoch 582| loss: 0.01541 | val_0_rmse: 0.18058 | val_0_mae: 0.12505 | val_0_mse: 0.03261 |  0:01:12s
    epoch 583| loss: 0.01504 | val_0_rmse: 0.18019 | val_0_mae: 0.12496 | val_0_mse: 0.03247 |  0:01:12s
    epoch 584| loss: 0.01535 | val_0_rmse: 0.18096 | val_0_mae: 0.12455 | val_0_mse: 0.03275 |  0:01:12s
    epoch 585| loss: 0.01578 | val_0_rmse: 0.18016 | val_0_mae: 0.12502 | val_0_mse: 0.03246 |  0:01:12s
    epoch 586| loss: 0.01641 | val_0_rmse: 0.1796  | val_0_mae: 0.12385 | val_0_mse: 0.03226 |  0:01:12s
    epoch 587| loss: 0.01563 | val_0_rmse: 0.18014 | val_0_mae: 0.12357 | val_0_mse: 0.03245 |  0:01:12s
    epoch 588| loss: 0.0158  | val_0_rmse: 0.18054 | val_0_mae: 0.12446 | val_0_mse: 0.0326  |  0:01:12s
    epoch 589| loss: 0.01509 | val_0_rmse: 0.18234 | val_0_mae: 0.12557 | val_0_mse: 0.03325 |  0:01:12s
    epoch 590| loss: 0.01587 | val_0_rmse: 0.18364 | val_0_mae: 0.12635 | val_0_mse: 0.03372 |  0:01:13s
    epoch 591| loss: 0.01597 | val_0_rmse: 0.18468 | val_0_mae: 0.12911 | val_0_mse: 0.03411 |  0:01:13s
    epoch 592| loss: 0.01521 | val_0_rmse: 0.18455 | val_0_mae: 0.12832 | val_0_mse: 0.03406 |  0:01:13s
    epoch 593| loss: 0.01468 | val_0_rmse: 0.18299 | val_0_mae: 0.1267  | val_0_mse: 0.03348 |  0:01:13s
    epoch 594| loss: 0.01602 | val_0_rmse: 0.18232 | val_0_mae: 0.12555 | val_0_mse: 0.03324 |  0:01:13s
    epoch 595| loss: 0.01601 | val_0_rmse: 0.18153 | val_0_mae: 0.12385 | val_0_mse: 0.03295 |  0:01:13s
    epoch 596| loss: 0.01536 | val_0_rmse: 0.18119 | val_0_mae: 0.12587 | val_0_mse: 0.03283 |  0:01:13s
    epoch 597| loss: 0.01578 | val_0_rmse: 0.1813  | val_0_mae: 0.12538 | val_0_mse: 0.03287 |  0:01:13s
    epoch 598| loss: 0.01613 | val_0_rmse: 0.18077 | val_0_mae: 0.12557 | val_0_mse: 0.03268 |  0:01:13s
    epoch 599| loss: 0.01544 | val_0_rmse: 0.18094 | val_0_mae: 0.12741 | val_0_mse: 0.03274 |  0:01:14s
    epoch 600| loss: 0.01599 | val_0_rmse: 0.18062 | val_0_mae: 0.12485 | val_0_mse: 0.03262 |  0:01:14s
    epoch 601| loss: 0.01526 | val_0_rmse: 0.18116 | val_0_mae: 0.12527 | val_0_mse: 0.03282 |  0:01:14s
    epoch 602| loss: 0.01448 | val_0_rmse: 0.18226 | val_0_mae: 0.12637 | val_0_mse: 0.03322 |  0:01:14s
    epoch 603| loss: 0.01467 | val_0_rmse: 0.18195 | val_0_mae: 0.12491 | val_0_mse: 0.0331  |  0:01:14s
    epoch 604| loss: 0.01506 | val_0_rmse: 0.18184 | val_0_mae: 0.12578 | val_0_mse: 0.03307 |  0:01:14s
    epoch 605| loss: 0.01463 | val_0_rmse: 0.18135 | val_0_mae: 0.12514 | val_0_mse: 0.03289 |  0:01:14s
    epoch 606| loss: 0.01495 | val_0_rmse: 0.18003 | val_0_mae: 0.12392 | val_0_mse: 0.03241 |  0:01:14s
    epoch 607| loss: 0.01551 | val_0_rmse: 0.18003 | val_0_mae: 0.12488 | val_0_mse: 0.03241 |  0:01:15s
    epoch 608| loss: 0.01522 | val_0_rmse: 0.17941 | val_0_mae: 0.12349 | val_0_mse: 0.03219 |  0:01:15s
    epoch 609| loss: 0.01519 | val_0_rmse: 0.18004 | val_0_mae: 0.12402 | val_0_mse: 0.03242 |  0:01:15s
    epoch 610| loss: 0.01494 | val_0_rmse: 0.1807  | val_0_mae: 0.12552 | val_0_mse: 0.03265 |  0:01:15s
    epoch 611| loss: 0.01514 | val_0_rmse: 0.18032 | val_0_mae: 0.12471 | val_0_mse: 0.03252 |  0:01:15s
    epoch 612| loss: 0.01451 | val_0_rmse: 0.18078 | val_0_mae: 0.12412 | val_0_mse: 0.03268 |  0:01:15s
    epoch 613| loss: 0.01547 | val_0_rmse: 0.1809  | val_0_mae: 0.12515 | val_0_mse: 0.03273 |  0:01:15s
    epoch 614| loss: 0.015   | val_0_rmse: 0.18092 | val_0_mae: 0.12554 | val_0_mse: 0.03273 |  0:01:15s
    epoch 615| loss: 0.01539 | val_0_rmse: 0.1801  | val_0_mae: 0.12472 | val_0_mse: 0.03244 |  0:01:15s
    epoch 616| loss: 0.01562 | val_0_rmse: 0.17942 | val_0_mae: 0.1242  | val_0_mse: 0.03219 |  0:01:16s
    epoch 617| loss: 0.01451 | val_0_rmse: 0.17883 | val_0_mae: 0.12357 | val_0_mse: 0.03198 |  0:01:16s
    epoch 618| loss: 0.01445 | val_0_rmse: 0.17925 | val_0_mae: 0.1235  | val_0_mse: 0.03213 |  0:01:16s
    epoch 619| loss: 0.01537 | val_0_rmse: 0.17907 | val_0_mae: 0.12359 | val_0_mse: 0.03206 |  0:01:16s
    epoch 620| loss: 0.01489 | val_0_rmse: 0.18013 | val_0_mae: 0.12496 | val_0_mse: 0.03245 |  0:01:16s
    epoch 621| loss: 0.01546 | val_0_rmse: 0.18054 | val_0_mae: 0.12518 | val_0_mse: 0.0326  |  0:01:16s
    epoch 622| loss: 0.01527 | val_0_rmse: 0.18046 | val_0_mae: 0.12506 | val_0_mse: 0.03257 |  0:01:16s
    epoch 623| loss: 0.01495 | val_0_rmse: 0.1795  | val_0_mae: 0.12292 | val_0_mse: 0.03222 |  0:01:16s
    epoch 624| loss: 0.01415 | val_0_rmse: 0.18008 | val_0_mae: 0.12382 | val_0_mse: 0.03243 |  0:01:16s
    epoch 625| loss: 0.01508 | val_0_rmse: 0.18033 | val_0_mae: 0.12423 | val_0_mse: 0.03252 |  0:01:17s
    epoch 626| loss: 0.01473 | val_0_rmse: 0.18021 | val_0_mae: 0.12346 | val_0_mse: 0.03248 |  0:01:17s
    epoch 627| loss: 0.01482 | val_0_rmse: 0.17959 | val_0_mae: 0.12279 | val_0_mse: 0.03225 |  0:01:17s
    epoch 628| loss: 0.01526 | val_0_rmse: 0.17951 | val_0_mae: 0.12396 | val_0_mse: 0.03222 |  0:01:17s
    epoch 629| loss: 0.01451 | val_0_rmse: 0.17931 | val_0_mae: 0.12287 | val_0_mse: 0.03215 |  0:01:17s
    epoch 630| loss: 0.01432 | val_0_rmse: 0.17962 | val_0_mae: 0.12309 | val_0_mse: 0.03226 |  0:01:17s
    epoch 631| loss: 0.01564 | val_0_rmse: 0.17971 | val_0_mae: 0.1252  | val_0_mse: 0.03229 |  0:01:17s
    epoch 632| loss: 0.01601 | val_0_rmse: 0.17927 | val_0_mae: 0.12335 | val_0_mse: 0.03214 |  0:01:17s
    epoch 633| loss: 0.01435 | val_0_rmse: 0.18004 | val_0_mae: 0.12398 | val_0_mse: 0.03242 |  0:01:17s
    epoch 634| loss: 0.01403 | val_0_rmse: 0.18039 | val_0_mae: 0.12496 | val_0_mse: 0.03254 |  0:01:18s
    epoch 635| loss: 0.01483 | val_0_rmse: 0.1794  | val_0_mae: 0.12392 | val_0_mse: 0.03218 |  0:01:18s
    epoch 636| loss: 0.0146  | val_0_rmse: 0.17982 | val_0_mae: 0.12576 | val_0_mse: 0.03234 |  0:01:18s
    epoch 637| loss: 0.01431 | val_0_rmse: 0.17928 | val_0_mae: 0.12413 | val_0_mse: 0.03214 |  0:01:18s
    epoch 638| loss: 0.01509 | val_0_rmse: 0.17826 | val_0_mae: 0.1242  | val_0_mse: 0.03178 |  0:01:18s
    epoch 639| loss: 0.01478 | val_0_rmse: 0.17779 | val_0_mae: 0.12481 | val_0_mse: 0.03161 |  0:01:18s
    epoch 640| loss: 0.01511 | val_0_rmse: 0.17769 | val_0_mae: 0.12345 | val_0_mse: 0.03157 |  0:01:18s
    epoch 641| loss: 0.01502 | val_0_rmse: 0.17762 | val_0_mae: 0.12425 | val_0_mse: 0.03155 |  0:01:18s
    epoch 642| loss: 0.01463 | val_0_rmse: 0.17798 | val_0_mae: 0.12394 | val_0_mse: 0.03168 |  0:01:18s
    epoch 643| loss: 0.01435 | val_0_rmse: 0.1773  | val_0_mae: 0.12336 | val_0_mse: 0.03143 |  0:01:19s
    epoch 644| loss: 0.01494 | val_0_rmse: 0.17761 | val_0_mae: 0.12362 | val_0_mse: 0.03155 |  0:01:19s
    epoch 645| loss: 0.01429 | val_0_rmse: 0.17965 | val_0_mae: 0.12378 | val_0_mse: 0.03228 |  0:01:19s
    epoch 646| loss: 0.01492 | val_0_rmse: 0.17924 | val_0_mae: 0.12616 | val_0_mse: 0.03213 |  0:01:19s
    epoch 647| loss: 0.01484 | val_0_rmse: 0.17954 | val_0_mae: 0.12373 | val_0_mse: 0.03223 |  0:01:19s
    epoch 648| loss: 0.01454 | val_0_rmse: 0.1807  | val_0_mae: 0.12531 | val_0_mse: 0.03265 |  0:01:19s
    epoch 649| loss: 0.01526 | val_0_rmse: 0.18071 | val_0_mae: 0.12507 | val_0_mse: 0.03266 |  0:01:19s
    epoch 650| loss: 0.0147  | val_0_rmse: 0.1807  | val_0_mae: 0.12433 | val_0_mse: 0.03265 |  0:01:19s
    epoch 651| loss: 0.01516 | val_0_rmse: 0.18    | val_0_mae: 0.12585 | val_0_mse: 0.0324  |  0:01:19s
    epoch 652| loss: 0.01465 | val_0_rmse: 0.17901 | val_0_mae: 0.12359 | val_0_mse: 0.03204 |  0:01:20s
    epoch 653| loss: 0.01444 | val_0_rmse: 0.17927 | val_0_mae: 0.12475 | val_0_mse: 0.03214 |  0:01:20s
    epoch 654| loss: 0.01481 | val_0_rmse: 0.17885 | val_0_mae: 0.12526 | val_0_mse: 0.03199 |  0:01:20s
    epoch 655| loss: 0.01537 | val_0_rmse: 0.17686 | val_0_mae: 0.12183 | val_0_mse: 0.03128 |  0:01:20s
    epoch 656| loss: 0.01379 | val_0_rmse: 0.17833 | val_0_mae: 0.12227 | val_0_mse: 0.0318  |  0:01:20s
    epoch 657| loss: 0.01555 | val_0_rmse: 0.17849 | val_0_mae: 0.12488 | val_0_mse: 0.03186 |  0:01:20s
    epoch 658| loss: 0.01484 | val_0_rmse: 0.17798 | val_0_mae: 0.12263 | val_0_mse: 0.03168 |  0:01:20s
    epoch 659| loss: 0.01454 | val_0_rmse: 0.17781 | val_0_mae: 0.12283 | val_0_mse: 0.03162 |  0:01:20s
    epoch 660| loss: 0.0143  | val_0_rmse: 0.17746 | val_0_mae: 0.12504 | val_0_mse: 0.03149 |  0:01:20s
    epoch 661| loss: 0.01461 | val_0_rmse: 0.17698 | val_0_mae: 0.122   | val_0_mse: 0.03132 |  0:01:21s
    epoch 662| loss: 0.01441 | val_0_rmse: 0.17873 | val_0_mae: 0.12325 | val_0_mse: 0.03194 |  0:01:21s
    epoch 663| loss: 0.01413 | val_0_rmse: 0.17818 | val_0_mae: 0.12386 | val_0_mse: 0.03175 |  0:01:21s
    epoch 664| loss: 0.0144  | val_0_rmse: 0.17867 | val_0_mae: 0.12358 | val_0_mse: 0.03192 |  0:01:21s
    epoch 665| loss: 0.01572 | val_0_rmse: 0.17919 | val_0_mae: 0.12459 | val_0_mse: 0.03211 |  0:01:21s
    epoch 666| loss: 0.01477 | val_0_rmse: 0.17902 | val_0_mae: 0.12458 | val_0_mse: 0.03205 |  0:01:21s
    epoch 667| loss: 0.01407 | val_0_rmse: 0.17768 | val_0_mae: 0.12267 | val_0_mse: 0.03157 |  0:01:21s
    epoch 668| loss: 0.01542 | val_0_rmse: 0.17588 | val_0_mae: 0.12157 | val_0_mse: 0.03093 |  0:01:21s
    epoch 669| loss: 0.01464 | val_0_rmse: 0.17607 | val_0_mae: 0.12162 | val_0_mse: 0.031   |  0:01:21s
    epoch 670| loss: 0.01439 | val_0_rmse: 0.17796 | val_0_mae: 0.12232 | val_0_mse: 0.03167 |  0:01:21s
    epoch 671| loss: 0.01498 | val_0_rmse: 0.1773  | val_0_mae: 0.1233  | val_0_mse: 0.03144 |  0:01:22s
    epoch 672| loss: 0.01593 | val_0_rmse: 0.17673 | val_0_mae: 0.12187 | val_0_mse: 0.03123 |  0:01:22s
    epoch 673| loss: 0.01557 | val_0_rmse: 0.17675 | val_0_mae: 0.12118 | val_0_mse: 0.03124 |  0:01:22s
    epoch 674| loss: 0.01438 | val_0_rmse: 0.1816  | val_0_mae: 0.12938 | val_0_mse: 0.03298 |  0:01:22s
    epoch 675| loss: 0.0158  | val_0_rmse: 0.17814 | val_0_mae: 0.12433 | val_0_mse: 0.03173 |  0:01:22s
    epoch 676| loss: 0.01573 | val_0_rmse: 0.17628 | val_0_mae: 0.12237 | val_0_mse: 0.03108 |  0:01:22s
    epoch 677| loss: 0.01496 | val_0_rmse: 0.17849 | val_0_mae: 0.12581 | val_0_mse: 0.03186 |  0:01:22s
    epoch 678| loss: 0.01432 | val_0_rmse: 0.17933 | val_0_mae: 0.1227  | val_0_mse: 0.03216 |  0:01:22s
    epoch 679| loss: 0.01539 | val_0_rmse: 0.17716 | val_0_mae: 0.12321 | val_0_mse: 0.03139 |  0:01:22s
    epoch 680| loss: 0.0144  | val_0_rmse: 0.17652 | val_0_mae: 0.12298 | val_0_mse: 0.03116 |  0:01:23s
    epoch 681| loss: 0.01463 | val_0_rmse: 0.17561 | val_0_mae: 0.12059 | val_0_mse: 0.03084 |  0:01:23s
    epoch 682| loss: 0.01525 | val_0_rmse: 0.17567 | val_0_mae: 0.12198 | val_0_mse: 0.03086 |  0:01:23s
    epoch 683| loss: 0.01432 | val_0_rmse: 0.17596 | val_0_mae: 0.12201 | val_0_mse: 0.03096 |  0:01:23s
    epoch 684| loss: 0.01428 | val_0_rmse: 0.17707 | val_0_mae: 0.12222 | val_0_mse: 0.03136 |  0:01:23s
    epoch 685| loss: 0.01446 | val_0_rmse: 0.17703 | val_0_mae: 0.1231  | val_0_mse: 0.03134 |  0:01:23s
    epoch 686| loss: 0.01305 | val_0_rmse: 0.17742 | val_0_mae: 0.1228  | val_0_mse: 0.03148 |  0:01:23s
    epoch 687| loss: 0.01391 | val_0_rmse: 0.17697 | val_0_mae: 0.12242 | val_0_mse: 0.03132 |  0:01:23s
    epoch 688| loss: 0.01425 | val_0_rmse: 0.17662 | val_0_mae: 0.12243 | val_0_mse: 0.03119 |  0:01:24s
    epoch 689| loss: 0.01377 | val_0_rmse: 0.17752 | val_0_mae: 0.12343 | val_0_mse: 0.03151 |  0:01:24s
    epoch 690| loss: 0.01429 | val_0_rmse: 0.17715 | val_0_mae: 0.12234 | val_0_mse: 0.03138 |  0:01:24s
    epoch 691| loss: 0.01433 | val_0_rmse: 0.17576 | val_0_mae: 0.12201 | val_0_mse: 0.03089 |  0:01:24s
    epoch 692| loss: 0.01393 | val_0_rmse: 0.17556 | val_0_mae: 0.1221  | val_0_mse: 0.03082 |  0:01:24s
    epoch 693| loss: 0.01393 | val_0_rmse: 0.1756  | val_0_mae: 0.12098 | val_0_mse: 0.03084 |  0:01:24s
    epoch 694| loss: 0.01415 | val_0_rmse: 0.17477 | val_0_mae: 0.12162 | val_0_mse: 0.03055 |  0:01:24s
    epoch 695| loss: 0.01409 | val_0_rmse: 0.17509 | val_0_mae: 0.1218  | val_0_mse: 0.03066 |  0:01:24s
    epoch 696| loss: 0.01434 | val_0_rmse: 0.17753 | val_0_mae: 0.12239 | val_0_mse: 0.03152 |  0:01:24s
    epoch 697| loss: 0.01451 | val_0_rmse: 0.1786  | val_0_mae: 0.12576 | val_0_mse: 0.0319  |  0:01:25s
    epoch 698| loss: 0.01445 | val_0_rmse: 0.17742 | val_0_mae: 0.12252 | val_0_mse: 0.03148 |  0:01:25s
    epoch 699| loss: 0.01368 | val_0_rmse: 0.17764 | val_0_mae: 0.12255 | val_0_mse: 0.03156 |  0:01:25s
    epoch 700| loss: 0.01385 | val_0_rmse: 0.17942 | val_0_mae: 0.12576 | val_0_mse: 0.03219 |  0:01:25s
    epoch 701| loss: 0.01473 | val_0_rmse: 0.17673 | val_0_mae: 0.12192 | val_0_mse: 0.03123 |  0:01:25s
    epoch 702| loss: 0.01414 | val_0_rmse: 0.17705 | val_0_mae: 0.12263 | val_0_mse: 0.03135 |  0:01:25s
    epoch 703| loss: 0.01453 | val_0_rmse: 0.17604 | val_0_mae: 0.12127 | val_0_mse: 0.03099 |  0:01:25s
    epoch 704| loss: 0.01395 | val_0_rmse: 0.17593 | val_0_mae: 0.12225 | val_0_mse: 0.03095 |  0:01:25s
    epoch 705| loss: 0.0138  | val_0_rmse: 0.17542 | val_0_mae: 0.12257 | val_0_mse: 0.03077 |  0:01:25s
    epoch 706| loss: 0.0136  | val_0_rmse: 0.17446 | val_0_mae: 0.12057 | val_0_mse: 0.03044 |  0:01:26s
    epoch 707| loss: 0.01379 | val_0_rmse: 0.17495 | val_0_mae: 0.12128 | val_0_mse: 0.03061 |  0:01:26s
    epoch 708| loss: 0.01363 | val_0_rmse: 0.17498 | val_0_mae: 0.12085 | val_0_mse: 0.03062 |  0:01:26s
    epoch 709| loss: 0.01329 | val_0_rmse: 0.17613 | val_0_mae: 0.12185 | val_0_mse: 0.03102 |  0:01:26s
    epoch 710| loss: 0.01406 | val_0_rmse: 0.17758 | val_0_mae: 0.12222 | val_0_mse: 0.03154 |  0:01:26s
    epoch 711| loss: 0.014   | val_0_rmse: 0.17655 | val_0_mae: 0.12276 | val_0_mse: 0.03117 |  0:01:26s
    epoch 712| loss: 0.01385 | val_0_rmse: 0.17585 | val_0_mae: 0.1211  | val_0_mse: 0.03092 |  0:01:26s
    epoch 713| loss: 0.01381 | val_0_rmse: 0.17722 | val_0_mae: 0.12223 | val_0_mse: 0.03141 |  0:01:26s
    epoch 714| loss: 0.01442 | val_0_rmse: 0.17748 | val_0_mae: 0.12427 | val_0_mse: 0.0315  |  0:01:26s
    epoch 715| loss: 0.01394 | val_0_rmse: 0.1771  | val_0_mae: 0.12356 | val_0_mse: 0.03136 |  0:01:27s
    epoch 716| loss: 0.01419 | val_0_rmse: 0.17798 | val_0_mae: 0.12236 | val_0_mse: 0.03168 |  0:01:27s
    epoch 717| loss: 0.01414 | val_0_rmse: 0.17834 | val_0_mae: 0.12403 | val_0_mse: 0.03181 |  0:01:27s
    epoch 718| loss: 0.01423 | val_0_rmse: 0.17845 | val_0_mae: 0.12437 | val_0_mse: 0.03184 |  0:01:27s
    epoch 719| loss: 0.01389 | val_0_rmse: 0.17807 | val_0_mae: 0.12444 | val_0_mse: 0.03171 |  0:01:27s
    epoch 720| loss: 0.01398 | val_0_rmse: 0.17823 | val_0_mae: 0.12273 | val_0_mse: 0.03177 |  0:01:27s
    epoch 721| loss: 0.01437 | val_0_rmse: 0.177   | val_0_mae: 0.1225  | val_0_mse: 0.03133 |  0:01:27s
    epoch 722| loss: 0.01357 | val_0_rmse: 0.17801 | val_0_mae: 0.12378 | val_0_mse: 0.03169 |  0:01:27s
    epoch 723| loss: 0.01363 | val_0_rmse: 0.1813  | val_0_mae: 0.12513 | val_0_mse: 0.03287 |  0:01:28s
    epoch 724| loss: 0.01317 | val_0_rmse: 0.18034 | val_0_mae: 0.12555 | val_0_mse: 0.03252 |  0:01:28s
    epoch 725| loss: 0.01396 | val_0_rmse: 0.17996 | val_0_mae: 0.1247  | val_0_mse: 0.03239 |  0:01:28s
    epoch 726| loss: 0.01417 | val_0_rmse: 0.179   | val_0_mae: 0.12343 | val_0_mse: 0.03204 |  0:01:28s
    epoch 727| loss: 0.01399 | val_0_rmse: 0.17809 | val_0_mae: 0.12402 | val_0_mse: 0.03172 |  0:01:28s
    epoch 728| loss: 0.01335 | val_0_rmse: 0.17733 | val_0_mae: 0.1224  | val_0_mse: 0.03145 |  0:01:28s
    epoch 729| loss: 0.01341 | val_0_rmse: 0.17849 | val_0_mae: 0.12309 | val_0_mse: 0.03186 |  0:01:28s
    epoch 730| loss: 0.01468 | val_0_rmse: 0.17943 | val_0_mae: 0.12654 | val_0_mse: 0.03219 |  0:01:28s
    epoch 731| loss: 0.01432 | val_0_rmse: 0.17693 | val_0_mae: 0.12291 | val_0_mse: 0.03131 |  0:01:28s
    epoch 732| loss: 0.01316 | val_0_rmse: 0.17769 | val_0_mae: 0.12258 | val_0_mse: 0.03157 |  0:01:29s
    epoch 733| loss: 0.01349 | val_0_rmse: 0.17802 | val_0_mae: 0.1258  | val_0_mse: 0.03169 |  0:01:29s
    epoch 734| loss: 0.01418 | val_0_rmse: 0.17713 | val_0_mae: 0.12356 | val_0_mse: 0.03137 |  0:01:29s
    epoch 735| loss: 0.01441 | val_0_rmse: 0.17725 | val_0_mae: 0.12336 | val_0_mse: 0.03142 |  0:01:29s
    epoch 736| loss: 0.01324 | val_0_rmse: 0.17862 | val_0_mae: 0.12684 | val_0_mse: 0.0319  |  0:01:29s
    epoch 737| loss: 0.0141  | val_0_rmse: 0.17696 | val_0_mae: 0.12221 | val_0_mse: 0.03131 |  0:01:29s
    epoch 738| loss: 0.01523 | val_0_rmse: 0.17614 | val_0_mae: 0.12116 | val_0_mse: 0.03102 |  0:01:29s
    epoch 739| loss: 0.01405 | val_0_rmse: 0.1779  | val_0_mae: 0.12543 | val_0_mse: 0.03165 |  0:01:29s
    epoch 740| loss: 0.01365 | val_0_rmse: 0.17768 | val_0_mae: 0.12338 | val_0_mse: 0.03157 |  0:01:30s
    epoch 741| loss: 0.01383 | val_0_rmse: 0.17607 | val_0_mae: 0.12188 | val_0_mse: 0.031   |  0:01:30s
    epoch 742| loss: 0.01447 | val_0_rmse: 0.17666 | val_0_mae: 0.12285 | val_0_mse: 0.03121 |  0:01:30s
    epoch 743| loss: 0.01332 | val_0_rmse: 0.17778 | val_0_mae: 0.12144 | val_0_mse: 0.03161 |  0:01:30s
    epoch 744| loss: 0.01328 | val_0_rmse: 0.17661 | val_0_mae: 0.12389 | val_0_mse: 0.03119 |  0:01:30s
    epoch 745| loss: 0.01386 | val_0_rmse: 0.1765  | val_0_mae: 0.12212 | val_0_mse: 0.03115 |  0:01:30s
    epoch 746| loss: 0.01285 | val_0_rmse: 0.17871 | val_0_mae: 0.12302 | val_0_mse: 0.03194 |  0:01:30s
    epoch 747| loss: 0.01395 | val_0_rmse: 0.17797 | val_0_mae: 0.12411 | val_0_mse: 0.03167 |  0:01:30s
    epoch 748| loss: 0.01359 | val_0_rmse: 0.17693 | val_0_mae: 0.1216  | val_0_mse: 0.03131 |  0:01:30s
    epoch 749| loss: 0.01453 | val_0_rmse: 0.17748 | val_0_mae: 0.12215 | val_0_mse: 0.0315  |  0:01:31s
    epoch 750| loss: 0.01367 | val_0_rmse: 0.17791 | val_0_mae: 0.12387 | val_0_mse: 0.03165 |  0:01:31s
    epoch 751| loss: 0.01318 | val_0_rmse: 0.17653 | val_0_mae: 0.1218  | val_0_mse: 0.03116 |  0:01:31s
    epoch 752| loss: 0.01404 | val_0_rmse: 0.17551 | val_0_mae: 0.12167 | val_0_mse: 0.0308  |  0:01:31s
    epoch 753| loss: 0.014   | val_0_rmse: 0.17549 | val_0_mae: 0.12152 | val_0_mse: 0.0308  |  0:01:31s
    epoch 754| loss: 0.01266 | val_0_rmse: 0.17592 | val_0_mae: 0.12149 | val_0_mse: 0.03095 |  0:01:31s
    epoch 755| loss: 0.01279 | val_0_rmse: 0.17591 | val_0_mae: 0.12143 | val_0_mse: 0.03095 |  0:01:31s
    epoch 756| loss: 0.01267 | val_0_rmse: 0.17735 | val_0_mae: 0.12332 | val_0_mse: 0.03145 |  0:01:31s
    epoch 757| loss: 0.01352 | val_0_rmse: 0.17742 | val_0_mae: 0.1228  | val_0_mse: 0.03148 |  0:01:31s
    epoch 758| loss: 0.01339 | val_0_rmse: 0.17637 | val_0_mae: 0.12145 | val_0_mse: 0.03111 |  0:01:32s
    epoch 759| loss: 0.01299 | val_0_rmse: 0.17587 | val_0_mae: 0.12212 | val_0_mse: 0.03093 |  0:01:32s
    epoch 760| loss: 0.01292 | val_0_rmse: 0.17646 | val_0_mae: 0.12147 | val_0_mse: 0.03114 |  0:01:32s
    epoch 761| loss: 0.01277 | val_0_rmse: 0.1767  | val_0_mae: 0.1215  | val_0_mse: 0.03122 |  0:01:32s
    epoch 762| loss: 0.01305 | val_0_rmse: 0.177   | val_0_mae: 0.12222 | val_0_mse: 0.03133 |  0:01:32s
    epoch 763| loss: 0.01336 | val_0_rmse: 0.17759 | val_0_mae: 0.12266 | val_0_mse: 0.03154 |  0:01:32s
    epoch 764| loss: 0.01352 | val_0_rmse: 0.17689 | val_0_mae: 0.12184 | val_0_mse: 0.03129 |  0:01:32s
    epoch 765| loss: 0.01293 | val_0_rmse: 0.1764  | val_0_mae: 0.12168 | val_0_mse: 0.03112 |  0:01:32s
    epoch 766| loss: 0.01331 | val_0_rmse: 0.17682 | val_0_mae: 0.12186 | val_0_mse: 0.03127 |  0:01:32s
    epoch 767| loss: 0.0127  | val_0_rmse: 0.17745 | val_0_mae: 0.12267 | val_0_mse: 0.03149 |  0:01:33s
    epoch 768| loss: 0.01298 | val_0_rmse: 0.17739 | val_0_mae: 0.12285 | val_0_mse: 0.03147 |  0:01:33s
    epoch 769| loss: 0.01357 | val_0_rmse: 0.17741 | val_0_mae: 0.12282 | val_0_mse: 0.03147 |  0:01:33s
    epoch 770| loss: 0.0142  | val_0_rmse: 0.17749 | val_0_mae: 0.12393 | val_0_mse: 0.0315  |  0:01:33s
    epoch 771| loss: 0.01379 | val_0_rmse: 0.17621 | val_0_mae: 0.12185 | val_0_mse: 0.03105 |  0:01:33s
    epoch 772| loss: 0.01281 | val_0_rmse: 0.17647 | val_0_mae: 0.12305 | val_0_mse: 0.03114 |  0:01:33s
    epoch 773| loss: 0.01297 | val_0_rmse: 0.17646 | val_0_mae: 0.12233 | val_0_mse: 0.03114 |  0:01:33s
    epoch 774| loss: 0.01219 | val_0_rmse: 0.17775 | val_0_mae: 0.12261 | val_0_mse: 0.0316  |  0:01:33s
    epoch 775| loss: 0.01369 | val_0_rmse: 0.1774  | val_0_mae: 0.1232  | val_0_mse: 0.03147 |  0:01:33s
    epoch 776| loss: 0.01316 | val_0_rmse: 0.17664 | val_0_mae: 0.12266 | val_0_mse: 0.0312  |  0:01:34s
    epoch 777| loss: 0.0133  | val_0_rmse: 0.17451 | val_0_mae: 0.12058 | val_0_mse: 0.03046 |  0:01:34s
    epoch 778| loss: 0.01338 | val_0_rmse: 0.17378 | val_0_mae: 0.1205  | val_0_mse: 0.0302  |  0:01:34s
    epoch 779| loss: 0.01318 | val_0_rmse: 0.17403 | val_0_mae: 0.12142 | val_0_mse: 0.03029 |  0:01:34s
    epoch 780| loss: 0.01304 | val_0_rmse: 0.17344 | val_0_mae: 0.11934 | val_0_mse: 0.03008 |  0:01:34s
    epoch 781| loss: 0.01337 | val_0_rmse: 0.17401 | val_0_mae: 0.11963 | val_0_mse: 0.03028 |  0:01:34s
    epoch 782| loss: 0.01268 | val_0_rmse: 0.17536 | val_0_mae: 0.12141 | val_0_mse: 0.03075 |  0:01:34s
    epoch 783| loss: 0.0123  | val_0_rmse: 0.17539 | val_0_mae: 0.12039 | val_0_mse: 0.03076 |  0:01:34s
    epoch 784| loss: 0.01273 | val_0_rmse: 0.17614 | val_0_mae: 0.12165 | val_0_mse: 0.03102 |  0:01:34s
    epoch 785| loss: 0.01271 | val_0_rmse: 0.17659 | val_0_mae: 0.12291 | val_0_mse: 0.03118 |  0:01:35s
    epoch 786| loss: 0.01296 | val_0_rmse: 0.17635 | val_0_mae: 0.12081 | val_0_mse: 0.0311  |  0:01:35s
    epoch 787| loss: 0.01343 | val_0_rmse: 0.17716 | val_0_mae: 0.12184 | val_0_mse: 0.03138 |  0:01:35s
    epoch 788| loss: 0.01482 | val_0_rmse: 0.17787 | val_0_mae: 0.12313 | val_0_mse: 0.03164 |  0:01:35s
    epoch 789| loss: 0.0131  | val_0_rmse: 0.17845 | val_0_mae: 0.1229  | val_0_mse: 0.03185 |  0:01:35s
    epoch 790| loss: 0.0127  | val_0_rmse: 0.17751 | val_0_mae: 0.12295 | val_0_mse: 0.03151 |  0:01:35s
    epoch 791| loss: 0.01299 | val_0_rmse: 0.17951 | val_0_mae: 0.12448 | val_0_mse: 0.03222 |  0:01:35s
    epoch 792| loss: 0.01261 | val_0_rmse: 0.17898 | val_0_mae: 0.12293 | val_0_mse: 0.03203 |  0:01:35s
    epoch 793| loss: 0.01304 | val_0_rmse: 0.17916 | val_0_mae: 0.12542 | val_0_mse: 0.0321  |  0:01:35s
    epoch 794| loss: 0.01315 | val_0_rmse: 0.1759  | val_0_mae: 0.12124 | val_0_mse: 0.03094 |  0:01:36s
    epoch 795| loss: 0.01259 | val_0_rmse: 0.1752  | val_0_mae: 0.12049 | val_0_mse: 0.03069 |  0:01:36s
    epoch 796| loss: 0.01316 | val_0_rmse: 0.17504 | val_0_mae: 0.12098 | val_0_mse: 0.03064 |  0:01:36s
    epoch 797| loss: 0.01288 | val_0_rmse: 0.17456 | val_0_mae: 0.1196  | val_0_mse: 0.03047 |  0:01:36s
    epoch 798| loss: 0.01242 | val_0_rmse: 0.17372 | val_0_mae: 0.11878 | val_0_mse: 0.03018 |  0:01:36s
    epoch 799| loss: 0.01293 | val_0_rmse: 0.17427 | val_0_mae: 0.11847 | val_0_mse: 0.03037 |  0:01:36s
    epoch 800| loss: 0.01228 | val_0_rmse: 0.17532 | val_0_mae: 0.11946 | val_0_mse: 0.03074 |  0:01:36s
    epoch 801| loss: 0.01309 | val_0_rmse: 0.17634 | val_0_mae: 0.12283 | val_0_mse: 0.0311  |  0:01:36s
    epoch 802| loss: 0.01294 | val_0_rmse: 0.17535 | val_0_mae: 0.11971 | val_0_mse: 0.03075 |  0:01:36s
    epoch 803| loss: 0.01411 | val_0_rmse: 0.17603 | val_0_mae: 0.11941 | val_0_mse: 0.03099 |  0:01:37s
    epoch 804| loss: 0.01294 | val_0_rmse: 0.17757 | val_0_mae: 0.12247 | val_0_mse: 0.03153 |  0:01:37s
    epoch 805| loss: 0.01288 | val_0_rmse: 0.17688 | val_0_mae: 0.12057 | val_0_mse: 0.03128 |  0:01:37s
    epoch 806| loss: 0.01252 | val_0_rmse: 0.1759  | val_0_mae: 0.11999 | val_0_mse: 0.03094 |  0:01:37s
    epoch 807| loss: 0.01215 | val_0_rmse: 0.17541 | val_0_mae: 0.12045 | val_0_mse: 0.03077 |  0:01:37s
    epoch 808| loss: 0.01225 | val_0_rmse: 0.17412 | val_0_mae: 0.1189  | val_0_mse: 0.03032 |  0:01:37s
    epoch 809| loss: 0.01262 | val_0_rmse: 0.1733  | val_0_mae: 0.11914 | val_0_mse: 0.03003 |  0:01:37s
    epoch 810| loss: 0.01213 | val_0_rmse: 0.17338 | val_0_mae: 0.11878 | val_0_mse: 0.03006 |  0:01:37s
    epoch 811| loss: 0.01297 | val_0_rmse: 0.17393 | val_0_mae: 0.1192  | val_0_mse: 0.03025 |  0:01:38s
    epoch 812| loss: 0.01277 | val_0_rmse: 0.17438 | val_0_mae: 0.12054 | val_0_mse: 0.03041 |  0:01:38s
    epoch 813| loss: 0.01251 | val_0_rmse: 0.17457 | val_0_mae: 0.11898 | val_0_mse: 0.03047 |  0:01:38s
    epoch 814| loss: 0.01245 | val_0_rmse: 0.17441 | val_0_mae: 0.11874 | val_0_mse: 0.03042 |  0:01:38s
    epoch 815| loss: 0.01259 | val_0_rmse: 0.17496 | val_0_mae: 0.11929 | val_0_mse: 0.03061 |  0:01:38s
    epoch 816| loss: 0.01223 | val_0_rmse: 0.17597 | val_0_mae: 0.11945 | val_0_mse: 0.03096 |  0:01:38s
    epoch 817| loss: 0.01218 | val_0_rmse: 0.17595 | val_0_mae: 0.12036 | val_0_mse: 0.03096 |  0:01:38s
    epoch 818| loss: 0.01275 | val_0_rmse: 0.1761  | val_0_mae: 0.11952 | val_0_mse: 0.03101 |  0:01:38s
    epoch 819| loss: 0.01248 | val_0_rmse: 0.17637 | val_0_mae: 0.12039 | val_0_mse: 0.03111 |  0:01:38s
    epoch 820| loss: 0.01318 | val_0_rmse: 0.17656 | val_0_mae: 0.12154 | val_0_mse: 0.03117 |  0:01:39s
    epoch 821| loss: 0.01241 | val_0_rmse: 0.17582 | val_0_mae: 0.12041 | val_0_mse: 0.03091 |  0:01:39s
    epoch 822| loss: 0.01341 | val_0_rmse: 0.17457 | val_0_mae: 0.12053 | val_0_mse: 0.03047 |  0:01:39s
    epoch 823| loss: 0.01244 | val_0_rmse: 0.17472 | val_0_mae: 0.12061 | val_0_mse: 0.03053 |  0:01:39s
    epoch 824| loss: 0.01322 | val_0_rmse: 0.17466 | val_0_mae: 0.11938 | val_0_mse: 0.03051 |  0:01:39s
    epoch 825| loss: 0.01305 | val_0_rmse: 0.17324 | val_0_mae: 0.11933 | val_0_mse: 0.03001 |  0:01:39s
    epoch 826| loss: 0.01409 | val_0_rmse: 0.17303 | val_0_mae: 0.11903 | val_0_mse: 0.02994 |  0:01:40s
    epoch 827| loss: 0.01372 | val_0_rmse: 0.17318 | val_0_mae: 0.11855 | val_0_mse: 0.02999 |  0:01:40s
    epoch 828| loss: 0.01293 | val_0_rmse: 0.17295 | val_0_mae: 0.1194  | val_0_mse: 0.02991 |  0:01:40s
    epoch 829| loss: 0.01341 | val_0_rmse: 0.17237 | val_0_mae: 0.11775 | val_0_mse: 0.02971 |  0:01:40s
    epoch 830| loss: 0.01251 | val_0_rmse: 0.17344 | val_0_mae: 0.11877 | val_0_mse: 0.03008 |  0:01:40s
    epoch 831| loss: 0.01293 | val_0_rmse: 0.17285 | val_0_mae: 0.11891 | val_0_mse: 0.02988 |  0:01:40s
    epoch 832| loss: 0.01319 | val_0_rmse: 0.17354 | val_0_mae: 0.11887 | val_0_mse: 0.03012 |  0:01:40s
    epoch 833| loss: 0.01353 | val_0_rmse: 0.17417 | val_0_mae: 0.11863 | val_0_mse: 0.03034 |  0:01:40s
    epoch 834| loss: 0.01335 | val_0_rmse: 0.17509 | val_0_mae: 0.1215  | val_0_mse: 0.03066 |  0:01:41s
    epoch 835| loss: 0.01287 | val_0_rmse: 0.17478 | val_0_mae: 0.11986 | val_0_mse: 0.03055 |  0:01:41s
    epoch 836| loss: 0.01208 | val_0_rmse: 0.17584 | val_0_mae: 0.12057 | val_0_mse: 0.03092 |  0:01:41s
    epoch 837| loss: 0.01253 | val_0_rmse: 0.17439 | val_0_mae: 0.11985 | val_0_mse: 0.03041 |  0:01:41s
    epoch 838| loss: 0.01348 | val_0_rmse: 0.17449 | val_0_mae: 0.11913 | val_0_mse: 0.03045 |  0:01:41s
    epoch 839| loss: 0.01247 | val_0_rmse: 0.17706 | val_0_mae: 0.1214  | val_0_mse: 0.03135 |  0:01:41s
    epoch 840| loss: 0.0128  | val_0_rmse: 0.17639 | val_0_mae: 0.11922 | val_0_mse: 0.03111 |  0:01:41s
    epoch 841| loss: 0.01283 | val_0_rmse: 0.17561 | val_0_mae: 0.12063 | val_0_mse: 0.03084 |  0:01:42s
    epoch 842| loss: 0.01259 | val_0_rmse: 0.17471 | val_0_mae: 0.11953 | val_0_mse: 0.03052 |  0:01:42s
    epoch 843| loss: 0.01251 | val_0_rmse: 0.17629 | val_0_mae: 0.12042 | val_0_mse: 0.03108 |  0:01:42s
    epoch 844| loss: 0.01227 | val_0_rmse: 0.17517 | val_0_mae: 0.12071 | val_0_mse: 0.03068 |  0:01:42s
    epoch 845| loss: 0.01277 | val_0_rmse: 0.1758  | val_0_mae: 0.12069 | val_0_mse: 0.03091 |  0:01:42s
    epoch 846| loss: 0.01235 | val_0_rmse: 0.1759  | val_0_mae: 0.12039 | val_0_mse: 0.03094 |  0:01:42s
    epoch 847| loss: 0.01227 | val_0_rmse: 0.17308 | val_0_mae: 0.11859 | val_0_mse: 0.02996 |  0:01:42s
    epoch 848| loss: 0.0126  | val_0_rmse: 0.17237 | val_0_mae: 0.11883 | val_0_mse: 0.02971 |  0:01:42s
    epoch 849| loss: 0.01202 | val_0_rmse: 0.17255 | val_0_mae: 0.11757 | val_0_mse: 0.02977 |  0:01:43s
    epoch 850| loss: 0.01272 | val_0_rmse: 0.17118 | val_0_mae: 0.11745 | val_0_mse: 0.0293  |  0:01:43s
    epoch 851| loss: 0.01266 | val_0_rmse: 0.17075 | val_0_mae: 0.11706 | val_0_mse: 0.02915 |  0:01:43s
    epoch 852| loss: 0.01224 | val_0_rmse: 0.17264 | val_0_mae: 0.11984 | val_0_mse: 0.0298  |  0:01:43s
    epoch 853| loss: 0.01274 | val_0_rmse: 0.17228 | val_0_mae: 0.11826 | val_0_mse: 0.02968 |  0:01:43s
    epoch 854| loss: 0.01195 | val_0_rmse: 0.17191 | val_0_mae: 0.11914 | val_0_mse: 0.02955 |  0:01:43s
    epoch 855| loss: 0.01204 | val_0_rmse: 0.1708  | val_0_mae: 0.11674 | val_0_mse: 0.02917 |  0:01:43s
    epoch 856| loss: 0.01293 | val_0_rmse: 0.17103 | val_0_mae: 0.11669 | val_0_mse: 0.02925 |  0:01:43s
    epoch 857| loss: 0.0131  | val_0_rmse: 0.17254 | val_0_mae: 0.11956 | val_0_mse: 0.02977 |  0:01:44s
    epoch 858| loss: 0.01301 | val_0_rmse: 0.17235 | val_0_mae: 0.11646 | val_0_mse: 0.0297  |  0:01:44s
    epoch 859| loss: 0.01169 | val_0_rmse: 0.17146 | val_0_mae: 0.11678 | val_0_mse: 0.0294  |  0:01:44s
    epoch 860| loss: 0.01222 | val_0_rmse: 0.17178 | val_0_mae: 0.1175  | val_0_mse: 0.02951 |  0:01:44s
    epoch 861| loss: 0.01235 | val_0_rmse: 0.17249 | val_0_mae: 0.11753 | val_0_mse: 0.02975 |  0:01:44s
    epoch 862| loss: 0.01257 | val_0_rmse: 0.17356 | val_0_mae: 0.11865 | val_0_mse: 0.03012 |  0:01:44s
    epoch 863| loss: 0.01179 | val_0_rmse: 0.17316 | val_0_mae: 0.11848 | val_0_mse: 0.02998 |  0:01:44s
    epoch 864| loss: 0.01221 | val_0_rmse: 0.17373 | val_0_mae: 0.11856 | val_0_mse: 0.03018 |  0:01:44s
    epoch 865| loss: 0.01311 | val_0_rmse: 0.1736  | val_0_mae: 0.11803 | val_0_mse: 0.03014 |  0:01:45s
    epoch 866| loss: 0.01228 | val_0_rmse: 0.17276 | val_0_mae: 0.11809 | val_0_mse: 0.02985 |  0:01:45s
    epoch 867| loss: 0.01333 | val_0_rmse: 0.17117 | val_0_mae: 0.11658 | val_0_mse: 0.0293  |  0:01:45s
    epoch 868| loss: 0.01352 | val_0_rmse: 0.17273 | val_0_mae: 0.11711 | val_0_mse: 0.02983 |  0:01:45s
    epoch 869| loss: 0.01268 | val_0_rmse: 0.17391 | val_0_mae: 0.12102 | val_0_mse: 0.03025 |  0:01:45s
    epoch 870| loss: 0.01311 | val_0_rmse: 0.1724  | val_0_mae: 0.11724 | val_0_mse: 0.02972 |  0:01:45s
    epoch 871| loss: 0.01234 | val_0_rmse: 0.17188 | val_0_mae: 0.11818 | val_0_mse: 0.02954 |  0:01:45s
    epoch 872| loss: 0.01294 | val_0_rmse: 0.17179 | val_0_mae: 0.11808 | val_0_mse: 0.02951 |  0:01:45s
    epoch 873| loss: 0.01266 | val_0_rmse: 0.17447 | val_0_mae: 0.11902 | val_0_mse: 0.03044 |  0:01:45s
    epoch 874| loss: 0.01193 | val_0_rmse: 0.17334 | val_0_mae: 0.11905 | val_0_mse: 0.03005 |  0:01:45s
    epoch 875| loss: 0.01239 | val_0_rmse: 0.17364 | val_0_mae: 0.11859 | val_0_mse: 0.03015 |  0:01:46s
    epoch 876| loss: 0.01233 | val_0_rmse: 0.17504 | val_0_mae: 0.11999 | val_0_mse: 0.03064 |  0:01:46s
    epoch 877| loss: 0.01205 | val_0_rmse: 0.17519 | val_0_mae: 0.1213  | val_0_mse: 0.03069 |  0:01:46s
    epoch 878| loss: 0.01296 | val_0_rmse: 0.17375 | val_0_mae: 0.11922 | val_0_mse: 0.03019 |  0:01:46s
    epoch 879| loss: 0.01299 | val_0_rmse: 0.17249 | val_0_mae: 0.1187  | val_0_mse: 0.02975 |  0:01:46s
    epoch 880| loss: 0.01241 | val_0_rmse: 0.17261 | val_0_mae: 0.11928 | val_0_mse: 0.02979 |  0:01:46s
    epoch 881| loss: 0.01247 | val_0_rmse: 0.17669 | val_0_mae: 0.12014 | val_0_mse: 0.03122 |  0:01:46s
    epoch 882| loss: 0.01233 | val_0_rmse: 0.17387 | val_0_mae: 0.11978 | val_0_mse: 0.03023 |  0:01:46s
    epoch 883| loss: 0.01309 | val_0_rmse: 0.17411 | val_0_mae: 0.11881 | val_0_mse: 0.03031 |  0:01:46s
    epoch 884| loss: 0.01201 | val_0_rmse: 0.17463 | val_0_mae: 0.11893 | val_0_mse: 0.0305  |  0:01:46s
    epoch 885| loss: 0.01196 | val_0_rmse: 0.17483 | val_0_mae: 0.12097 | val_0_mse: 0.03056 |  0:01:47s
    epoch 886| loss: 0.0122  | val_0_rmse: 0.17351 | val_0_mae: 0.11766 | val_0_mse: 0.03011 |  0:01:47s
    epoch 887| loss: 0.01219 | val_0_rmse: 0.17386 | val_0_mae: 0.11818 | val_0_mse: 0.03023 |  0:01:47s
    epoch 888| loss: 0.01132 | val_0_rmse: 0.17571 | val_0_mae: 0.12167 | val_0_mse: 0.03088 |  0:01:47s
    epoch 889| loss: 0.01239 | val_0_rmse: 0.17464 | val_0_mae: 0.11828 | val_0_mse: 0.0305  |  0:01:47s
    epoch 890| loss: 0.01244 | val_0_rmse: 0.17221 | val_0_mae: 0.11729 | val_0_mse: 0.02966 |  0:01:47s
    epoch 891| loss: 0.01245 | val_0_rmse: 0.17228 | val_0_mae: 0.11786 | val_0_mse: 0.02968 |  0:01:47s
    epoch 892| loss: 0.01166 | val_0_rmse: 0.17222 | val_0_mae: 0.11671 | val_0_mse: 0.02966 |  0:01:47s
    epoch 893| loss: 0.01166 | val_0_rmse: 0.17178 | val_0_mae: 0.11665 | val_0_mse: 0.02951 |  0:01:47s
    epoch 894| loss: 0.01189 | val_0_rmse: 0.17188 | val_0_mae: 0.11687 | val_0_mse: 0.02954 |  0:01:48s
    epoch 895| loss: 0.01143 | val_0_rmse: 0.17232 | val_0_mae: 0.11685 | val_0_mse: 0.0297  |  0:01:48s
    epoch 896| loss: 0.01189 | val_0_rmse: 0.17369 | val_0_mae: 0.11895 | val_0_mse: 0.03017 |  0:01:48s
    epoch 897| loss: 0.01171 | val_0_rmse: 0.17286 | val_0_mae: 0.11782 | val_0_mse: 0.02988 |  0:01:48s
    epoch 898| loss: 0.01179 | val_0_rmse: 0.17308 | val_0_mae: 0.11795 | val_0_mse: 0.02996 |  0:01:48s
    epoch 899| loss: 0.0117  | val_0_rmse: 0.17444 | val_0_mae: 0.12048 | val_0_mse: 0.03043 |  0:01:48s
    epoch 900| loss: 0.01174 | val_0_rmse: 0.17379 | val_0_mae: 0.11859 | val_0_mse: 0.0302  |  0:01:48s
    epoch 901| loss: 0.01195 | val_0_rmse: 0.17328 | val_0_mae: 0.11887 | val_0_mse: 0.03003 |  0:01:48s
    epoch 902| loss: 0.01195 | val_0_rmse: 0.17299 | val_0_mae: 0.11835 | val_0_mse: 0.02992 |  0:01:49s
    epoch 903| loss: 0.01099 | val_0_rmse: 0.17409 | val_0_mae: 0.11882 | val_0_mse: 0.03031 |  0:01:49s
    epoch 904| loss: 0.01175 | val_0_rmse: 0.17463 | val_0_mae: 0.11881 | val_0_mse: 0.0305  |  0:01:49s
    epoch 905| loss: 0.01147 | val_0_rmse: 0.17602 | val_0_mae: 0.12171 | val_0_mse: 0.03098 |  0:01:49s
    epoch 906| loss: 0.0118  | val_0_rmse: 0.17479 | val_0_mae: 0.11896 | val_0_mse: 0.03055 |  0:01:49s
    epoch 907| loss: 0.01145 | val_0_rmse: 0.17414 | val_0_mae: 0.11861 | val_0_mse: 0.03032 |  0:01:49s
    epoch 908| loss: 0.01163 | val_0_rmse: 0.174   | val_0_mae: 0.11835 | val_0_mse: 0.03028 |  0:01:49s
    epoch 909| loss: 0.01066 | val_0_rmse: 0.17437 | val_0_mae: 0.11884 | val_0_mse: 0.0304  |  0:01:49s
    epoch 910| loss: 0.01161 | val_0_rmse: 0.17458 | val_0_mae: 0.11818 | val_0_mse: 0.03048 |  0:01:49s
    epoch 911| loss: 0.01138 | val_0_rmse: 0.17493 | val_0_mae: 0.11876 | val_0_mse: 0.0306  |  0:01:50s
    epoch 912| loss: 0.01161 | val_0_rmse: 0.17494 | val_0_mae: 0.11841 | val_0_mse: 0.0306  |  0:01:50s
    epoch 913| loss: 0.011   | val_0_rmse: 0.17542 | val_0_mae: 0.11854 | val_0_mse: 0.03077 |  0:01:50s
    epoch 914| loss: 0.01114 | val_0_rmse: 0.17491 | val_0_mae: 0.11852 | val_0_mse: 0.03059 |  0:01:50s
    epoch 915| loss: 0.01143 | val_0_rmse: 0.17368 | val_0_mae: 0.11821 | val_0_mse: 0.03017 |  0:01:50s
    epoch 916| loss: 0.0117  | val_0_rmse: 0.17282 | val_0_mae: 0.11854 | val_0_mse: 0.02987 |  0:01:50s
    epoch 917| loss: 0.01217 | val_0_rmse: 0.17132 | val_0_mae: 0.11692 | val_0_mse: 0.02935 |  0:01:50s
    epoch 918| loss: 0.01115 | val_0_rmse: 0.17119 | val_0_mae: 0.1167  | val_0_mse: 0.0293  |  0:01:50s
    epoch 919| loss: 0.01172 | val_0_rmse: 0.17204 | val_0_mae: 0.11808 | val_0_mse: 0.0296  |  0:01:50s
    epoch 920| loss: 0.01241 | val_0_rmse: 0.17169 | val_0_mae: 0.11664 | val_0_mse: 0.02948 |  0:01:50s
    epoch 921| loss: 0.01247 | val_0_rmse: 0.17188 | val_0_mae: 0.11724 | val_0_mse: 0.02954 |  0:01:51s
    epoch 922| loss: 0.01179 | val_0_rmse: 0.17235 | val_0_mae: 0.11761 | val_0_mse: 0.0297  |  0:01:51s
    epoch 923| loss: 0.01119 | val_0_rmse: 0.17346 | val_0_mae: 0.11756 | val_0_mse: 0.03009 |  0:01:51s
    epoch 924| loss: 0.01126 | val_0_rmse: 0.17384 | val_0_mae: 0.11792 | val_0_mse: 0.03022 |  0:01:51s
    epoch 925| loss: 0.0116  | val_0_rmse: 0.17317 | val_0_mae: 0.11762 | val_0_mse: 0.02999 |  0:01:51s
    epoch 926| loss: 0.01095 | val_0_rmse: 0.17388 | val_0_mae: 0.11861 | val_0_mse: 0.03023 |  0:01:51s
    epoch 927| loss: 0.01211 | val_0_rmse: 0.17398 | val_0_mae: 0.11771 | val_0_mse: 0.03027 |  0:01:51s
    epoch 928| loss: 0.01124 | val_0_rmse: 0.17411 | val_0_mae: 0.1192  | val_0_mse: 0.03032 |  0:01:51s
    epoch 929| loss: 0.01138 | val_0_rmse: 0.17355 | val_0_mae: 0.11843 | val_0_mse: 0.03012 |  0:01:51s
    epoch 930| loss: 0.01099 | val_0_rmse: 0.17261 | val_0_mae: 0.11748 | val_0_mse: 0.02979 |  0:01:52s
    epoch 931| loss: 0.01123 | val_0_rmse: 0.17441 | val_0_mae: 0.12142 | val_0_mse: 0.03042 |  0:01:52s
    epoch 932| loss: 0.01168 | val_0_rmse: 0.17318 | val_0_mae: 0.11858 | val_0_mse: 0.02999 |  0:01:52s
    epoch 933| loss: 0.01128 | val_0_rmse: 0.17359 | val_0_mae: 0.11866 | val_0_mse: 0.03013 |  0:01:52s
    epoch 934| loss: 0.01202 | val_0_rmse: 0.1743  | val_0_mae: 0.12161 | val_0_mse: 0.03038 |  0:01:52s
    epoch 935| loss: 0.01195 | val_0_rmse: 0.17342 | val_0_mae: 0.11855 | val_0_mse: 0.03008 |  0:01:52s
    epoch 936| loss: 0.0108  | val_0_rmse: 0.1736  | val_0_mae: 0.11801 | val_0_mse: 0.03014 |  0:01:52s
    epoch 937| loss: 0.01138 | val_0_rmse: 0.17371 | val_0_mae: 0.11976 | val_0_mse: 0.03018 |  0:01:52s
    epoch 938| loss: 0.0115  | val_0_rmse: 0.17284 | val_0_mae: 0.11712 | val_0_mse: 0.02987 |  0:01:52s
    epoch 939| loss: 0.01127 | val_0_rmse: 0.17451 | val_0_mae: 0.12006 | val_0_mse: 0.03045 |  0:01:52s
    epoch 940| loss: 0.01187 | val_0_rmse: 0.17391 | val_0_mae: 0.11967 | val_0_mse: 0.03025 |  0:01:53s
    epoch 941| loss: 0.01123 | val_0_rmse: 0.17284 | val_0_mae: 0.11768 | val_0_mse: 0.02987 |  0:01:53s
    epoch 942| loss: 0.01248 | val_0_rmse: 0.17245 | val_0_mae: 0.11748 | val_0_mse: 0.02974 |  0:01:53s
    epoch 943| loss: 0.01167 | val_0_rmse: 0.17185 | val_0_mae: 0.11703 | val_0_mse: 0.02953 |  0:01:53s
    epoch 944| loss: 0.01126 | val_0_rmse: 0.17206 | val_0_mae: 0.11777 | val_0_mse: 0.0296  |  0:01:53s
    epoch 945| loss: 0.01116 | val_0_rmse: 0.17238 | val_0_mae: 0.11727 | val_0_mse: 0.02971 |  0:01:53s
    epoch 946| loss: 0.01202 | val_0_rmse: 0.17166 | val_0_mae: 0.11711 | val_0_mse: 0.02947 |  0:01:53s
    epoch 947| loss: 0.01155 | val_0_rmse: 0.17158 | val_0_mae: 0.11769 | val_0_mse: 0.02944 |  0:01:53s
    epoch 948| loss: 0.01131 | val_0_rmse: 0.17255 | val_0_mae: 0.11743 | val_0_mse: 0.02977 |  0:01:54s
    epoch 949| loss: 0.01089 | val_0_rmse: 0.17283 | val_0_mae: 0.11783 | val_0_mse: 0.02987 |  0:01:54s
    epoch 950| loss: 0.01112 | val_0_rmse: 0.17341 | val_0_mae: 0.11769 | val_0_mse: 0.03007 |  0:01:54s
    epoch 951| loss: 0.01124 | val_0_rmse: 0.17401 | val_0_mae: 0.11851 | val_0_mse: 0.03028 |  0:01:54s
    epoch 952| loss: 0.0115  | val_0_rmse: 0.17363 | val_0_mae: 0.11892 | val_0_mse: 0.03015 |  0:01:54s
    epoch 953| loss: 0.01103 | val_0_rmse: 0.17353 | val_0_mae: 0.11876 | val_0_mse: 0.03011 |  0:01:54s
    epoch 954| loss: 0.01073 | val_0_rmse: 0.17482 | val_0_mae: 0.11911 | val_0_mse: 0.03056 |  0:01:54s
    epoch 955| loss: 0.01098 | val_0_rmse: 0.17469 | val_0_mae: 0.11858 | val_0_mse: 0.03052 |  0:01:54s
    epoch 956| loss: 0.01117 | val_0_rmse: 0.17465 | val_0_mae: 0.11772 | val_0_mse: 0.0305  |  0:01:54s
    epoch 957| loss: 0.01222 | val_0_rmse: 0.17391 | val_0_mae: 0.118   | val_0_mse: 0.03024 |  0:01:55s
    epoch 958| loss: 0.0111  | val_0_rmse: 0.17388 | val_0_mae: 0.1177  | val_0_mse: 0.03024 |  0:01:55s
    epoch 959| loss: 0.01106 | val_0_rmse: 0.1733  | val_0_mae: 0.11811 | val_0_mse: 0.03003 |  0:01:55s
    epoch 960| loss: 0.01109 | val_0_rmse: 0.17296 | val_0_mae: 0.11814 | val_0_mse: 0.02991 |  0:01:55s
    epoch 961| loss: 0.01208 | val_0_rmse: 0.17349 | val_0_mae: 0.11876 | val_0_mse: 0.0301  |  0:01:55s
    epoch 962| loss: 0.01084 | val_0_rmse: 0.17277 | val_0_mae: 0.11799 | val_0_mse: 0.02985 |  0:01:55s
    epoch 963| loss: 0.01143 | val_0_rmse: 0.17273 | val_0_mae: 0.1189  | val_0_mse: 0.02983 |  0:01:55s
    epoch 964| loss: 0.01182 | val_0_rmse: 0.17199 | val_0_mae: 0.11799 | val_0_mse: 0.02958 |  0:01:55s
    epoch 965| loss: 0.01159 | val_0_rmse: 0.17371 | val_0_mae: 0.11858 | val_0_mse: 0.03017 |  0:01:56s
    epoch 966| loss: 0.01128 | val_0_rmse: 0.17359 | val_0_mae: 0.11745 | val_0_mse: 0.03013 |  0:01:56s
    epoch 967| loss: 0.01243 | val_0_rmse: 0.17491 | val_0_mae: 0.12033 | val_0_mse: 0.03059 |  0:01:56s
    epoch 968| loss: 0.01213 | val_0_rmse: 0.17562 | val_0_mae: 0.12011 | val_0_mse: 0.03084 |  0:01:56s
    epoch 969| loss: 0.01216 | val_0_rmse: 0.17741 | val_0_mae: 0.12145 | val_0_mse: 0.03147 |  0:01:56s
    epoch 970| loss: 0.01182 | val_0_rmse: 0.17507 | val_0_mae: 0.11996 | val_0_mse: 0.03065 |  0:01:56s
    epoch 971| loss: 0.01122 | val_0_rmse: 0.17427 | val_0_mae: 0.11851 | val_0_mse: 0.03037 |  0:01:56s
    epoch 972| loss: 0.01114 | val_0_rmse: 0.17434 | val_0_mae: 0.11786 | val_0_mse: 0.03039 |  0:01:56s
    epoch 973| loss: 0.01192 | val_0_rmse: 0.1724  | val_0_mae: 0.11767 | val_0_mse: 0.02972 |  0:01:56s
    epoch 974| loss: 0.0117  | val_0_rmse: 0.17215 | val_0_mae: 0.11704 | val_0_mse: 0.02964 |  0:01:57s
    epoch 975| loss: 0.01096 | val_0_rmse: 0.17385 | val_0_mae: 0.11861 | val_0_mse: 0.03022 |  0:01:57s
    epoch 976| loss: 0.01182 | val_0_rmse: 0.17317 | val_0_mae: 0.11844 | val_0_mse: 0.02999 |  0:01:57s
    epoch 977| loss: 0.01134 | val_0_rmse: 0.17378 | val_0_mae: 0.11783 | val_0_mse: 0.0302  |  0:01:57s
    epoch 978| loss: 0.01122 | val_0_rmse: 0.17354 | val_0_mae: 0.11725 | val_0_mse: 0.03012 |  0:01:57s
    epoch 979| loss: 0.01093 | val_0_rmse: 0.17319 | val_0_mae: 0.11802 | val_0_mse: 0.02999 |  0:01:57s
    epoch 980| loss: 0.01082 | val_0_rmse: 0.17322 | val_0_mae: 0.11733 | val_0_mse: 0.03001 |  0:01:57s
    epoch 981| loss: 0.01121 | val_0_rmse: 0.17259 | val_0_mae: 0.11683 | val_0_mse: 0.02979 |  0:01:57s
    epoch 982| loss: 0.01118 | val_0_rmse: 0.17231 | val_0_mae: 0.11708 | val_0_mse: 0.02969 |  0:01:57s
    epoch 983| loss: 0.01155 | val_0_rmse: 0.17192 | val_0_mae: 0.11655 | val_0_mse: 0.02956 |  0:01:58s
    epoch 984| loss: 0.01082 | val_0_rmse: 0.17103 | val_0_mae: 0.11556 | val_0_mse: 0.02925 |  0:01:58s
    epoch 985| loss: 0.01083 | val_0_rmse: 0.17071 | val_0_mae: 0.11722 | val_0_mse: 0.02914 |  0:01:58s
    epoch 986| loss: 0.01118 | val_0_rmse: 0.16957 | val_0_mae: 0.11683 | val_0_mse: 0.02876 |  0:01:58s
    epoch 987| loss: 0.01089 | val_0_rmse: 0.17003 | val_0_mae: 0.1171  | val_0_mse: 0.02891 |  0:01:58s
    epoch 988| loss: 0.01131 | val_0_rmse: 0.1699  | val_0_mae: 0.1164  | val_0_mse: 0.02887 |  0:01:58s
    epoch 989| loss: 0.01063 | val_0_rmse: 0.17058 | val_0_mae: 0.11763 | val_0_mse: 0.0291  |  0:01:58s
    epoch 990| loss: 0.01065 | val_0_rmse: 0.17079 | val_0_mae: 0.11704 | val_0_mse: 0.02917 |  0:01:58s
    epoch 991| loss: 0.01025 | val_0_rmse: 0.173   | val_0_mae: 0.12031 | val_0_mse: 0.02993 |  0:01:58s
    epoch 992| loss: 0.01076 | val_0_rmse: 0.17184 | val_0_mae: 0.11691 | val_0_mse: 0.02953 |  0:01:59s
    epoch 993| loss: 0.01076 | val_0_rmse: 0.17272 | val_0_mae: 0.11686 | val_0_mse: 0.02983 |  0:01:59s
    epoch 994| loss: 0.01046 | val_0_rmse: 0.17382 | val_0_mae: 0.11889 | val_0_mse: 0.03021 |  0:01:59s
    epoch 995| loss: 0.01117 | val_0_rmse: 0.17433 | val_0_mae: 0.11771 | val_0_mse: 0.03039 |  0:01:59s
    epoch 996| loss: 0.01234 | val_0_rmse: 0.1749  | val_0_mae: 0.11887 | val_0_mse: 0.03059 |  0:01:59s
    epoch 997| loss: 0.01133 | val_0_rmse: 0.17438 | val_0_mae: 0.11894 | val_0_mse: 0.03041 |  0:01:59s
    epoch 998| loss: 0.01076 | val_0_rmse: 0.17391 | val_0_mae: 0.11766 | val_0_mse: 0.03024 |  0:01:59s
    epoch 999| loss: 0.01141 | val_0_rmse: 0.17318 | val_0_mae: 0.11775 | val_0_mse: 0.02999 |  0:01:59s
    epoch 1000| loss: 0.01088 | val_0_rmse: 0.17306 | val_0_mae: 0.11652 | val_0_mse: 0.02995 |  0:01:59s
    epoch 1001| loss: 0.01055 | val_0_rmse: 0.17465 | val_0_mae: 0.11872 | val_0_mse: 0.0305  |  0:02:00s
    epoch 1002| loss: 0.01181 | val_0_rmse: 0.17436 | val_0_mae: 0.1191  | val_0_mse: 0.0304  |  0:02:00s
    epoch 1003| loss: 0.01071 | val_0_rmse: 0.17298 | val_0_mae: 0.11752 | val_0_mse: 0.02992 |  0:02:00s
    epoch 1004| loss: 0.01129 | val_0_rmse: 0.17384 | val_0_mae: 0.11782 | val_0_mse: 0.03022 |  0:02:00s
    epoch 1005| loss: 0.0115  | val_0_rmse: 0.17407 | val_0_mae: 0.11886 | val_0_mse: 0.0303  |  0:02:00s
    epoch 1006| loss: 0.01059 | val_0_rmse: 0.17392 | val_0_mae: 0.11838 | val_0_mse: 0.03025 |  0:02:00s
    epoch 1007| loss: 0.01127 | val_0_rmse: 0.17298 | val_0_mae: 0.11728 | val_0_mse: 0.02992 |  0:02:00s
    epoch 1008| loss: 0.01095 | val_0_rmse: 0.173   | val_0_mae: 0.11755 | val_0_mse: 0.02993 |  0:02:00s
    epoch 1009| loss: 0.01055 | val_0_rmse: 0.1737  | val_0_mae: 0.11814 | val_0_mse: 0.03017 |  0:02:00s
    epoch 1010| loss: 0.01107 | val_0_rmse: 0.17312 | val_0_mae: 0.1177  | val_0_mse: 0.02997 |  0:02:01s
    epoch 1011| loss: 0.01097 | val_0_rmse: 0.17432 | val_0_mae: 0.11833 | val_0_mse: 0.03039 |  0:02:01s
    epoch 1012| loss: 0.01149 | val_0_rmse: 0.17541 | val_0_mae: 0.11911 | val_0_mse: 0.03077 |  0:02:01s
    epoch 1013| loss: 0.01126 | val_0_rmse: 0.17539 | val_0_mae: 0.11953 | val_0_mse: 0.03076 |  0:02:01s
    epoch 1014| loss: 0.0111  | val_0_rmse: 0.17501 | val_0_mae: 0.11967 | val_0_mse: 0.03063 |  0:02:01s
    epoch 1015| loss: 0.01137 | val_0_rmse: 0.17528 | val_0_mae: 0.11974 | val_0_mse: 0.03072 |  0:02:01s
    epoch 1016| loss: 0.01036 | val_0_rmse: 0.17399 | val_0_mae: 0.11843 | val_0_mse: 0.03027 |  0:02:01s
    epoch 1017| loss: 0.0107  | val_0_rmse: 0.1736  | val_0_mae: 0.11737 | val_0_mse: 0.03014 |  0:02:01s
    epoch 1018| loss: 0.01045 | val_0_rmse: 0.17519 | val_0_mae: 0.11972 | val_0_mse: 0.03069 |  0:02:01s
    epoch 1019| loss: 0.01107 | val_0_rmse: 0.17261 | val_0_mae: 0.11726 | val_0_mse: 0.02979 |  0:02:02s
    epoch 1020| loss: 0.01036 | val_0_rmse: 0.17194 | val_0_mae: 0.11789 | val_0_mse: 0.02956 |  0:02:02s
    epoch 1021| loss: 0.01133 | val_0_rmse: 0.17292 | val_0_mae: 0.11902 | val_0_mse: 0.0299  |  0:02:02s
    epoch 1022| loss: 0.01167 | val_0_rmse: 0.17248 | val_0_mae: 0.11749 | val_0_mse: 0.02975 |  0:02:02s
    epoch 1023| loss: 0.0113  | val_0_rmse: 0.17209 | val_0_mae: 0.11727 | val_0_mse: 0.02961 |  0:02:02s
    epoch 1024| loss: 0.01037 | val_0_rmse: 0.1722  | val_0_mae: 0.11815 | val_0_mse: 0.02965 |  0:02:02s
    epoch 1025| loss: 0.0111  | val_0_rmse: 0.17199 | val_0_mae: 0.11628 | val_0_mse: 0.02958 |  0:02:02s
    epoch 1026| loss: 0.01074 | val_0_rmse: 0.17239 | val_0_mae: 0.11709 | val_0_mse: 0.02972 |  0:02:02s
    epoch 1027| loss: 0.01088 | val_0_rmse: 0.17268 | val_0_mae: 0.11739 | val_0_mse: 0.02982 |  0:02:02s
    epoch 1028| loss: 0.01066 | val_0_rmse: 0.17237 | val_0_mae: 0.11688 | val_0_mse: 0.02971 |  0:02:02s
    epoch 1029| loss: 0.01088 | val_0_rmse: 0.17217 | val_0_mae: 0.11762 | val_0_mse: 0.02964 |  0:02:03s
    epoch 1030| loss: 0.01107 | val_0_rmse: 0.17248 | val_0_mae: 0.11797 | val_0_mse: 0.02975 |  0:02:03s
    epoch 1031| loss: 0.01131 | val_0_rmse: 0.17265 | val_0_mae: 0.11777 | val_0_mse: 0.02981 |  0:02:03s
    epoch 1032| loss: 0.01055 | val_0_rmse: 0.1724  | val_0_mae: 0.11766 | val_0_mse: 0.02972 |  0:02:03s
    epoch 1033| loss: 0.01065 | val_0_rmse: 0.17205 | val_0_mae: 0.11729 | val_0_mse: 0.0296  |  0:02:03s
    epoch 1034| loss: 0.01064 | val_0_rmse: 0.17279 | val_0_mae: 0.11828 | val_0_mse: 0.02986 |  0:02:03s
    epoch 1035| loss: 0.0105  | val_0_rmse: 0.17238 | val_0_mae: 0.11686 | val_0_mse: 0.02971 |  0:02:03s
    epoch 1036| loss: 0.01053 | val_0_rmse: 0.17295 | val_0_mae: 0.11714 | val_0_mse: 0.02991 |  0:02:03s
    epoch 1037| loss: 0.01037 | val_0_rmse: 0.17364 | val_0_mae: 0.11811 | val_0_mse: 0.03015 |  0:02:03s
    epoch 1038| loss: 0.01049 | val_0_rmse: 0.17247 | val_0_mae: 0.11652 | val_0_mse: 0.02975 |  0:02:04s
    epoch 1039| loss: 0.01105 | val_0_rmse: 0.17171 | val_0_mae: 0.11632 | val_0_mse: 0.02948 |  0:02:04s
    epoch 1040| loss: 0.0103  | val_0_rmse: 0.17142 | val_0_mae: 0.11658 | val_0_mse: 0.02938 |  0:02:04s
    epoch 1041| loss: 0.00987 | val_0_rmse: 0.17226 | val_0_mae: 0.11744 | val_0_mse: 0.02968 |  0:02:04s
    epoch 1042| loss: 0.01101 | val_0_rmse: 0.17268 | val_0_mae: 0.11687 | val_0_mse: 0.02982 |  0:02:04s
    epoch 1043| loss: 0.01079 | val_0_rmse: 0.17244 | val_0_mae: 0.11655 | val_0_mse: 0.02974 |  0:02:04s
    epoch 1044| loss: 0.01092 | val_0_rmse: 0.17307 | val_0_mae: 0.11879 | val_0_mse: 0.02995 |  0:02:04s
    epoch 1045| loss: 0.01039 | val_0_rmse: 0.1722  | val_0_mae: 0.11708 | val_0_mse: 0.02965 |  0:02:04s
    epoch 1046| loss: 0.01    | val_0_rmse: 0.17327 | val_0_mae: 0.11659 | val_0_mse: 0.03002 |  0:02:04s
    epoch 1047| loss: 0.01077 | val_0_rmse: 0.17271 | val_0_mae: 0.11648 | val_0_mse: 0.02983 |  0:02:05s
    epoch 1048| loss: 0.01069 | val_0_rmse: 0.17362 | val_0_mae: 0.11836 | val_0_mse: 0.03014 |  0:02:05s
    epoch 1049| loss: 0.01091 | val_0_rmse: 0.17388 | val_0_mae: 0.11813 | val_0_mse: 0.03023 |  0:02:05s
    epoch 1050| loss: 0.01101 | val_0_rmse: 0.17458 | val_0_mae: 0.11793 | val_0_mse: 0.03048 |  0:02:05s
    epoch 1051| loss: 0.01047 | val_0_rmse: 0.17376 | val_0_mae: 0.11686 | val_0_mse: 0.03019 |  0:02:05s
    epoch 1052| loss: 0.01114 | val_0_rmse: 0.17393 | val_0_mae: 0.11758 | val_0_mse: 0.03025 |  0:02:05s
    epoch 1053| loss: 0.01044 | val_0_rmse: 0.1732  | val_0_mae: 0.11716 | val_0_mse: 0.03    |  0:02:05s
    epoch 1054| loss: 0.01057 | val_0_rmse: 0.17268 | val_0_mae: 0.11652 | val_0_mse: 0.02982 |  0:02:05s
    epoch 1055| loss: 0.01087 | val_0_rmse: 0.1724  | val_0_mae: 0.11663 | val_0_mse: 0.02972 |  0:02:05s
    epoch 1056| loss: 0.01067 | val_0_rmse: 0.17244 | val_0_mae: 0.11605 | val_0_mse: 0.02973 |  0:02:06s
    epoch 1057| loss: 0.00991 | val_0_rmse: 0.17405 | val_0_mae: 0.11706 | val_0_mse: 0.03029 |  0:02:06s
    epoch 1058| loss: 0.01086 | val_0_rmse: 0.17355 | val_0_mae: 0.11794 | val_0_mse: 0.03012 |  0:02:06s
    epoch 1059| loss: 0.01127 | val_0_rmse: 0.17303 | val_0_mae: 0.11753 | val_0_mse: 0.02994 |  0:02:06s
    epoch 1060| loss: 0.01038 | val_0_rmse: 0.17334 | val_0_mae: 0.11769 | val_0_mse: 0.03005 |  0:02:06s
    epoch 1061| loss: 0.01128 | val_0_rmse: 0.17288 | val_0_mae: 0.11813 | val_0_mse: 0.02989 |  0:02:06s
    epoch 1062| loss: 0.01125 | val_0_rmse: 0.17289 | val_0_mae: 0.11661 | val_0_mse: 0.02989 |  0:02:06s
    epoch 1063| loss: 0.01075 | val_0_rmse: 0.17321 | val_0_mae: 0.11599 | val_0_mse: 0.03    |  0:02:06s
    epoch 1064| loss: 0.00992 | val_0_rmse: 0.17309 | val_0_mae: 0.11744 | val_0_mse: 0.02996 |  0:02:06s
    epoch 1065| loss: 0.01081 | val_0_rmse: 0.17316 | val_0_mae: 0.11691 | val_0_mse: 0.02998 |  0:02:07s
    epoch 1066| loss: 0.0107  | val_0_rmse: 0.17316 | val_0_mae: 0.11685 | val_0_mse: 0.02998 |  0:02:07s
    epoch 1067| loss: 0.01081 | val_0_rmse: 0.17303 | val_0_mae: 0.11747 | val_0_mse: 0.02994 |  0:02:07s
    epoch 1068| loss: 0.01088 | val_0_rmse: 0.17365 | val_0_mae: 0.11719 | val_0_mse: 0.03016 |  0:02:07s
    epoch 1069| loss: 0.01036 | val_0_rmse: 0.17399 | val_0_mae: 0.11853 | val_0_mse: 0.03027 |  0:02:07s
    epoch 1070| loss: 0.01057 | val_0_rmse: 0.17395 | val_0_mae: 0.11903 | val_0_mse: 0.03026 |  0:02:07s
    epoch 1071| loss: 0.01084 | val_0_rmse: 0.17364 | val_0_mae: 0.1184  | val_0_mse: 0.03015 |  0:02:07s
    epoch 1072| loss: 0.01064 | val_0_rmse: 0.17242 | val_0_mae: 0.11693 | val_0_mse: 0.02973 |  0:02:07s
    epoch 1073| loss: 0.01055 | val_0_rmse: 0.17244 | val_0_mae: 0.11714 | val_0_mse: 0.02973 |  0:02:08s
    epoch 1074| loss: 0.0109  | val_0_rmse: 0.17248 | val_0_mae: 0.11611 | val_0_mse: 0.02975 |  0:02:08s
    epoch 1075| loss: 0.01022 | val_0_rmse: 0.17293 | val_0_mae: 0.11685 | val_0_mse: 0.0299  |  0:02:08s
    epoch 1076| loss: 0.01057 | val_0_rmse: 0.17357 | val_0_mae: 0.11821 | val_0_mse: 0.03013 |  0:02:08s
    epoch 1077| loss: 0.01057 | val_0_rmse: 0.17448 | val_0_mae: 0.11907 | val_0_mse: 0.03044 |  0:02:08s
    epoch 1078| loss: 0.01071 | val_0_rmse: 0.17575 | val_0_mae: 0.11975 | val_0_mse: 0.03089 |  0:02:08s
    epoch 1079| loss: 0.01108 | val_0_rmse: 0.17658 | val_0_mae: 0.1207  | val_0_mse: 0.03118 |  0:02:08s
    epoch 1080| loss: 0.01134 | val_0_rmse: 0.17481 | val_0_mae: 0.11871 | val_0_mse: 0.03056 |  0:02:08s
    epoch 1081| loss: 0.01007 | val_0_rmse: 0.17382 | val_0_mae: 0.11807 | val_0_mse: 0.03021 |  0:02:08s
    epoch 1082| loss: 0.01043 | val_0_rmse: 0.17294 | val_0_mae: 0.1184  | val_0_mse: 0.02991 |  0:02:08s
    epoch 1083| loss: 0.0106  | val_0_rmse: 0.1716  | val_0_mae: 0.1166  | val_0_mse: 0.02945 |  0:02:09s
    epoch 1084| loss: 0.01063 | val_0_rmse: 0.17133 | val_0_mae: 0.11621 | val_0_mse: 0.02935 |  0:02:09s
    epoch 1085| loss: 0.01077 | val_0_rmse: 0.17153 | val_0_mae: 0.11611 | val_0_mse: 0.02942 |  0:02:09s
    epoch 1086| loss: 0.00955 | val_0_rmse: 0.17236 | val_0_mae: 0.1172  | val_0_mse: 0.02971 |  0:02:09s
    epoch 1087| loss: 0.01049 | val_0_rmse: 0.17354 | val_0_mae: 0.11773 | val_0_mse: 0.03012 |  0:02:09s
    epoch 1088| loss: 0.01033 | val_0_rmse: 0.17374 | val_0_mae: 0.11743 | val_0_mse: 0.03018 |  0:02:09s
    epoch 1089| loss: 0.01058 | val_0_rmse: 0.17318 | val_0_mae: 0.11703 | val_0_mse: 0.02999 |  0:02:09s
    epoch 1090| loss: 0.00952 | val_0_rmse: 0.17335 | val_0_mae: 0.11737 | val_0_mse: 0.03005 |  0:02:09s
    epoch 1091| loss: 0.01024 | val_0_rmse: 0.17189 | val_0_mae: 0.1155  | val_0_mse: 0.02955 |  0:02:09s
    epoch 1092| loss: 0.01012 | val_0_rmse: 0.17185 | val_0_mae: 0.11705 | val_0_mse: 0.02953 |  0:02:10s
    epoch 1093| loss: 0.01134 | val_0_rmse: 0.1706  | val_0_mae: 0.11641 | val_0_mse: 0.0291  |  0:02:10s
    epoch 1094| loss: 0.01068 | val_0_rmse: 0.17075 | val_0_mae: 0.11623 | val_0_mse: 0.02915 |  0:02:10s
    epoch 1095| loss: 0.01066 | val_0_rmse: 0.17222 | val_0_mae: 0.11683 | val_0_mse: 0.02966 |  0:02:10s
    epoch 1096| loss: 0.01009 | val_0_rmse: 0.17499 | val_0_mae: 0.11958 | val_0_mse: 0.03062 |  0:02:10s
    epoch 1097| loss: 0.01004 | val_0_rmse: 0.17716 | val_0_mae: 0.1215  | val_0_mse: 0.03139 |  0:02:10s
    epoch 1098| loss: 0.01165 | val_0_rmse: 0.1737  | val_0_mae: 0.11689 | val_0_mse: 0.03017 |  0:02:10s
    epoch 1099| loss: 0.01063 | val_0_rmse: 0.17438 | val_0_mae: 0.11643 | val_0_mse: 0.03041 |  0:02:10s
    epoch 1100| loss: 0.00972 | val_0_rmse: 0.17451 | val_0_mae: 0.11783 | val_0_mse: 0.03045 |  0:02:10s
    epoch 1101| loss: 0.01014 | val_0_rmse: 0.17433 | val_0_mae: 0.1168  | val_0_mse: 0.03039 |  0:02:11s
    epoch 1102| loss: 0.01029 | val_0_rmse: 0.17553 | val_0_mae: 0.11778 | val_0_mse: 0.03081 |  0:02:11s
    epoch 1103| loss: 0.00991 | val_0_rmse: 0.17641 | val_0_mae: 0.11889 | val_0_mse: 0.03112 |  0:02:11s
    epoch 1104| loss: 0.01019 | val_0_rmse: 0.17517 | val_0_mae: 0.11769 | val_0_mse: 0.03068 |  0:02:11s
    epoch 1105| loss: 0.00998 | val_0_rmse: 0.17455 | val_0_mae: 0.11745 | val_0_mse: 0.03047 |  0:02:11s
    epoch 1106| loss: 0.01012 | val_0_rmse: 0.17285 | val_0_mae: 0.11656 | val_0_mse: 0.02988 |  0:02:11s
    epoch 1107| loss: 0.00997 | val_0_rmse: 0.1726  | val_0_mae: 0.11718 | val_0_mse: 0.02979 |  0:02:11s
    epoch 1108| loss: 0.01062 | val_0_rmse: 0.173   | val_0_mae: 0.11609 | val_0_mse: 0.02993 |  0:02:11s
    epoch 1109| loss: 0.01061 | val_0_rmse: 0.17425 | val_0_mae: 0.11717 | val_0_mse: 0.03036 |  0:02:11s
    epoch 1110| loss: 0.0113  | val_0_rmse: 0.17441 | val_0_mae: 0.1178  | val_0_mse: 0.03042 |  0:02:12s
    epoch 1111| loss: 0.01074 | val_0_rmse: 0.17365 | val_0_mae: 0.11603 | val_0_mse: 0.03015 |  0:02:12s
    epoch 1112| loss: 0.01172 | val_0_rmse: 0.17302 | val_0_mae: 0.11591 | val_0_mse: 0.02993 |  0:02:12s
    epoch 1113| loss: 0.01049 | val_0_rmse: 0.17367 | val_0_mae: 0.11821 | val_0_mse: 0.03016 |  0:02:12s
    epoch 1114| loss: 0.01004 | val_0_rmse: 0.17388 | val_0_mae: 0.11678 | val_0_mse: 0.03023 |  0:02:12s
    epoch 1115| loss: 0.01044 | val_0_rmse: 0.17362 | val_0_mae: 0.11867 | val_0_mse: 0.03014 |  0:02:12s
    epoch 1116| loss: 0.01064 | val_0_rmse: 0.17241 | val_0_mae: 0.11624 | val_0_mse: 0.02972 |  0:02:12s
    epoch 1117| loss: 0.01011 | val_0_rmse: 0.17313 | val_0_mae: 0.11591 | val_0_mse: 0.02997 |  0:02:12s
    epoch 1118| loss: 0.00974 | val_0_rmse: 0.17328 | val_0_mae: 0.11713 | val_0_mse: 0.03003 |  0:02:13s
    epoch 1119| loss: 0.01064 | val_0_rmse: 0.17453 | val_0_mae: 0.11668 | val_0_mse: 0.03046 |  0:02:13s
    epoch 1120| loss: 0.01083 | val_0_rmse: 0.17524 | val_0_mae: 0.11771 | val_0_mse: 0.03071 |  0:02:13s
    epoch 1121| loss: 0.00966 | val_0_rmse: 0.17529 | val_0_mae: 0.11805 | val_0_mse: 0.03073 |  0:02:13s
    epoch 1122| loss: 0.01008 | val_0_rmse: 0.1738  | val_0_mae: 0.11734 | val_0_mse: 0.03021 |  0:02:13s
    epoch 1123| loss: 0.01036 | val_0_rmse: 0.17202 | val_0_mae: 0.11627 | val_0_mse: 0.02959 |  0:02:13s
    epoch 1124| loss: 0.00996 | val_0_rmse: 0.17013 | val_0_mae: 0.11486 | val_0_mse: 0.02895 |  0:02:13s
    epoch 1125| loss: 0.01025 | val_0_rmse: 0.17083 | val_0_mae: 0.11552 | val_0_mse: 0.02918 |  0:02:13s
    epoch 1126| loss: 0.01014 | val_0_rmse: 0.17149 | val_0_mae: 0.11556 | val_0_mse: 0.02941 |  0:02:13s
    epoch 1127| loss: 0.00995 | val_0_rmse: 0.17114 | val_0_mae: 0.11479 | val_0_mse: 0.02929 |  0:02:13s
    epoch 1128| loss: 0.00948 | val_0_rmse: 0.17141 | val_0_mae: 0.11549 | val_0_mse: 0.02938 |  0:02:14s
    epoch 1129| loss: 0.00976 | val_0_rmse: 0.17132 | val_0_mae: 0.11483 | val_0_mse: 0.02935 |  0:02:14s
    epoch 1130| loss: 0.01004 | val_0_rmse: 0.17136 | val_0_mae: 0.11508 | val_0_mse: 0.02936 |  0:02:14s
    epoch 1131| loss: 0.00986 | val_0_rmse: 0.17204 | val_0_mae: 0.1162  | val_0_mse: 0.0296  |  0:02:14s
    epoch 1132| loss: 0.01046 | val_0_rmse: 0.17349 | val_0_mae: 0.11631 | val_0_mse: 0.0301  |  0:02:14s
    epoch 1133| loss: 0.00968 | val_0_rmse: 0.1747  | val_0_mae: 0.11776 | val_0_mse: 0.03052 |  0:02:14s
    epoch 1134| loss: 0.01014 | val_0_rmse: 0.17461 | val_0_mae: 0.11711 | val_0_mse: 0.03049 |  0:02:14s
    epoch 1135| loss: 0.01048 | val_0_rmse: 0.17349 | val_0_mae: 0.11697 | val_0_mse: 0.0301  |  0:02:14s
    epoch 1136| loss: 0.01045 | val_0_rmse: 0.17339 | val_0_mae: 0.11684 | val_0_mse: 0.03006 |  0:02:14s
    epoch 1137| loss: 0.00972 | val_0_rmse: 0.17456 | val_0_mae: 0.11722 | val_0_mse: 0.03047 |  0:02:15s
    epoch 1138| loss: 0.01062 | val_0_rmse: 0.17502 | val_0_mae: 0.11847 | val_0_mse: 0.03063 |  0:02:15s
    epoch 1139| loss: 0.01008 | val_0_rmse: 0.17414 | val_0_mae: 0.11678 | val_0_mse: 0.03032 |  0:02:15s
    epoch 1140| loss: 0.00996 | val_0_rmse: 0.1753  | val_0_mae: 0.11799 | val_0_mse: 0.03073 |  0:02:15s
    epoch 1141| loss: 0.00997 | val_0_rmse: 0.17622 | val_0_mae: 0.11939 | val_0_mse: 0.03106 |  0:02:15s
    epoch 1142| loss: 0.01032 | val_0_rmse: 0.17517 | val_0_mae: 0.11773 | val_0_mse: 0.03068 |  0:02:15s
    epoch 1143| loss: 0.01126 | val_0_rmse: 0.17376 | val_0_mae: 0.11644 | val_0_mse: 0.03019 |  0:02:15s
    epoch 1144| loss: 0.01007 | val_0_rmse: 0.17523 | val_0_mae: 0.11877 | val_0_mse: 0.0307  |  0:02:15s
    epoch 1145| loss: 0.01018 | val_0_rmse: 0.17453 | val_0_mae: 0.11643 | val_0_mse: 0.03046 |  0:02:16s
    epoch 1146| loss: 0.01038 | val_0_rmse: 0.17641 | val_0_mae: 0.11882 | val_0_mse: 0.03112 |  0:02:16s
    epoch 1147| loss: 0.0103  | val_0_rmse: 0.17476 | val_0_mae: 0.116   | val_0_mse: 0.03054 |  0:02:16s
    epoch 1148| loss: 0.00978 | val_0_rmse: 0.17468 | val_0_mae: 0.11597 | val_0_mse: 0.03051 |  0:02:16s
    epoch 1149| loss: 0.01003 | val_0_rmse: 0.17425 | val_0_mae: 0.11736 | val_0_mse: 0.03036 |  0:02:16s
    epoch 1150| loss: 0.01028 | val_0_rmse: 0.17293 | val_0_mae: 0.11544 | val_0_mse: 0.0299  |  0:02:16s
    epoch 1151| loss: 0.0101  | val_0_rmse: 0.172   | val_0_mae: 0.1159  | val_0_mse: 0.02958 |  0:02:16s
    epoch 1152| loss: 0.01014 | val_0_rmse: 0.17246 | val_0_mae: 0.11606 | val_0_mse: 0.02974 |  0:02:16s
    epoch 1153| loss: 0.00982 | val_0_rmse: 0.1732  | val_0_mae: 0.11584 | val_0_mse: 0.03    |  0:02:16s
    epoch 1154| loss: 0.00996 | val_0_rmse: 0.17293 | val_0_mae: 0.11829 | val_0_mse: 0.0299  |  0:02:17s
    epoch 1155| loss: 0.01035 | val_0_rmse: 0.17193 | val_0_mae: 0.11762 | val_0_mse: 0.02956 |  0:02:17s
    epoch 1156| loss: 0.01065 | val_0_rmse: 0.17251 | val_0_mae: 0.11675 | val_0_mse: 0.02976 |  0:02:17s
    epoch 1157| loss: 0.01001 | val_0_rmse: 0.17349 | val_0_mae: 0.11738 | val_0_mse: 0.0301  |  0:02:17s
    epoch 1158| loss: 0.00966 | val_0_rmse: 0.17398 | val_0_mae: 0.11754 | val_0_mse: 0.03027 |  0:02:17s
    epoch 1159| loss: 0.01022 | val_0_rmse: 0.17382 | val_0_mae: 0.11675 | val_0_mse: 0.03021 |  0:02:17s
    epoch 1160| loss: 0.00984 | val_0_rmse: 0.17315 | val_0_mae: 0.11586 | val_0_mse: 0.02998 |  0:02:17s
    epoch 1161| loss: 0.00975 | val_0_rmse: 0.17271 | val_0_mae: 0.11493 | val_0_mse: 0.02983 |  0:02:17s
    epoch 1162| loss: 0.01001 | val_0_rmse: 0.17313 | val_0_mae: 0.11721 | val_0_mse: 0.02997 |  0:02:17s
    epoch 1163| loss: 0.01019 | val_0_rmse: 0.17203 | val_0_mae: 0.11448 | val_0_mse: 0.0296  |  0:02:18s
    epoch 1164| loss: 0.01006 | val_0_rmse: 0.1716  | val_0_mae: 0.11474 | val_0_mse: 0.02945 |  0:02:18s
    epoch 1165| loss: 0.01018 | val_0_rmse: 0.17146 | val_0_mae: 0.11522 | val_0_mse: 0.0294  |  0:02:18s
    epoch 1166| loss: 0.01103 | val_0_rmse: 0.17104 | val_0_mae: 0.11438 | val_0_mse: 0.02925 |  0:02:18s
    epoch 1167| loss: 0.0103  | val_0_rmse: 0.17225 | val_0_mae: 0.11632 | val_0_mse: 0.02967 |  0:02:18s
    epoch 1168| loss: 0.0103  | val_0_rmse: 0.17387 | val_0_mae: 0.11686 | val_0_mse: 0.03023 |  0:02:18s
    epoch 1169| loss: 0.01067 | val_0_rmse: 0.17326 | val_0_mae: 0.11599 | val_0_mse: 0.03002 |  0:02:18s
    epoch 1170| loss: 0.01086 | val_0_rmse: 0.17225 | val_0_mae: 0.11629 | val_0_mse: 0.02967 |  0:02:18s
    epoch 1171| loss: 0.01068 | val_0_rmse: 0.17312 | val_0_mae: 0.11549 | val_0_mse: 0.02997 |  0:02:18s
    epoch 1172| loss: 0.01043 | val_0_rmse: 0.17052 | val_0_mae: 0.1147  | val_0_mse: 0.02908 |  0:02:19s
    epoch 1173| loss: 0.00985 | val_0_rmse: 0.17025 | val_0_mae: 0.11459 | val_0_mse: 0.02898 |  0:02:19s
    epoch 1174| loss: 0.01004 | val_0_rmse: 0.17044 | val_0_mae: 0.11481 | val_0_mse: 0.02905 |  0:02:19s
    epoch 1175| loss: 0.01001 | val_0_rmse: 0.1709  | val_0_mae: 0.11532 | val_0_mse: 0.02921 |  0:02:19s
    epoch 1176| loss: 0.00983 | val_0_rmse: 0.17029 | val_0_mae: 0.11472 | val_0_mse: 0.029   |  0:02:19s
    epoch 1177| loss: 0.00972 | val_0_rmse: 0.17071 | val_0_mae: 0.11548 | val_0_mse: 0.02914 |  0:02:19s
    epoch 1178| loss: 0.01029 | val_0_rmse: 0.1716  | val_0_mae: 0.11604 | val_0_mse: 0.02945 |  0:02:19s
    epoch 1179| loss: 0.00996 | val_0_rmse: 0.17168 | val_0_mae: 0.11556 | val_0_mse: 0.02947 |  0:02:19s
    epoch 1180| loss: 0.01008 | val_0_rmse: 0.17188 | val_0_mae: 0.11626 | val_0_mse: 0.02954 |  0:02:19s
    epoch 1181| loss: 0.00974 | val_0_rmse: 0.1723  | val_0_mae: 0.11655 | val_0_mse: 0.02969 |  0:02:20s
    epoch 1182| loss: 0.0101  | val_0_rmse: 0.17157 | val_0_mae: 0.11759 | val_0_mse: 0.02944 |  0:02:20s
    epoch 1183| loss: 0.00967 | val_0_rmse: 0.17154 | val_0_mae: 0.11597 | val_0_mse: 0.02942 |  0:02:20s
    epoch 1184| loss: 0.00968 | val_0_rmse: 0.17117 | val_0_mae: 0.11623 | val_0_mse: 0.0293  |  0:02:20s
    epoch 1185| loss: 0.01041 | val_0_rmse: 0.17192 | val_0_mae: 0.11703 | val_0_mse: 0.02956 |  0:02:20s
    epoch 1186| loss: 0.00988 | val_0_rmse: 0.17188 | val_0_mae: 0.11554 | val_0_mse: 0.02954 |  0:02:20s
    epoch 1187| loss: 0.0099  | val_0_rmse: 0.17246 | val_0_mae: 0.11697 | val_0_mse: 0.02974 |  0:02:20s
    epoch 1188| loss: 0.01002 | val_0_rmse: 0.17305 | val_0_mae: 0.11656 | val_0_mse: 0.02995 |  0:02:20s
    epoch 1189| loss: 0.00993 | val_0_rmse: 0.17167 | val_0_mae: 0.11545 | val_0_mse: 0.02947 |  0:02:21s
    epoch 1190| loss: 0.01056 | val_0_rmse: 0.17095 | val_0_mae: 0.11724 | val_0_mse: 0.02922 |  0:02:21s
    epoch 1191| loss: 0.01008 | val_0_rmse: 0.17036 | val_0_mae: 0.11522 | val_0_mse: 0.02902 |  0:02:21s
    epoch 1192| loss: 0.01077 | val_0_rmse: 0.16957 | val_0_mae: 0.11458 | val_0_mse: 0.02875 |  0:02:21s
    epoch 1193| loss: 0.00994 | val_0_rmse: 0.16974 | val_0_mae: 0.1141  | val_0_mse: 0.02881 |  0:02:21s
    epoch 1194| loss: 0.01069 | val_0_rmse: 0.17017 | val_0_mae: 0.11426 | val_0_mse: 0.02896 |  0:02:21s
    epoch 1195| loss: 0.00933 | val_0_rmse: 0.17143 | val_0_mae: 0.11653 | val_0_mse: 0.02939 |  0:02:21s
    epoch 1196| loss: 0.0101  | val_0_rmse: 0.16943 | val_0_mae: 0.11376 | val_0_mse: 0.02871 |  0:02:21s
    epoch 1197| loss: 0.00998 | val_0_rmse: 0.16849 | val_0_mae: 0.11352 | val_0_mse: 0.02839 |  0:02:21s
    epoch 1198| loss: 0.00943 | val_0_rmse: 0.16818 | val_0_mae: 0.11349 | val_0_mse: 0.02829 |  0:02:22s
    epoch 1199| loss: 0.00956 | val_0_rmse: 0.1686  | val_0_mae: 0.11361 | val_0_mse: 0.02843 |  0:02:22s
    epoch 1200| loss: 0.00938 | val_0_rmse: 0.16969 | val_0_mae: 0.11395 | val_0_mse: 0.02879 |  0:02:22s
    epoch 1201| loss: 0.01039 | val_0_rmse: 0.17086 | val_0_mae: 0.11457 | val_0_mse: 0.02919 |  0:02:22s
    epoch 1202| loss: 0.00985 | val_0_rmse: 0.17081 | val_0_mae: 0.1145  | val_0_mse: 0.02918 |  0:02:22s
    epoch 1203| loss: 0.00967 | val_0_rmse: 0.17121 | val_0_mae: 0.11522 | val_0_mse: 0.02931 |  0:02:22s
    epoch 1204| loss: 0.00978 | val_0_rmse: 0.1705  | val_0_mae: 0.114   | val_0_mse: 0.02907 |  0:02:22s
    epoch 1205| loss: 0.00997 | val_0_rmse: 0.17047 | val_0_mae: 0.1133  | val_0_mse: 0.02906 |  0:02:22s
    epoch 1206| loss: 0.01096 | val_0_rmse: 0.17138 | val_0_mae: 0.11541 | val_0_mse: 0.02937 |  0:02:23s
    epoch 1207| loss: 0.01029 | val_0_rmse: 0.17377 | val_0_mae: 0.1165  | val_0_mse: 0.0302  |  0:02:23s
    epoch 1208| loss: 0.01081 | val_0_rmse: 0.17504 | val_0_mae: 0.11839 | val_0_mse: 0.03064 |  0:02:23s
    epoch 1209| loss: 0.01039 | val_0_rmse: 0.17383 | val_0_mae: 0.11599 | val_0_mse: 0.03022 |  0:02:23s
    epoch 1210| loss: 0.00995 | val_0_rmse: 0.17309 | val_0_mae: 0.11792 | val_0_mse: 0.02996 |  0:02:23s
    epoch 1211| loss: 0.01012 | val_0_rmse: 0.17215 | val_0_mae: 0.11574 | val_0_mse: 0.02963 |  0:02:23s
    epoch 1212| loss: 0.01002 | val_0_rmse: 0.17263 | val_0_mae: 0.11623 | val_0_mse: 0.0298  |  0:02:23s
    epoch 1213| loss: 0.01025 | val_0_rmse: 0.17248 | val_0_mae: 0.11739 | val_0_mse: 0.02975 |  0:02:23s
    epoch 1214| loss: 0.00977 | val_0_rmse: 0.17318 | val_0_mae: 0.11595 | val_0_mse: 0.02999 |  0:02:23s
    epoch 1215| loss: 0.00997 | val_0_rmse: 0.17376 | val_0_mae: 0.11636 | val_0_mse: 0.03019 |  0:02:23s
    epoch 1216| loss: 0.00935 | val_0_rmse: 0.17426 | val_0_mae: 0.1177  | val_0_mse: 0.03037 |  0:02:24s
    epoch 1217| loss: 0.01032 | val_0_rmse: 0.1746  | val_0_mae: 0.11845 | val_0_mse: 0.03048 |  0:02:24s
    epoch 1218| loss: 0.00958 | val_0_rmse: 0.17337 | val_0_mae: 0.11634 | val_0_mse: 0.03006 |  0:02:24s
    epoch 1219| loss: 0.0094  | val_0_rmse: 0.17323 | val_0_mae: 0.11585 | val_0_mse: 0.03001 |  0:02:24s
    epoch 1220| loss: 0.00976 | val_0_rmse: 0.17374 | val_0_mae: 0.1166  | val_0_mse: 0.03019 |  0:02:24s
    epoch 1221| loss: 0.00979 | val_0_rmse: 0.17377 | val_0_mae: 0.1168  | val_0_mse: 0.0302  |  0:02:24s
    epoch 1222| loss: 0.00927 | val_0_rmse: 0.17321 | val_0_mae: 0.11589 | val_0_mse: 0.03    |  0:02:24s
    epoch 1223| loss: 0.00922 | val_0_rmse: 0.17508 | val_0_mae: 0.11893 | val_0_mse: 0.03065 |  0:02:24s
    epoch 1224| loss: 0.01031 | val_0_rmse: 0.17514 | val_0_mae: 0.11894 | val_0_mse: 0.03068 |  0:02:24s
    epoch 1225| loss: 0.01001 | val_0_rmse: 0.17442 | val_0_mae: 0.11808 | val_0_mse: 0.03042 |  0:02:25s
    epoch 1226| loss: 0.00983 | val_0_rmse: 0.17565 | val_0_mae: 0.11792 | val_0_mse: 0.03085 |  0:02:25s
    epoch 1227| loss: 0.01011 | val_0_rmse: 0.17513 | val_0_mae: 0.11801 | val_0_mse: 0.03067 |  0:02:25s
    epoch 1228| loss: 0.01047 | val_0_rmse: 0.1731  | val_0_mae: 0.11681 | val_0_mse: 0.02997 |  0:02:25s
    epoch 1229| loss: 0.00951 | val_0_rmse: 0.17348 | val_0_mae: 0.11687 | val_0_mse: 0.0301  |  0:02:25s
    epoch 1230| loss: 0.00889 | val_0_rmse: 0.17336 | val_0_mae: 0.11666 | val_0_mse: 0.03005 |  0:02:25s
    epoch 1231| loss: 0.00947 | val_0_rmse: 0.17274 | val_0_mae: 0.11606 | val_0_mse: 0.02984 |  0:02:25s
    epoch 1232| loss: 0.01019 | val_0_rmse: 0.1742  | val_0_mae: 0.11806 | val_0_mse: 0.03034 |  0:02:25s
    epoch 1233| loss: 0.00928 | val_0_rmse: 0.17418 | val_0_mae: 0.11679 | val_0_mse: 0.03034 |  0:02:26s
    epoch 1234| loss: 0.00977 | val_0_rmse: 0.17509 | val_0_mae: 0.11745 | val_0_mse: 0.03066 |  0:02:26s
    epoch 1235| loss: 0.00963 | val_0_rmse: 0.17514 | val_0_mae: 0.11697 | val_0_mse: 0.03067 |  0:02:26s
    epoch 1236| loss: 0.00917 | val_0_rmse: 0.17446 | val_0_mae: 0.11677 | val_0_mse: 0.03044 |  0:02:26s
    epoch 1237| loss: 0.00929 | val_0_rmse: 0.17361 | val_0_mae: 0.11596 | val_0_mse: 0.03014 |  0:02:26s
    epoch 1238| loss: 0.00902 | val_0_rmse: 0.17256 | val_0_mae: 0.11546 | val_0_mse: 0.02978 |  0:02:26s
    epoch 1239| loss: 0.00921 | val_0_rmse: 0.17176 | val_0_mae: 0.11503 | val_0_mse: 0.0295  |  0:02:26s
    epoch 1240| loss: 0.00951 | val_0_rmse: 0.17094 | val_0_mae: 0.11528 | val_0_mse: 0.02922 |  0:02:26s
    epoch 1241| loss: 0.01003 | val_0_rmse: 0.17122 | val_0_mae: 0.11606 | val_0_mse: 0.02932 |  0:02:26s
    epoch 1242| loss: 0.0095  | val_0_rmse: 0.17073 | val_0_mae: 0.11459 | val_0_mse: 0.02915 |  0:02:27s
    epoch 1243| loss: 0.00874 | val_0_rmse: 0.1704  | val_0_mae: 0.11523 | val_0_mse: 0.02904 |  0:02:27s
    epoch 1244| loss: 0.00899 | val_0_rmse: 0.17066 | val_0_mae: 0.11439 | val_0_mse: 0.02913 |  0:02:27s
    epoch 1245| loss: 0.00907 | val_0_rmse: 0.17166 | val_0_mae: 0.11497 | val_0_mse: 0.02947 |  0:02:27s
    epoch 1246| loss: 0.00926 | val_0_rmse: 0.17246 | val_0_mae: 0.11613 | val_0_mse: 0.02974 |  0:02:27s
    epoch 1247| loss: 0.01005 | val_0_rmse: 0.17135 | val_0_mae: 0.11463 | val_0_mse: 0.02936 |  0:02:27s
    epoch 1248| loss: 0.00945 | val_0_rmse: 0.17218 | val_0_mae: 0.11641 | val_0_mse: 0.02964 |  0:02:27s
    epoch 1249| loss: 0.00987 | val_0_rmse: 0.17315 | val_0_mae: 0.11676 | val_0_mse: 0.02998 |  0:02:27s
    epoch 1250| loss: 0.0096  | val_0_rmse: 0.17337 | val_0_mae: 0.11575 | val_0_mse: 0.03006 |  0:02:27s
    epoch 1251| loss: 0.0098  | val_0_rmse: 0.17267 | val_0_mae: 0.11501 | val_0_mse: 0.02982 |  0:02:28s
    epoch 1252| loss: 0.00939 | val_0_rmse: 0.17335 | val_0_mae: 0.11673 | val_0_mse: 0.03005 |  0:02:28s
    epoch 1253| loss: 0.00914 | val_0_rmse: 0.17342 | val_0_mae: 0.11584 | val_0_mse: 0.03007 |  0:02:28s
    epoch 1254| loss: 0.00939 | val_0_rmse: 0.17354 | val_0_mae: 0.1164  | val_0_mse: 0.03012 |  0:02:28s
    epoch 1255| loss: 0.00947 | val_0_rmse: 0.17305 | val_0_mae: 0.11682 | val_0_mse: 0.02995 |  0:02:28s
    epoch 1256| loss: 0.00911 | val_0_rmse: 0.17288 | val_0_mae: 0.11623 | val_0_mse: 0.02989 |  0:02:28s
    epoch 1257| loss: 0.00959 | val_0_rmse: 0.17242 | val_0_mae: 0.11506 | val_0_mse: 0.02973 |  0:02:28s
    epoch 1258| loss: 0.00947 | val_0_rmse: 0.17185 | val_0_mae: 0.1149  | val_0_mse: 0.02953 |  0:02:29s
    epoch 1259| loss: 0.00874 | val_0_rmse: 0.17163 | val_0_mae: 0.11578 | val_0_mse: 0.02946 |  0:02:29s
    epoch 1260| loss: 0.00881 | val_0_rmse: 0.17183 | val_0_mae: 0.11543 | val_0_mse: 0.02952 |  0:02:29s
    epoch 1261| loss: 0.00905 | val_0_rmse: 0.17153 | val_0_mae: 0.11437 | val_0_mse: 0.02942 |  0:02:29s
    epoch 1262| loss: 0.00888 | val_0_rmse: 0.17197 | val_0_mae: 0.11501 | val_0_mse: 0.02957 |  0:02:29s
    epoch 1263| loss: 0.00893 | val_0_rmse: 0.17297 | val_0_mae: 0.11536 | val_0_mse: 0.02992 |  0:02:29s
    epoch 1264| loss: 0.00946 | val_0_rmse: 0.17366 | val_0_mae: 0.11566 | val_0_mse: 0.03016 |  0:02:29s
    epoch 1265| loss: 0.00931 | val_0_rmse: 0.17366 | val_0_mae: 0.11713 | val_0_mse: 0.03016 |  0:02:30s
    epoch 1266| loss: 0.00942 | val_0_rmse: 0.17179 | val_0_mae: 0.11478 | val_0_mse: 0.02951 |  0:02:30s
    epoch 1267| loss: 0.00896 | val_0_rmse: 0.17207 | val_0_mae: 0.11597 | val_0_mse: 0.02961 |  0:02:30s
    epoch 1268| loss: 0.00892 | val_0_rmse: 0.17193 | val_0_mae: 0.1163  | val_0_mse: 0.02956 |  0:02:30s
    epoch 1269| loss: 0.00942 | val_0_rmse: 0.17262 | val_0_mae: 0.1158  | val_0_mse: 0.0298  |  0:02:30s
    epoch 1270| loss: 0.00951 | val_0_rmse: 0.17367 | val_0_mae: 0.11662 | val_0_mse: 0.03016 |  0:02:30s
    epoch 1271| loss: 0.00897 | val_0_rmse: 0.17388 | val_0_mae: 0.11593 | val_0_mse: 0.03023 |  0:02:30s
    epoch 1272| loss: 0.00951 | val_0_rmse: 0.17282 | val_0_mae: 0.11491 | val_0_mse: 0.02987 |  0:02:30s
    epoch 1273| loss: 0.00913 | val_0_rmse: 0.17287 | val_0_mae: 0.11575 | val_0_mse: 0.02988 |  0:02:30s
    epoch 1274| loss: 0.00969 | val_0_rmse: 0.17285 | val_0_mae: 0.11519 | val_0_mse: 0.02988 |  0:02:31s
    epoch 1275| loss: 0.00897 | val_0_rmse: 0.17332 | val_0_mae: 0.11641 | val_0_mse: 0.03004 |  0:02:31s
    epoch 1276| loss: 0.0093  | val_0_rmse: 0.17434 | val_0_mae: 0.11685 | val_0_mse: 0.03039 |  0:02:31s
    epoch 1277| loss: 0.00896 | val_0_rmse: 0.17606 | val_0_mae: 0.11857 | val_0_mse: 0.031   |  0:02:31s
    epoch 1278| loss: 0.00892 | val_0_rmse: 0.17477 | val_0_mae: 0.11684 | val_0_mse: 0.03054 |  0:02:31s
    epoch 1279| loss: 0.00917 | val_0_rmse: 0.17454 | val_0_mae: 0.11685 | val_0_mse: 0.03046 |  0:02:31s
    epoch 1280| loss: 0.00916 | val_0_rmse: 0.17546 | val_0_mae: 0.11815 | val_0_mse: 0.03078 |  0:02:31s
    epoch 1281| loss: 0.0092  | val_0_rmse: 0.17477 | val_0_mae: 0.11689 | val_0_mse: 0.03054 |  0:02:31s
    epoch 1282| loss: 0.00948 | val_0_rmse: 0.17523 | val_0_mae: 0.11852 | val_0_mse: 0.03071 |  0:02:32s
    epoch 1283| loss: 0.00852 | val_0_rmse: 0.17384 | val_0_mae: 0.11628 | val_0_mse: 0.03022 |  0:02:32s
    epoch 1284| loss: 0.00852 | val_0_rmse: 0.17372 | val_0_mae: 0.11574 | val_0_mse: 0.03018 |  0:02:32s
    epoch 1285| loss: 0.00939 | val_0_rmse: 0.17375 | val_0_mae: 0.11591 | val_0_mse: 0.03019 |  0:02:32s
    epoch 1286| loss: 0.00934 | val_0_rmse: 0.17328 | val_0_mae: 0.11581 | val_0_mse: 0.03003 |  0:02:32s
    epoch 1287| loss: 0.00911 | val_0_rmse: 0.17332 | val_0_mae: 0.11554 | val_0_mse: 0.03004 |  0:02:32s
    epoch 1288| loss: 0.0089  | val_0_rmse: 0.17356 | val_0_mae: 0.11729 | val_0_mse: 0.03012 |  0:02:32s
    epoch 1289| loss: 0.0096  | val_0_rmse: 0.1735  | val_0_mae: 0.11667 | val_0_mse: 0.0301  |  0:02:32s
    epoch 1290| loss: 0.00996 | val_0_rmse: 0.17391 | val_0_mae: 0.11794 | val_0_mse: 0.03024 |  0:02:32s
    epoch 1291| loss: 0.00913 | val_0_rmse: 0.17168 | val_0_mae: 0.11481 | val_0_mse: 0.02947 |  0:02:33s
    epoch 1292| loss: 0.00964 | val_0_rmse: 0.17244 | val_0_mae: 0.11636 | val_0_mse: 0.02974 |  0:02:33s
    epoch 1293| loss: 0.00962 | val_0_rmse: 0.17301 | val_0_mae: 0.11765 | val_0_mse: 0.02993 |  0:02:33s
    epoch 1294| loss: 0.00966 | val_0_rmse: 0.17259 | val_0_mae: 0.11542 | val_0_mse: 0.02979 |  0:02:33s
    epoch 1295| loss: 0.00906 | val_0_rmse: 0.17328 | val_0_mae: 0.11572 | val_0_mse: 0.03003 |  0:02:33s
    epoch 1296| loss: 0.00932 | val_0_rmse: 0.17343 | val_0_mae: 0.11669 | val_0_mse: 0.03008 |  0:02:34s
    epoch 1297| loss: 0.00938 | val_0_rmse: 0.17261 | val_0_mae: 0.11502 | val_0_mse: 0.02979 |  0:02:34s
    epoch 1298| loss: 0.00923 | val_0_rmse: 0.17341 | val_0_mae: 0.11598 | val_0_mse: 0.03007 |  0:02:34s
    epoch 1299| loss: 0.00887 | val_0_rmse: 0.17295 | val_0_mae: 0.11486 | val_0_mse: 0.02991 |  0:02:34s
    epoch 1300| loss: 0.00899 | val_0_rmse: 0.17205 | val_0_mae: 0.11391 | val_0_mse: 0.0296  |  0:02:34s
    epoch 1301| loss: 0.00854 | val_0_rmse: 0.17257 | val_0_mae: 0.11556 | val_0_mse: 0.02978 |  0:02:34s
    epoch 1302| loss: 0.00875 | val_0_rmse: 0.17248 | val_0_mae: 0.11554 | val_0_mse: 0.02975 |  0:02:34s
    epoch 1303| loss: 0.00858 | val_0_rmse: 0.17251 | val_0_mae: 0.1159  | val_0_mse: 0.02976 |  0:02:34s
    epoch 1304| loss: 0.00924 | val_0_rmse: 0.17175 | val_0_mae: 0.11427 | val_0_mse: 0.0295  |  0:02:34s
    epoch 1305| loss: 0.00867 | val_0_rmse: 0.17188 | val_0_mae: 0.11503 | val_0_mse: 0.02954 |  0:02:34s
    epoch 1306| loss: 0.00914 | val_0_rmse: 0.17157 | val_0_mae: 0.11489 | val_0_mse: 0.02943 |  0:02:35s
    epoch 1307| loss: 0.00913 | val_0_rmse: 0.17215 | val_0_mae: 0.11562 | val_0_mse: 0.02964 |  0:02:35s
    epoch 1308| loss: 0.00897 | val_0_rmse: 0.17297 | val_0_mae: 0.11512 | val_0_mse: 0.02992 |  0:02:35s
    epoch 1309| loss: 0.00905 | val_0_rmse: 0.17418 | val_0_mae: 0.11601 | val_0_mse: 0.03034 |  0:02:35s
    epoch 1310| loss: 0.00921 | val_0_rmse: 0.17422 | val_0_mae: 0.1154  | val_0_mse: 0.03035 |  0:02:35s
    epoch 1311| loss: 0.00939 | val_0_rmse: 0.17382 | val_0_mae: 0.11578 | val_0_mse: 0.03022 |  0:02:35s
    epoch 1312| loss: 0.00896 | val_0_rmse: 0.1728  | val_0_mae: 0.11662 | val_0_mse: 0.02986 |  0:02:35s
    epoch 1313| loss: 0.00921 | val_0_rmse: 0.17093 | val_0_mae: 0.11433 | val_0_mse: 0.02922 |  0:02:35s
    epoch 1314| loss: 0.00904 | val_0_rmse: 0.17023 | val_0_mae: 0.11502 | val_0_mse: 0.02898 |  0:02:35s
    epoch 1315| loss: 0.00917 | val_0_rmse: 0.16935 | val_0_mae: 0.11335 | val_0_mse: 0.02868 |  0:02:36s
    epoch 1316| loss: 0.00929 | val_0_rmse: 0.16989 | val_0_mae: 0.11286 | val_0_mse: 0.02886 |  0:02:36s
    epoch 1317| loss: 0.00896 | val_0_rmse: 0.17188 | val_0_mae: 0.11551 | val_0_mse: 0.02954 |  0:02:36s
    epoch 1318| loss: 0.00943 | val_0_rmse: 0.17293 | val_0_mae: 0.11634 | val_0_mse: 0.02991 |  0:02:36s
    epoch 1319| loss: 0.00956 | val_0_rmse: 0.17209 | val_0_mae: 0.11476 | val_0_mse: 0.02961 |  0:02:36s
    epoch 1320| loss: 0.0104  | val_0_rmse: 0.1721  | val_0_mae: 0.11478 | val_0_mse: 0.02962 |  0:02:36s
    epoch 1321| loss: 0.01001 | val_0_rmse: 0.17321 | val_0_mae: 0.11559 | val_0_mse: 0.03    |  0:02:36s
    epoch 1322| loss: 0.00988 | val_0_rmse: 0.17167 | val_0_mae: 0.11439 | val_0_mse: 0.02947 |  0:02:36s
    epoch 1323| loss: 0.00964 | val_0_rmse: 0.1711  | val_0_mae: 0.1134  | val_0_mse: 0.02928 |  0:02:36s
    epoch 1324| loss: 0.0097  | val_0_rmse: 0.17059 | val_0_mae: 0.11243 | val_0_mse: 0.0291  |  0:02:37s
    epoch 1325| loss: 0.00869 | val_0_rmse: 0.17124 | val_0_mae: 0.11347 | val_0_mse: 0.02932 |  0:02:37s
    epoch 1326| loss: 0.00946 | val_0_rmse: 0.17117 | val_0_mae: 0.11263 | val_0_mse: 0.0293  |  0:02:37s
    epoch 1327| loss: 0.00901 | val_0_rmse: 0.17153 | val_0_mae: 0.11419 | val_0_mse: 0.02942 |  0:02:37s
    epoch 1328| loss: 0.0094  | val_0_rmse: 0.17304 | val_0_mae: 0.11534 | val_0_mse: 0.02994 |  0:02:37s
    epoch 1329| loss: 0.00894 | val_0_rmse: 0.17387 | val_0_mae: 0.11539 | val_0_mse: 0.03023 |  0:02:37s
    epoch 1330| loss: 0.00903 | val_0_rmse: 0.17455 | val_0_mae: 0.11664 | val_0_mse: 0.03047 |  0:02:37s
    epoch 1331| loss: 0.00946 | val_0_rmse: 0.1727  | val_0_mae: 0.11514 | val_0_mse: 0.02982 |  0:02:37s
    epoch 1332| loss: 0.00934 | val_0_rmse: 0.17222 | val_0_mae: 0.11572 | val_0_mse: 0.02966 |  0:02:37s
    epoch 1333| loss: 0.00934 | val_0_rmse: 0.17007 | val_0_mae: 0.11421 | val_0_mse: 0.02892 |  0:02:37s
    epoch 1334| loss: 0.00959 | val_0_rmse: 0.17008 | val_0_mae: 0.11345 | val_0_mse: 0.02893 |  0:02:38s
    epoch 1335| loss: 0.01002 | val_0_rmse: 0.17298 | val_0_mae: 0.11655 | val_0_mse: 0.02992 |  0:02:38s
    epoch 1336| loss: 0.0095  | val_0_rmse: 0.17146 | val_0_mae: 0.11521 | val_0_mse: 0.0294  |  0:02:38s
    epoch 1337| loss: 0.00994 | val_0_rmse: 0.17179 | val_0_mae: 0.11702 | val_0_mse: 0.02951 |  0:02:38s
    epoch 1338| loss: 0.0088  | val_0_rmse: 0.17111 | val_0_mae: 0.11504 | val_0_mse: 0.02928 |  0:02:38s
    epoch 1339| loss: 0.00852 | val_0_rmse: 0.17    | val_0_mae: 0.11312 | val_0_mse: 0.0289  |  0:02:38s
    epoch 1340| loss: 0.00872 | val_0_rmse: 0.17113 | val_0_mae: 0.11591 | val_0_mse: 0.02928 |  0:02:38s
    epoch 1341| loss: 0.0088  | val_0_rmse: 0.17075 | val_0_mae: 0.11321 | val_0_mse: 0.02916 |  0:02:38s
    epoch 1342| loss: 0.00905 | val_0_rmse: 0.1705  | val_0_mae: 0.11385 | val_0_mse: 0.02907 |  0:02:38s
    epoch 1343| loss: 0.00906 | val_0_rmse: 0.1704  | val_0_mae: 0.11515 | val_0_mse: 0.02904 |  0:02:39s
    epoch 1344| loss: 0.00806 | val_0_rmse: 0.16973 | val_0_mae: 0.11356 | val_0_mse: 0.02881 |  0:02:39s
    epoch 1345| loss: 0.00838 | val_0_rmse: 0.16955 | val_0_mae: 0.11309 | val_0_mse: 0.02875 |  0:02:39s
    epoch 1346| loss: 0.0089  | val_0_rmse: 0.17098 | val_0_mae: 0.11417 | val_0_mse: 0.02924 |  0:02:39s
    epoch 1347| loss: 0.00837 | val_0_rmse: 0.17334 | val_0_mae: 0.11645 | val_0_mse: 0.03005 |  0:02:39s
    epoch 1348| loss: 0.00891 | val_0_rmse: 0.17282 | val_0_mae: 0.11501 | val_0_mse: 0.02987 |  0:02:39s
    epoch 1349| loss: 0.00958 | val_0_rmse: 0.17191 | val_0_mae: 0.11414 | val_0_mse: 0.02955 |  0:02:39s
    epoch 1350| loss: 0.00912 | val_0_rmse: 0.17145 | val_0_mae: 0.11344 | val_0_mse: 0.0294  |  0:02:39s
    epoch 1351| loss: 0.00921 | val_0_rmse: 0.17265 | val_0_mae: 0.11556 | val_0_mse: 0.02981 |  0:02:39s
    epoch 1352| loss: 0.00844 | val_0_rmse: 0.17096 | val_0_mae: 0.11288 | val_0_mse: 0.02923 |  0:02:40s
    epoch 1353| loss: 0.00916 | val_0_rmse: 0.17162 | val_0_mae: 0.11404 | val_0_mse: 0.02945 |  0:02:40s
    epoch 1354| loss: 0.00933 | val_0_rmse: 0.17171 | val_0_mae: 0.11341 | val_0_mse: 0.02948 |  0:02:40s
    epoch 1355| loss: 0.00871 | val_0_rmse: 0.17257 | val_0_mae: 0.11426 | val_0_mse: 0.02978 |  0:02:40s
    epoch 1356| loss: 0.00905 | val_0_rmse: 0.17181 | val_0_mae: 0.1133  | val_0_mse: 0.02952 |  0:02:40s
    epoch 1357| loss: 0.00894 | val_0_rmse: 0.17235 | val_0_mae: 0.11405 | val_0_mse: 0.02971 |  0:02:40s
    epoch 1358| loss: 0.00871 | val_0_rmse: 0.17326 | val_0_mae: 0.11502 | val_0_mse: 0.03002 |  0:02:40s
    epoch 1359| loss: 0.00863 | val_0_rmse: 0.17365 | val_0_mae: 0.11537 | val_0_mse: 0.03016 |  0:02:40s
    epoch 1360| loss: 0.00966 | val_0_rmse: 0.17296 | val_0_mae: 0.1152  | val_0_mse: 0.02992 |  0:02:40s
    epoch 1361| loss: 0.00898 | val_0_rmse: 0.17256 | val_0_mae: 0.11537 | val_0_mse: 0.02978 |  0:02:41s
    epoch 1362| loss: 0.00926 | val_0_rmse: 0.17177 | val_0_mae: 0.1139  | val_0_mse: 0.0295  |  0:02:41s
    epoch 1363| loss: 0.00861 | val_0_rmse: 0.17065 | val_0_mae: 0.11224 | val_0_mse: 0.02912 |  0:02:41s
    epoch 1364| loss: 0.00896 | val_0_rmse: 0.17092 | val_0_mae: 0.11347 | val_0_mse: 0.02922 |  0:02:41s
    epoch 1365| loss: 0.00852 | val_0_rmse: 0.1732  | val_0_mae: 0.11431 | val_0_mse: 0.03    |  0:02:41s
    epoch 1366| loss: 0.00964 | val_0_rmse: 0.17317 | val_0_mae: 0.11569 | val_0_mse: 0.02999 |  0:02:41s
    epoch 1367| loss: 0.00854 | val_0_rmse: 0.17249 | val_0_mae: 0.11424 | val_0_mse: 0.02975 |  0:02:41s
    epoch 1368| loss: 0.009   | val_0_rmse: 0.17095 | val_0_mae: 0.11325 | val_0_mse: 0.02923 |  0:02:41s
    epoch 1369| loss: 0.00884 | val_0_rmse: 0.17074 | val_0_mae: 0.11483 | val_0_mse: 0.02915 |  0:02:42s
    epoch 1370| loss: 0.00917 | val_0_rmse: 0.17046 | val_0_mae: 0.11194 | val_0_mse: 0.02906 |  0:02:42s
    epoch 1371| loss: 0.00878 | val_0_rmse: 0.1708  | val_0_mae: 0.1131  | val_0_mse: 0.02917 |  0:02:42s
    epoch 1372| loss: 0.00887 | val_0_rmse: 0.17076 | val_0_mae: 0.11289 | val_0_mse: 0.02916 |  0:02:42s
    epoch 1373| loss: 0.00883 | val_0_rmse: 0.17064 | val_0_mae: 0.11255 | val_0_mse: 0.02912 |  0:02:42s
    epoch 1374| loss: 0.00863 | val_0_rmse: 0.17114 | val_0_mae: 0.11357 | val_0_mse: 0.02929 |  0:02:42s
    epoch 1375| loss: 0.00862 | val_0_rmse: 0.17098 | val_0_mae: 0.11238 | val_0_mse: 0.02923 |  0:02:42s
    epoch 1376| loss: 0.00871 | val_0_rmse: 0.17111 | val_0_mae: 0.1133  | val_0_mse: 0.02928 |  0:02:42s
    epoch 1377| loss: 0.00913 | val_0_rmse: 0.17039 | val_0_mae: 0.1128  | val_0_mse: 0.02903 |  0:02:42s
    epoch 1378| loss: 0.00878 | val_0_rmse: 0.17092 | val_0_mae: 0.11302 | val_0_mse: 0.02922 |  0:02:43s
    epoch 1379| loss: 0.00863 | val_0_rmse: 0.17176 | val_0_mae: 0.11489 | val_0_mse: 0.0295  |  0:02:43s
    epoch 1380| loss: 0.00963 | val_0_rmse: 0.17002 | val_0_mae: 0.11201 | val_0_mse: 0.02891 |  0:02:43s
    epoch 1381| loss: 0.00879 | val_0_rmse: 0.17078 | val_0_mae: 0.11386 | val_0_mse: 0.02917 |  0:02:43s
    epoch 1382| loss: 0.00873 | val_0_rmse: 0.17094 | val_0_mae: 0.11394 | val_0_mse: 0.02922 |  0:02:43s
    epoch 1383| loss: 0.00877 | val_0_rmse: 0.17131 | val_0_mae: 0.11375 | val_0_mse: 0.02935 |  0:02:43s
    epoch 1384| loss: 0.00906 | val_0_rmse: 0.17074 | val_0_mae: 0.1133  | val_0_mse: 0.02915 |  0:02:43s
    epoch 1385| loss: 0.00872 | val_0_rmse: 0.17115 | val_0_mae: 0.11382 | val_0_mse: 0.02929 |  0:02:43s
    epoch 1386| loss: 0.00901 | val_0_rmse: 0.17186 | val_0_mae: 0.11371 | val_0_mse: 0.02954 |  0:02:44s
    epoch 1387| loss: 0.0085  | val_0_rmse: 0.1718  | val_0_mae: 0.11437 | val_0_mse: 0.02952 |  0:02:44s
    epoch 1388| loss: 0.00862 | val_0_rmse: 0.17125 | val_0_mae: 0.11411 | val_0_mse: 0.02933 |  0:02:44s
    epoch 1389| loss: 0.00892 | val_0_rmse: 0.1721  | val_0_mae: 0.11423 | val_0_mse: 0.02962 |  0:02:44s
    epoch 1390| loss: 0.00891 | val_0_rmse: 0.17316 | val_0_mae: 0.11529 | val_0_mse: 0.02998 |  0:02:44s
    epoch 1391| loss: 0.00889 | val_0_rmse: 0.17373 | val_0_mae: 0.11599 | val_0_mse: 0.03018 |  0:02:44s
    epoch 1392| loss: 0.00962 | val_0_rmse: 0.17289 | val_0_mae: 0.11447 | val_0_mse: 0.02989 |  0:02:44s
    epoch 1393| loss: 0.0093  | val_0_rmse: 0.17301 | val_0_mae: 0.1174  | val_0_mse: 0.02993 |  0:02:44s
    epoch 1394| loss: 0.0089  | val_0_rmse: 0.17255 | val_0_mae: 0.11531 | val_0_mse: 0.02977 |  0:02:44s
    epoch 1395| loss: 0.00856 | val_0_rmse: 0.17272 | val_0_mae: 0.11541 | val_0_mse: 0.02983 |  0:02:45s
    epoch 1396| loss: 0.00878 | val_0_rmse: 0.17182 | val_0_mae: 0.11429 | val_0_mse: 0.02952 |  0:02:45s
    epoch 1397| loss: 0.00847 | val_0_rmse: 0.17145 | val_0_mae: 0.11358 | val_0_mse: 0.0294  |  0:02:45s
    epoch 1398| loss: 0.00893 | val_0_rmse: 0.17206 | val_0_mae: 0.11369 | val_0_mse: 0.0296  |  0:02:45s
    epoch 1399| loss: 0.00951 | val_0_rmse: 0.1729  | val_0_mae: 0.11423 | val_0_mse: 0.02989 |  0:02:45s
    epoch 1400| loss: 0.00876 | val_0_rmse: 0.17227 | val_0_mae: 0.11378 | val_0_mse: 0.02968 |  0:02:45s
    epoch 1401| loss: 0.00883 | val_0_rmse: 0.17111 | val_0_mae: 0.11393 | val_0_mse: 0.02928 |  0:02:45s
    epoch 1402| loss: 0.0088  | val_0_rmse: 0.17105 | val_0_mae: 0.11356 | val_0_mse: 0.02926 |  0:02:45s
    epoch 1403| loss: 0.00856 | val_0_rmse: 0.17266 | val_0_mae: 0.1166  | val_0_mse: 0.02981 |  0:02:45s
    epoch 1404| loss: 0.00906 | val_0_rmse: 0.17035 | val_0_mae: 0.11326 | val_0_mse: 0.02902 |  0:02:46s
    epoch 1405| loss: 0.00845 | val_0_rmse: 0.16899 | val_0_mae: 0.1122  | val_0_mse: 0.02856 |  0:02:46s
    epoch 1406| loss: 0.0087  | val_0_rmse: 0.17037 | val_0_mae: 0.11513 | val_0_mse: 0.02903 |  0:02:46s
    epoch 1407| loss: 0.0086  | val_0_rmse: 0.1716  | val_0_mae: 0.11407 | val_0_mse: 0.02945 |  0:02:46s
    epoch 1408| loss: 0.00908 | val_0_rmse: 0.17196 | val_0_mae: 0.11417 | val_0_mse: 0.02957 |  0:02:46s
    epoch 1409| loss: 0.0084  | val_0_rmse: 0.17183 | val_0_mae: 0.1136  | val_0_mse: 0.02952 |  0:02:46s
    epoch 1410| loss: 0.00802 | val_0_rmse: 0.17192 | val_0_mae: 0.11338 | val_0_mse: 0.02955 |  0:02:46s
    epoch 1411| loss: 0.0088  | val_0_rmse: 0.17145 | val_0_mae: 0.11405 | val_0_mse: 0.02939 |  0:02:46s
    epoch 1412| loss: 0.00909 | val_0_rmse: 0.1713  | val_0_mae: 0.11342 | val_0_mse: 0.02934 |  0:02:46s
    epoch 1413| loss: 0.00826 | val_0_rmse: 0.17095 | val_0_mae: 0.11295 | val_0_mse: 0.02922 |  0:02:47s
    epoch 1414| loss: 0.0081  | val_0_rmse: 0.17115 | val_0_mae: 0.11358 | val_0_mse: 0.02929 |  0:02:47s
    epoch 1415| loss: 0.00852 | val_0_rmse: 0.17198 | val_0_mae: 0.11435 | val_0_mse: 0.02958 |  0:02:47s
    epoch 1416| loss: 0.0083  | val_0_rmse: 0.17225 | val_0_mae: 0.11513 | val_0_mse: 0.02967 |  0:02:47s
    epoch 1417| loss: 0.00852 | val_0_rmse: 0.17181 | val_0_mae: 0.11462 | val_0_mse: 0.02952 |  0:02:47s
    epoch 1418| loss: 0.00812 | val_0_rmse: 0.17066 | val_0_mae: 0.11301 | val_0_mse: 0.02912 |  0:02:47s
    epoch 1419| loss: 0.00841 | val_0_rmse: 0.17075 | val_0_mae: 0.11403 | val_0_mse: 0.02915 |  0:02:47s
    epoch 1420| loss: 0.0082  | val_0_rmse: 0.1741  | val_0_mae: 0.11673 | val_0_mse: 0.03031 |  0:02:47s
    epoch 1421| loss: 0.00848 | val_0_rmse: 0.17347 | val_0_mae: 0.11431 | val_0_mse: 0.03009 |  0:02:48s
    epoch 1422| loss: 0.00963 | val_0_rmse: 0.17288 | val_0_mae: 0.11458 | val_0_mse: 0.02989 |  0:02:48s
    epoch 1423| loss: 0.00914 | val_0_rmse: 0.17225 | val_0_mae: 0.11465 | val_0_mse: 0.02967 |  0:02:48s
    epoch 1424| loss: 0.00889 | val_0_rmse: 0.17156 | val_0_mae: 0.1137  | val_0_mse: 0.02943 |  0:02:48s
    epoch 1425| loss: 0.00897 | val_0_rmse: 0.17242 | val_0_mae: 0.11665 | val_0_mse: 0.02973 |  0:02:48s
    epoch 1426| loss: 0.0098  | val_0_rmse: 0.1712  | val_0_mae: 0.11369 | val_0_mse: 0.02931 |  0:02:48s
    epoch 1427| loss: 0.00947 | val_0_rmse: 0.17189 | val_0_mae: 0.11481 | val_0_mse: 0.02955 |  0:02:48s
    epoch 1428| loss: 0.00918 | val_0_rmse: 0.17207 | val_0_mae: 0.11409 | val_0_mse: 0.02961 |  0:02:48s
    epoch 1429| loss: 0.00856 | val_0_rmse: 0.1726  | val_0_mae: 0.11517 | val_0_mse: 0.02979 |  0:02:48s
    epoch 1430| loss: 0.009   | val_0_rmse: 0.17145 | val_0_mae: 0.11382 | val_0_mse: 0.0294  |  0:02:49s
    epoch 1431| loss: 0.00887 | val_0_rmse: 0.17062 | val_0_mae: 0.1135  | val_0_mse: 0.02911 |  0:02:49s
    epoch 1432| loss: 0.00848 | val_0_rmse: 0.1707  | val_0_mae: 0.11285 | val_0_mse: 0.02914 |  0:02:49s
    epoch 1433| loss: 0.00844 | val_0_rmse: 0.17092 | val_0_mae: 0.11321 | val_0_mse: 0.02922 |  0:02:49s
    epoch 1434| loss: 0.00859 | val_0_rmse: 0.1709  | val_0_mae: 0.11311 | val_0_mse: 0.02921 |  0:02:49s
    epoch 1435| loss: 0.00863 | val_0_rmse: 0.17099 | val_0_mae: 0.114   | val_0_mse: 0.02924 |  0:02:49s
    epoch 1436| loss: 0.00874 | val_0_rmse: 0.17109 | val_0_mae: 0.11328 | val_0_mse: 0.02927 |  0:02:49s
    epoch 1437| loss: 0.00846 | val_0_rmse: 0.17256 | val_0_mae: 0.11397 | val_0_mse: 0.02978 |  0:02:49s
    epoch 1438| loss: 0.00844 | val_0_rmse: 0.17381 | val_0_mae: 0.11467 | val_0_mse: 0.03021 |  0:02:49s
    epoch 1439| loss: 0.0084  | val_0_rmse: 0.17473 | val_0_mae: 0.11495 | val_0_mse: 0.03053 |  0:02:50s
    epoch 1440| loss: 0.0085  | val_0_rmse: 0.17496 | val_0_mae: 0.11555 | val_0_mse: 0.03061 |  0:02:50s
    epoch 1441| loss: 0.00843 | val_0_rmse: 0.17404 | val_0_mae: 0.11488 | val_0_mse: 0.03029 |  0:02:50s
    epoch 1442| loss: 0.0088  | val_0_rmse: 0.17303 | val_0_mae: 0.11396 | val_0_mse: 0.02994 |  0:02:50s
    epoch 1443| loss: 0.00891 | val_0_rmse: 0.17251 | val_0_mae: 0.11366 | val_0_mse: 0.02976 |  0:02:50s
    epoch 1444| loss: 0.00862 | val_0_rmse: 0.17223 | val_0_mae: 0.11297 | val_0_mse: 0.02966 |  0:02:50s
    epoch 1445| loss: 0.009   | val_0_rmse: 0.17484 | val_0_mae: 0.11545 | val_0_mse: 0.03057 |  0:02:50s
    epoch 1446| loss: 0.00874 | val_0_rmse: 0.17322 | val_0_mae: 0.11318 | val_0_mse: 0.03001 |  0:02:50s
    epoch 1447| loss: 0.00914 | val_0_rmse: 0.17337 | val_0_mae: 0.11356 | val_0_mse: 0.03006 |  0:02:50s
    epoch 1448| loss: 0.00886 | val_0_rmse: 0.17353 | val_0_mae: 0.11432 | val_0_mse: 0.03011 |  0:02:51s
    epoch 1449| loss: 0.00872 | val_0_rmse: 0.1717  | val_0_mae: 0.11308 | val_0_mse: 0.02948 |  0:02:51s
    epoch 1450| loss: 0.00895 | val_0_rmse: 0.17347 | val_0_mae: 0.11603 | val_0_mse: 0.03009 |  0:02:51s
    epoch 1451| loss: 0.00926 | val_0_rmse: 0.17258 | val_0_mae: 0.11331 | val_0_mse: 0.02978 |  0:02:51s
    epoch 1452| loss: 0.00833 | val_0_rmse: 0.17283 | val_0_mae: 0.11443 | val_0_mse: 0.02987 |  0:02:51s
    epoch 1453| loss: 0.0089  | val_0_rmse: 0.17245 | val_0_mae: 0.1126  | val_0_mse: 0.02974 |  0:02:51s
    epoch 1454| loss: 0.0085  | val_0_rmse: 0.17259 | val_0_mae: 0.11254 | val_0_mse: 0.02979 |  0:02:51s
    epoch 1455| loss: 0.00879 | val_0_rmse: 0.17242 | val_0_mae: 0.1144  | val_0_mse: 0.02973 |  0:02:51s
    epoch 1456| loss: 0.0086  | val_0_rmse: 0.17166 | val_0_mae: 0.11321 | val_0_mse: 0.02947 |  0:02:51s
    epoch 1457| loss: 0.00919 | val_0_rmse: 0.17149 | val_0_mae: 0.11307 | val_0_mse: 0.02941 |  0:02:52s
    epoch 1458| loss: 0.00916 | val_0_rmse: 0.17355 | val_0_mae: 0.11554 | val_0_mse: 0.03012 |  0:02:52s
    epoch 1459| loss: 0.00856 | val_0_rmse: 0.17394 | val_0_mae: 0.11543 | val_0_mse: 0.03025 |  0:02:52s
    epoch 1460| loss: 0.00991 | val_0_rmse: 0.17299 | val_0_mae: 0.11472 | val_0_mse: 0.02993 |  0:02:52s
    epoch 1461| loss: 0.0096  | val_0_rmse: 0.1727  | val_0_mae: 0.1136  | val_0_mse: 0.02983 |  0:02:52s
    epoch 1462| loss: 0.00872 | val_0_rmse: 0.17383 | val_0_mae: 0.11385 | val_0_mse: 0.03022 |  0:02:52s
    epoch 1463| loss: 0.00936 | val_0_rmse: 0.1729  | val_0_mae: 0.11563 | val_0_mse: 0.02989 |  0:02:52s
    epoch 1464| loss: 0.00943 | val_0_rmse: 0.17029 | val_0_mae: 0.11091 | val_0_mse: 0.029   |  0:02:52s
    epoch 1465| loss: 0.00962 | val_0_rmse: 0.17037 | val_0_mae: 0.11056 | val_0_mse: 0.02902 |  0:02:52s
    epoch 1466| loss: 0.00872 | val_0_rmse: 0.17278 | val_0_mae: 0.11413 | val_0_mse: 0.02985 |  0:02:53s
    epoch 1467| loss: 0.00845 | val_0_rmse: 0.17342 | val_0_mae: 0.11284 | val_0_mse: 0.03008 |  0:02:53s
    epoch 1468| loss: 0.00866 | val_0_rmse: 0.17378 | val_0_mae: 0.11393 | val_0_mse: 0.0302  |  0:02:53s
    epoch 1469| loss: 0.00836 | val_0_rmse: 0.17345 | val_0_mae: 0.11399 | val_0_mse: 0.03008 |  0:02:53s
    epoch 1470| loss: 0.009   | val_0_rmse: 0.17438 | val_0_mae: 0.11444 | val_0_mse: 0.03041 |  0:02:53s
    epoch 1471| loss: 0.00908 | val_0_rmse: 0.17399 | val_0_mae: 0.1151  | val_0_mse: 0.03027 |  0:02:53s
    epoch 1472| loss: 0.00793 | val_0_rmse: 0.17335 | val_0_mae: 0.11394 | val_0_mse: 0.03005 |  0:02:53s
    epoch 1473| loss: 0.00817 | val_0_rmse: 0.17435 | val_0_mae: 0.11488 | val_0_mse: 0.0304  |  0:02:53s
    epoch 1474| loss: 0.00936 | val_0_rmse: 0.17501 | val_0_mae: 0.11766 | val_0_mse: 0.03063 |  0:02:53s
    epoch 1475| loss: 0.00818 | val_0_rmse: 0.17306 | val_0_mae: 0.11551 | val_0_mse: 0.02995 |  0:02:54s
    epoch 1476| loss: 0.00964 | val_0_rmse: 0.17427 | val_0_mae: 0.11552 | val_0_mse: 0.03037 |  0:02:54s
    epoch 1477| loss: 0.00975 | val_0_rmse: 0.1756  | val_0_mae: 0.11932 | val_0_mse: 0.03083 |  0:02:54s
    epoch 1478| loss: 0.01036 | val_0_rmse: 0.17373 | val_0_mae: 0.11586 | val_0_mse: 0.03018 |  0:02:54s
    epoch 1479| loss: 0.00987 | val_0_rmse: 0.17348 | val_0_mae: 0.11581 | val_0_mse: 0.03009 |  0:02:54s
    epoch 1480| loss: 0.0091  | val_0_rmse: 0.17407 | val_0_mae: 0.11591 | val_0_mse: 0.0303  |  0:02:54s
    epoch 1481| loss: 0.00984 | val_0_rmse: 0.17167 | val_0_mae: 0.11327 | val_0_mse: 0.02947 |  0:02:54s
    epoch 1482| loss: 0.0092  | val_0_rmse: 0.16901 | val_0_mae: 0.11284 | val_0_mse: 0.02857 |  0:02:54s
    epoch 1483| loss: 0.0096  | val_0_rmse: 0.16912 | val_0_mae: 0.11251 | val_0_mse: 0.0286  |  0:02:55s
    epoch 1484| loss: 0.00904 | val_0_rmse: 0.17063 | val_0_mae: 0.11381 | val_0_mse: 0.02911 |  0:02:55s
    epoch 1485| loss: 0.00924 | val_0_rmse: 0.17155 | val_0_mae: 0.11547 | val_0_mse: 0.02943 |  0:02:55s
    epoch 1486| loss: 0.00904 | val_0_rmse: 0.17142 | val_0_mae: 0.11397 | val_0_mse: 0.02939 |  0:02:55s
    epoch 1487| loss: 0.00897 | val_0_rmse: 0.17226 | val_0_mae: 0.11429 | val_0_mse: 0.02967 |  0:02:55s
    epoch 1488| loss: 0.00906 | val_0_rmse: 0.17198 | val_0_mae: 0.11326 | val_0_mse: 0.02958 |  0:02:55s
    epoch 1489| loss: 0.009   | val_0_rmse: 0.16995 | val_0_mae: 0.1119  | val_0_mse: 0.02888 |  0:02:55s
    epoch 1490| loss: 0.00877 | val_0_rmse: 0.17072 | val_0_mae: 0.11179 | val_0_mse: 0.02914 |  0:02:55s
    epoch 1491| loss: 0.00908 | val_0_rmse: 0.17253 | val_0_mae: 0.11452 | val_0_mse: 0.02977 |  0:02:55s
    epoch 1492| loss: 0.00928 | val_0_rmse: 0.17222 | val_0_mae: 0.11382 | val_0_mse: 0.02966 |  0:02:56s
    epoch 1493| loss: 0.0087  | val_0_rmse: 0.17127 | val_0_mae: 0.11438 | val_0_mse: 0.02933 |  0:02:56s
    epoch 1494| loss: 0.00886 | val_0_rmse: 0.17078 | val_0_mae: 0.11208 | val_0_mse: 0.02917 |  0:02:56s
    epoch 1495| loss: 0.00855 | val_0_rmse: 0.17114 | val_0_mae: 0.11215 | val_0_mse: 0.02929 |  0:02:56s
    epoch 1496| loss: 0.00799 | val_0_rmse: 0.17301 | val_0_mae: 0.11533 | val_0_mse: 0.02993 |  0:02:56s
    epoch 1497| loss: 0.00845 | val_0_rmse: 0.17125 | val_0_mae: 0.11174 | val_0_mse: 0.02933 |  0:02:56s
    epoch 1498| loss: 0.00908 | val_0_rmse: 0.17041 | val_0_mae: 0.11246 | val_0_mse: 0.02904 |  0:02:56s
    
    Early stopping occurred at epoch 1498 with best_epoch = 1198 and best_val_0_mse = 0.02829
    Final test score:  0.11950071034662174


.. parsed-literal::

    /opt/venvs/tensorflow/lib/python3.11/site-packages/pytorch_tabnet/callbacks.py:172: UserWarning: Best weights from best epoch are automatically used!
      warnings.warn(wrn_msg)


.. code:: ipython3

    X




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>col_0</th>
          <th>col_1</th>
          <th>col_2</th>
          <th>col_3</th>
          <th>col_4</th>
          <th>col_5</th>
          <th>col_6</th>
          <th>col_7</th>
          <th>col_8</th>
          <th>col_9</th>
          <th>...</th>
          <th>col_59</th>
          <th>col_60</th>
          <th>col_61</th>
          <th>col_62</th>
          <th>col_63</th>
          <th>col_64</th>
          <th>col_65</th>
          <th>col_66</th>
          <th>col_67</th>
          <th>col_68</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>318.537570</td>
          <td>-105.688985</td>
          <td>-344.782099</td>
          <td>776.301796</td>
          <td>-422.124764</td>
          <td>-349.252299</td>
          <td>892.480273</td>
          <td>-314.072991</td>
          <td>-765.183088</td>
          <td>833.742853</td>
          <td>...</td>
          <td>-673.510182</td>
          <td>-797.151793</td>
          <td>102.383167</td>
          <td>351.377492</td>
          <td>705.769464</td>
          <td>745.626934</td>
          <td>-672.749070</td>
          <td>37.912559</td>
          <td>44.572019</td>
          <td>-0.068508</td>
        </tr>
        <tr>
          <th>1</th>
          <td>331.043792</td>
          <td>-106.066452</td>
          <td>-325.057207</td>
          <td>780.875925</td>
          <td>-405.770977</td>
          <td>-340.830997</td>
          <td>861.915655</td>
          <td>-316.604112</td>
          <td>-773.849192</td>
          <td>796.474642</td>
          <td>...</td>
          <td>-670.348882</td>
          <td>-803.903229</td>
          <td>470.845687</td>
          <td>978.464027</td>
          <td>193.131275</td>
          <td>380.666434</td>
          <td>976.566840</td>
          <td>38.236469</td>
          <td>42.399777</td>
          <td>-0.088507</td>
        </tr>
        <tr>
          <th>2</th>
          <td>325.220097</td>
          <td>-106.740678</td>
          <td>-339.710357</td>
          <td>777.722674</td>
          <td>-433.236777</td>
          <td>-365.925475</td>
          <td>885.879756</td>
          <td>-302.301703</td>
          <td>-761.395322</td>
          <td>794.216045</td>
          <td>...</td>
          <td>-673.831722</td>
          <td>-800.956963</td>
          <td>56.483612</td>
          <td>947.728908</td>
          <td>1183.604256</td>
          <td>646.327178</td>
          <td>136.844224</td>
          <td>51.441338</td>
          <td>30.844137</td>
          <td>-0.070826</td>
        </tr>
        <tr>
          <th>3</th>
          <td>315.041000</td>
          <td>-100.251559</td>
          <td>-364.497573</td>
          <td>814.283888</td>
          <td>-411.104582</td>
          <td>-367.199172</td>
          <td>888.625287</td>
          <td>-316.603090</td>
          <td>-795.967765</td>
          <td>876.380577</td>
          <td>...</td>
          <td>-663.932624</td>
          <td>-810.366501</td>
          <td>10.556329</td>
          <td>1424.036500</td>
          <td>464.316577</td>
          <td>710.433742</td>
          <td>355.677607</td>
          <td>58.454446</td>
          <td>30.161479</td>
          <td>-0.062253</td>
        </tr>
        <tr>
          <th>4</th>
          <td>327.202995</td>
          <td>-102.659730</td>
          <td>-336.783640</td>
          <td>817.603617</td>
          <td>-423.830321</td>
          <td>-367.601537</td>
          <td>931.636914</td>
          <td>-295.274436</td>
          <td>-732.291440</td>
          <td>838.988254</td>
          <td>...</td>
          <td>-680.300329</td>
          <td>-804.427753</td>
          <td>40.458445</td>
          <td>244.053107</td>
          <td>137.972405</td>
          <td>1174.084352</td>
          <td>1235.238752</td>
          <td>36.631358</td>
          <td>39.560704</td>
          <td>-0.094738</td>
        </tr>
        <tr>
          <th>...</th>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
        </tr>
        <tr>
          <th>4995</th>
          <td>312.658498</td>
          <td>-108.490348</td>
          <td>-350.826961</td>
          <td>776.732486</td>
          <td>-415.718930</td>
          <td>-342.191044</td>
          <td>892.593256</td>
          <td>-306.501771</td>
          <td>-760.163055</td>
          <td>798.573730</td>
          <td>...</td>
          <td>-671.620917</td>
          <td>-806.277565</td>
          <td>854.572764</td>
          <td>1417.694118</td>
          <td>461.622975</td>
          <td>1247.086226</td>
          <td>1159.433770</td>
          <td>33.790555</td>
          <td>41.937085</td>
          <td>-0.080447</td>
        </tr>
        <tr>
          <th>4996</th>
          <td>333.808974</td>
          <td>-111.618743</td>
          <td>-358.315406</td>
          <td>756.268801</td>
          <td>-433.045223</td>
          <td>-370.207437</td>
          <td>926.244264</td>
          <td>-295.228612</td>
          <td>-721.551561</td>
          <td>800.059243</td>
          <td>...</td>
          <td>-660.437624</td>
          <td>-782.266526</td>
          <td>211.603579</td>
          <td>636.975500</td>
          <td>675.417136</td>
          <td>1286.907805</td>
          <td>875.912466</td>
          <td>37.970978</td>
          <td>48.243003</td>
          <td>-0.073006</td>
        </tr>
        <tr>
          <th>4997</th>
          <td>322.349713</td>
          <td>-111.898039</td>
          <td>-330.297291</td>
          <td>804.613920</td>
          <td>-415.032000</td>
          <td>-363.548828</td>
          <td>853.023507</td>
          <td>-317.629365</td>
          <td>-805.332571</td>
          <td>841.473202</td>
          <td>...</td>
          <td>-678.610047</td>
          <td>-811.097153</td>
          <td>475.953638</td>
          <td>310.072152</td>
          <td>1281.330362</td>
          <td>595.399871</td>
          <td>1292.472002</td>
          <td>51.349449</td>
          <td>54.055874</td>
          <td>-0.060540</td>
        </tr>
        <tr>
          <th>4998</th>
          <td>320.941704</td>
          <td>-109.376080</td>
          <td>-352.789073</td>
          <td>740.130769</td>
          <td>-402.333706</td>
          <td>-368.692858</td>
          <td>896.105857</td>
          <td>-316.625261</td>
          <td>-768.947026</td>
          <td>821.369079</td>
          <td>...</td>
          <td>-661.460364</td>
          <td>-807.745470</td>
          <td>137.259720</td>
          <td>223.818457</td>
          <td>1442.211061</td>
          <td>485.027785</td>
          <td>1213.311455</td>
          <td>31.507366</td>
          <td>49.434780</td>
          <td>-0.071665</td>
        </tr>
        <tr>
          <th>4999</th>
          <td>317.307279</td>
          <td>-105.163018</td>
          <td>-357.287618</td>
          <td>750.070604</td>
          <td>-418.285415</td>
          <td>-358.040925</td>
          <td>907.415014</td>
          <td>-301.048090</td>
          <td>-756.703210</td>
          <td>837.797896</td>
          <td>...</td>
          <td>-671.132669</td>
          <td>-795.625598</td>
          <td>430.693792</td>
          <td>409.190929</td>
          <td>1087.061549</td>
          <td>1224.381272</td>
          <td>656.509527</td>
          <td>45.271678</td>
          <td>53.091571</td>
          <td>-0.070216</td>
        </tr>
      </tbody>
    </table>
    <p>5000 rows × 69 columns</p>
    </div>



.. code:: ipython3

    X_test




.. parsed-literal::

    array([[ 3.32525731e+02, -1.04644317e+02, -3.36847598e+02, ...,
             4.70283589e+01,  4.39246583e+01, -8.62332453e-02],
           [ 3.14794480e+02, -1.09462871e+02, -3.34499643e+02, ...,
             3.36142585e+01,  4.22607335e+01, -7.67718631e-02],
           [ 3.00393690e+02, -1.09987787e+02, -3.29255868e+02, ...,
             3.81125726e+01,  4.94278843e+01, -8.44866726e-02],
           ...,
           [ 3.17684151e+02, -1.09434100e+02, -3.36762928e+02, ...,
             3.66601776e+01,  4.15948580e+01, -8.01291188e-02],
           [ 3.25991169e+02, -1.09306614e+02, -3.61410070e+02, ...,
             3.91384228e+01,  4.93876403e+01, -7.31450990e-02],
           [ 3.24336487e+02, -1.10083728e+02, -3.47959600e+02, ...,
             3.54791410e+01,  4.72827973e+01, -7.48318749e-02]])



.. code:: ipython3

    X_test.shape




.. parsed-literal::

    (1000, 69)



.. code:: ipython3

    y = regressor.predict(X_test) # X is a vector with 69 values

.. code:: ipython3

    y




.. parsed-literal::

    array([[2.1452718 , 1.6329834 ],
           [1.4703108 , 1.0289346 ],
           [1.9577343 , 1.1927242 ],
           ...,
           [1.329911  , 1.0410416 ],
           [0.82285684, 1.5039024 ],
           [1.2100147 , 1.1607559 ]], dtype=float32)



.. code:: ipython3

    y_test




.. parsed-literal::

    array([[2.27708263, 1.64015909],
           [1.48112191, 1.00691477],
           [1.92089405, 1.11805966],
           ...,
           [1.37331878, 1.01655114],
           [0.79205932, 1.49273011],
           [1.27456681, 1.09328693]])



.. code:: ipython3

    y.shape




.. parsed-literal::

    (1000, 2)



.. code:: ipython3

    plt.plot(abs((y-y_test)/y_test))
    plt.yscale("log")



.. image:: output_17_0.png


.. code:: ipython3

    
    plt.plot(y_test[:, 0], y_test[:, 1], 'k.', alpha=0.2, label="Test data")
    plt.plot(y[:, 0], y[:, 1], 'bx', alpha=0.2, label="predicted")
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()




.. parsed-literal::

    <matplotlib.legend.Legend at 0x7f014088a450>




.. image:: output_18_1.png


.. code:: ipython3

    points_start = y
    points_end = y_test
    
    differences = points_end - points_start
    plt.figure(figsize=(10, 6))
    plt.plot(y_test[:, 0], y_test[:, 1], 'b.', alpha=0.3, label="Test data")
    plt.plot(y[:, 0], y[:, 1], 'r.', alpha=0.3, label="predicted")
    plt.quiver(points_start[:, 0], points_start[:, 1], differences[:, 0], differences[:, 1],
               angles='xy', scale_units='xy', scale=1, width=0.0025, color="black", alpha=0.4)
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()




.. parsed-literal::

    <matplotlib.legend.Legend at 0x7f013db14290>




.. image:: output_19_1.png


.. code:: ipython3

    plt.figure(figsize=(10, 6))
    plt.plot(y_train[:, 0], y_train[:, 1], 'k.', alpha=0.3, label="Test data")




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x7f013d863210>]




.. image:: output_20_1.png


Redone without outliers

.. code:: ipython3

    y = regressor.predict(X_test)
    #y_test  = y_test.values
    
    points_start = y
    points_end = y_test
    
    differences = points_end - points_start
    plt.figure(figsize=(10, 6))
    plt.plot(y_test[:, 0], y_test[:, 1], 'b.', alpha=0.3, label="Test data")
    plt.plot(y[:, 0], y[:, 1], 'r.', alpha=0.3, label="predicted")
    plt.quiver(points_start[:, 0], points_start[:, 1], differences[:, 0], differences[:, 1],
               angles='xy', scale_units='xy', scale=1, width=0.0025, color="black", alpha=0.4)
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()




.. parsed-literal::

    <matplotlib.legend.Legend at 0x7f0137f63e90>




.. image:: output_22_1.png


.. code:: ipython3

    indices = np.any(y_test > 4, axis=1)
    X_test_cropped = X_test[~indices]
    y_test_cropped = y_test[~indices]
    
    # Evaluate on test data: difference between the regression (X_test -> preds) and the expected data (y_test)
    from sklearn.metrics import mean_squared_error
    preds = regressor.predict(X_test_cropped) # should be close to 'y_test'
    test_mse = mean_squared_error(y_pred=preds, y_true=y_test_cropped)
    print("Final test score: ", test_mse)


.. parsed-literal::

    Final test score:  0.027538374259424472



.. code:: ipython3

    y = regressor.predict(X_test_cropped)
    #y_test  = y_test.values
    
    points_start = y
    points_end = y_test_cropped
    
    differences = points_end - points_start
    plt.figure(figsize=(10, 6))
    plt.plot(y_test_cropped[:, 0], y_test_cropped[:, 1], 'b.', alpha=0.3, label="Test data")
    plt.plot(y[:, 0], y[:, 1], 'r.', alpha=0.3, label="predicted")
    plt.quiver(points_start[:, 0], points_start[:, 1], differences[:, 0], differences[:, 1],
               angles='xy', scale_units='xy', scale=1, width=0.0025, color="black", alpha=0.4)
    plt.yscale("log")
    plt.xscale("log")
    plt.legend()




.. parsed-literal::

    <matplotlib.legend.Legend at 0x7f013e890d90>




.. image:: output_25_1.png





















































































































.. code:: ipython3

    vvvvvvvvvvvvv

.. code:: ipython3

    vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

.. code:: ipython3

    vvvvvvvvvvvvvvvvvvv







.. code:: ipython3

    vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv








.. code:: ipython3

    from joblib import dump
    dump(regressor, 'tabnet_accelerator.joblib')

.. code:: ipython3

    from joblib import load
    regressor = load('tabnet_accelerator.joblib')
